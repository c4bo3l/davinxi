﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterOperativeAction
    {
        public MasterOperativeAction()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Required]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Key]
        [Required]
        [Column(Order = 1)]
        [Index("ActionIDIndex")]
        [Display(Name = "Operative Action ID")]
        public string action_id { get; set; }

        [Required]
        [Display(Name = "Operative Action Name")]
        public string action_name { get; set; }

        [Display(Name = "Note")]
        public string note { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}