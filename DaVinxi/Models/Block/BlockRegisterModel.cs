﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class BlockRegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Block Initial")]
        public string block_id { get; set; }

        [Required]
        [Display(Name = "Division")]
        public string division_id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string block_name { get; set; }

        [Required]
        [Display(Name = "PIC")]
        [Index("PICIndex")]
        public string pic_username { get; set; }

        [Display(Name = "Description")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string description { get; set; }
    }
}