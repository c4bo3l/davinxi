﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    [Serializable]
    public class MasterUser
    {
        public MasterUser()
        {
            created_date = revised_date = DateTime.Now;
        }

        protected string _Username;

        [Key]
        [Required]
        [Display(Name="Username")]
        public string username
        {
            get { return _Username; }
            set { _Username = value != null ? value.ToLower() : ""; }
        }

        [Required]
        [Display(Name="Password")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string password { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string full_name { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.EmailAddress)]
        public string email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.PhoneNumber)]
        public string phone_number { get; set; }

        [Required]
        [Display(Name = "Department")]
        public string department { get; set; }

        [Required]
        [Display(Name = "Created Date")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [Display(Name = "Revised Date")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        [Display(Name = "Revised by")]
        public string revised_by { get; set; }

        [Required]
        [Display(Name = "Company")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }
    }
}