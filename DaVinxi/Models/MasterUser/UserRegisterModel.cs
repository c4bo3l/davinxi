﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class UserRegisterModel
    {
        protected string _Username;

        [Required]
        [Display(Name = "Username")]
        public string username
        {
            get { return _Username; }
            set { _Username = value != null ? value.ToLower() : ""; }
        }

        [Required]
        [Display(Name = "Password")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string password { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string full_name { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.EmailAddress)]
        public string email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.PhoneNumber)]
        public string phone_number { get; set; }

        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Department")]
        public string department { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("password", ErrorMessage = "The password and confirmation password do not match.")]
        public string confirm_password { get; set; }
    }
}