﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class UserEditModel
    {
        protected string _Username;

        [Required]
        [Display(Name = "Username")]
        public string username
        {
            get { return _Username; }
            set { _Username = value != null ? value.ToLower() : ""; }
        }

        [Required]
        [Display(Name = "Full Name")]
        public string full_name { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.EmailAddress)]
        public string email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.PhoneNumber)]
        public string phone_number { get; set; }

        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Department")]
        public string department { get; set; }
    }
}