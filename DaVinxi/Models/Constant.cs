﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AESLib;
using System.Web.Mvc;

namespace DaVinxi.Models
{
    public static class Constant
    {
        private static AES _AES;
        public static AES AESSecure
        {
            get
            {
                if (_AES == null)
                    _AES = new AES("davinxi_web_application_MVC");
                return _AES;
            }
        }

        public static DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /*private static MyDBContext _DB;
        public static MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }*/

        public static string CompanyImagePath = "~/Images/Companies/";
        public static string DefectImagePath = "~/Images/Defect/";
        public static string DefectRefFilePath = "~/PDFFile/Defect/";
    }
}