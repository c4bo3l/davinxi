﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class EngineeringUnitRegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Engineering Unit ID")]
        public string unit_id { get; set; }

        [Required]
        [Display(Name = "Engineering Unit Name")]
        public string unit_name { get; set; }

        [Display(Name = "Description")]
        public string description { get; set; }
    }
}