﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterEngineeringUnits
    {
        public MasterEngineeringUnits()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Display(Name="Company")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name="Engineering Unit ID")]
        public string unit_id { get; set; }

        [Required]
        [Display(Name = "Engineering Unit Name")]
        public string unit_name { get; set; }

        [Display(Name = "Description")]
        public string description { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}