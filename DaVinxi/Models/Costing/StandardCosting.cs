﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class StandardCosting
    {
        [Key, Required, Column(Order = 0)]
        public int company_id { get; set; }

        [Key, Required, Column(Order = 1)]
        public string sku_id { get; set; }

        [Key, Required, Column(Order = 2)]
        public string machine_id { get; set; }

        [Required]
        public double fixed_cost { get; set; }

        [Required]
        public double energy_cost { get; set; }

        [Required]
        public double chemical_cost { get; set; }

      
    }
}