﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class HistorianTag
    {
        public HistorianTag()
        {
            created_date = revised_date = DateTime.Now;
            last_date_data = new DateTime(1899, 1, 1, 1, 0, 0);
        }

        [Key,
        Column(Order = 0),
        Index("CompanyIndex"), 
        Required]
        public int company_id { get; set; }

        [Key, 
        Column(Order = 1), 
        Index("TagIndex"), 
        Display(Name = "Tag ID"),
        Required]
        public string tag_id { get; set; }

        [Required, Display(Name = "Tag Name"), MaxLength(128), Index("NameIndex")]
        public string tag_name { get; set; }

        [Display(Name="Description (optional)")]
        public string tag_description { get; set; }

        [Required, Display(Name = "UoM"), MaxLength(128), Index("UoMIndex")]
        public string uom { get; set; }

        [Required, 
        Display(Name = "Machine"), 
        Index("MachineIndex"),
        MaxLength(128)]
        public string machine_id { get; set; }

        [Required, Display(Name = "Compression Method"), MaxLength(128), Index("CompressionIndex")]
        public string compression_method { get; set; }

        [Display(Name = "Bandwidth")]
        public double compression_bandwidth { get; set; }

        [Display(Name = "Expired Time (in seconds)")]
        public double expired_time { get; set; }

        [Display(Name = "Active"), Index("ActiveIndex"), Required]
        public bool active { get; set; }

        public DateTime last_date_data { get; set; }

        [Display(Name="Source Tag ID (optional)")]
        public string source_tag_id { get; set; }

        public DateTime created_date { get; set; }

        public DateTime revised_date { get; set; }

        public string revised_by { get; set; }
    }
}