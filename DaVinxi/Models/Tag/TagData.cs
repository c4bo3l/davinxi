﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DaVinxi.Models
{
    public class TagData
    {
        public TagData()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Required, Key, Column(Order = 0), Index("CompanyIndex")]
        public int company_id { get; set; }

        [Required, Key, Column(Order = 1), Index("TagIndex")]
        public string tag_id { get; set; }

        [Required, Key, Column(Order = 2), Index("TimestampIndex")]
        public double timestamp { get; set; }

        [Required]
        public double value { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}