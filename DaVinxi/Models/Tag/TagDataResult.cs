﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class TagDataResult
    {
        public HistorianTag Tag { get; set; }

        public MasterEngineeringUnits Unit { get; set; }

        public List<TagDataStructure> Data { get; set; }
    }

    public class TagDataStructure {
        public double timestamp { get; set; }
        public double value { get; set; }
    }
}