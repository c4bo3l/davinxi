﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class TagCompression
    {
        public TagCompression()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Required, Key, Index("CompressionIndex")]
        public string compression_id { get; set; }

        [Required]
        public string compression_name { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}