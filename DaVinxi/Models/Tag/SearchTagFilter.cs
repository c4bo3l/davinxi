﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class SearchTagFilter
    {
        public int company_id { get; set; }
        public string division_id { get; set; }
        public string block_id { get; set; }
        public string machine_id { get; set; }
        public string tag_name { get; set; }

        public int active_state { get; set; }
    }
}