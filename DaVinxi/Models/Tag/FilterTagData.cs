﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class FilterTagData
    {
        public int company_id { get; set; }

        public IEnumerable<string> tag_id { get; set; }

        public DateTime start_date { get; set; }

        public DateTime finish_date { get; set; }

        public double small_step { get; set; }

        public double small_step_type { get; set; }

        public double big_step { get; set; }

        public double big_step_type { get; set; }
    }
}