﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class DivisionRegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Division Initial")]
        public string division_id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string division_name { get; set; }

        [Required]
        [Display(Name = "PIC")]
        public string pic_username { get; set; }

        [Display(Name = "Description")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string description { get; set; }
    }
}