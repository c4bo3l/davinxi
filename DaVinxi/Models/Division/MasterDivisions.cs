﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterDivisions
    {
        public MasterDivisions()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key,Column(Order=0)]
        [Display(Name="Company")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key, Column(Order = 1)]
        [Display(Name = "Division Initial")]
        public string division_id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string division_name { get; set; }

        [Required]
        [Index("PICIndex")]
        [Display(Name = "PIC")]
        [MaxLength(128)]
        public string pic_username { get; set; }

        [Display(Name = "Description")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string description { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}