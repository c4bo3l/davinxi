﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterMachine
    {
        public MasterMachine()
        {
            created_date = revised_date = installed_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Display(Name = "Company")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Machine Type")]
        [Index("MachineTypeIndex")]
        public string machine_type_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [Display(Name = "Machine ID")]
        [Index("MachineIndex")]
        public string machine_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("BlockIndex")]
        [Display(Name = "Block")]
        public string block_id { get; set; }

        [Required]
        [Display(Name = "Machine Name")]
        public string machine_name { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Date)]
        [DisplayFormat(DataFormatString="{0:MMMM dd yyyy}")]
        [Index("InstalledDateIndex")]
        [Display(Name="Installed on")]
        public DateTime installed_date { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}