﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MachineTypeRegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Machine Type ID")]
        public string machine_type_id { get; set; }

        [Required]
        [Display(Name = "Machine Type Name")]
        public string machine_type_name { get; set; }
    }
}