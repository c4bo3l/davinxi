﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class PostedMachineComponents
    {
        public string id { get; set; }
        public bool is_numeric { get; set; }
        public string data_value { get; set; }

        public string tag_id { get; set; }
    }
}