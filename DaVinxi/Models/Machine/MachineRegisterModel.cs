﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class MachineRegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Machine Type")]
        public string machine_type_id { get; set; }

        [Required]
        [Display(Name = "Machine ID")]
        public string machine_id { get; set; }

        [Required]
        [Display(Name = "Machine Name")]
        public string machine_name { get; set; }

        [Required]
        [Display(Name = "Block")]
        public string block_id { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMMM dd yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Installed on")]
        public DateTime installed_date { get; set; }
    }
}