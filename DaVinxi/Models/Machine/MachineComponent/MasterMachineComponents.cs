﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterMachineComponents
    {
        public MasterMachineComponents()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Component ID")]
        public string component_id { get; set; }

        [Required]
        [Display(Name = "Component Name")]
        public string component_name { get; set; }

        [Required]
        [Display(Name = "Engineering Units")]
        [Index("UnitIndex")]
        [MaxLength(128)]
        public string unit_id { get; set; }

        [Display(Name = "Numeric data")]
        [Index("IsNumericIndex")]
        public bool is_numeric { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}