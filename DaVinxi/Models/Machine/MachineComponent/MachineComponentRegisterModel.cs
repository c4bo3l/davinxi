﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class MachineComponentRegisterModel
    {
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Component ID")]
        public string component_id { get; set; }

        [Required]
        [Display(Name = "Component Name")]
        public string component_name { get; set; }

        [Required]
        [Display(Name = "Engineering Units")]
        public string unit_id { get; set; }

        [Display(Name = "Numeric data")]
        public bool is_numeric { get; set; }
    }
}