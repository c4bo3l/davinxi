﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MachineComponents
    {
        public MachineComponents()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Display(Name = "Company")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Machine")]
        [Index("MachineIndex")]
        public string machine_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [Display(Name = "Component")]
        [Index("ComponentIndex")]
        public string component_id { get; set; }

        [Display(Name = "Value")]
        public string value { get; set; }

        [Display(Name = "Tag"), MaxLength(128), Index("TagIndex")]
        public string tag_id { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}