﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class CompanyRegisterModel
    {
        [Required]
        [Display(Name = "Company Name")]
        public string company_name { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }

        [Required]
        [Display(Name = "Phone")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.PhoneNumber)]
        public string phone_number { get; set; }

        [Display(Name = "Fax")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.PhoneNumber)]
        public string fax_number { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.EmailAddress)]
        public string email { get; set; }

        [Display(Name = "Notes")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string notes { get; set; }

        [Display(Name = "Logo")]
        public string logo { get; set; }
    }
}