﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace DaVinxi.Models
{
    public class MyDBContext:DbContext
    {
        public MyDBContext()
        {
            Database.SetInitializer<MyDBContext>(null);
        }

        public void DetachAll()
        {
            foreach (DbEntityEntry dbEntityEntry in this.ChangeTracker.Entries())
            {

                if (dbEntityEntry.Entity != null)
                {
                    dbEntityEntry.State = EntityState.Detached;
                }
            }
        }

        public DbSet<MasterUser> Users { get; set; }
        public DbSet<MasterCompanies> Companies { get; set; }
        public DbSet<MasterDivisions> Divisions { get; set; }
        public DbSet<MasterBlocks> Blocks { get; set; }
        public DbSet<MasterEngineeringUnits> EngineeringUnits { get; set; }
        public DbSet<MasterMachineComponents> Components { get; set; }
        
        public DbSet<MasterMachineType> MachineTypes { get; set; }
        public DbSet<MachineTypeComponents> MachineTypeComponents { get; set; }
        public DbSet<MasterMachine> Machines { get; set; }
        public DbSet<MachineComponents> MachineComponents { get; set; }

        public DbSet<QualityProperties> QualityProperties { get; set; }
        public DbSet<SKUCategories> SKUCategories { get; set; }
        public DbSet<SKUCategoryProperties> SKUCategoryProperties { get; set; }
        public DbSet<SKUSubcategories> SKUSubcategories { get; set; }
        public DbSet<SKUSubcategoryProperties> SKUSubcategoryProperties { get; set; }
        public DbSet<SKUSegments> SKUSegments { get; set; }
        public DbSet<SKUSegmentProperties> SKUSegmentProperties { get; set; }
        public DbSet<SKUBusinesses> SKUBusiness { get; set; }
        public DbSet<SKUBusinessProperties> SKUBusinessProperties { get; set; }
        public DbSet<MasterSKU> SKU { get; set; }
        public DbSet<SKUProperties> SKUProperties { get; set; }
        public DbSet<SKUPropertiesSetting> SKUPropertySettings { get; set; }

        public DbSet<MasterDefect> Defects { get; set; }
        public DbSet<DefectPhotos> DefectPhotos { get; set; }

        public DbSet<MasterBatch> Batches { get; set; }
        public DbSet<MasterProduct> Products { get; set; }
        public DbSet<ProductQuality> ProductQualities { get; set; }
        public DbSet<ProductDefect> ProductDefects { get; set; }

        public DbSet<MasterDowntime> Downtimes { get; set; }

        public DbSet<MasterOperativeAction> OperativeActions { get; set; }

        public DbSet<DowntimeLog> DowntimeLogs { get; set; }
        public DbSet<DowntimeAction> DowntimeActions { get; set; }

        public DbSet<HistorianTag> HistorianTags { get; set; }

        public DbSet<TagCompression> TagCompressions { get; set; }

        public DbSet<TagData> TagValues { get; set; }

        public DbSet<MasterStateMachine> StateMachines { get; set; }
        public DbSet<StateMachineState> StateMachineStates { get; set; }
        public DbSet<StateMachineVariable> StateMachineVariables { get; set; }
        public DbSet<StateMachineTransition> StateMachineTransitions { get; set; }

        public DbSet<SKUMachine> SKUMachineSettings { get; set; }
    }
}