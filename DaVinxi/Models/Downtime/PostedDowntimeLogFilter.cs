﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class PostedDowntimeLogFilter
    {
        public int company_id { get; set; }

        public string division_id { get; set; }

        public string block_id { get; set; }

        public string machine_id { get; set; }

        public DateTime start_date { get; set; }

        public DateTime finish_date { get; set; }
    }
}