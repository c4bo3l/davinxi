﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class DowntimeLog
    {
        public DowntimeLog()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Required]
        [Display(Name = "Company")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        [Display(Name = "Downtime Log ID")]
        [Index("DowntimeLogIndex")]
        public string downtime_log_id { get; set; }

        [Required]
        [Display(Name = "Start Time")]
        [Index("StartTimeIndex")]
        public DateTime start_time { get; set; }

        [Required]
        [Display(Name = "End Time")]
        [Index("EndTimeIndex")]
        public DateTime end_time { get; set; }

        [Required]
        [Display(Name = "Machine")]
        [Index("MachineIndex")]
        [MaxLength(128)]
        public string machine_id { get; set; }

        [Required]
        [Display(Name = "Primary Downtime Cause")]
        [Index("DowntimeIndex")]
        [MaxLength(128)]
        public string downtime_id { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}