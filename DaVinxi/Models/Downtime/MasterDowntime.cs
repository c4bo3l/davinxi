﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterDowntime
    {
        public MasterDowntime()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Column(Order = 0)]
        [Key]
        [Index("CompanyIndex")]
        [Required]
        [Display(Name="Company")]
        public int company_id { get; set; }

        [Column(Order = 1)]
        [Key]
        [Index("DowntimeIndex")]
        [Required]
        [Display(Name="Downtime ID")]
        public string downtime_id { get; set; }

        [Required]
        [Display(Name="Downtime Name")]
        public string downtime_name { get; set; }

        [Required]
        [Display(Name = "Downtime Category")]
        [Index("ParentDowntimeIndex")]
        [MaxLength(128)]
        public string parent_downtime_id { get; set; }

        [Display(Name="Description")]
        public string description { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}