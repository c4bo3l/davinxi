﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class DowntimeTrackingModel
    {
        public string downtime_log_id { get; set; }

        public DateTime start_time { get; set; }

        public DateTime end_time { get; set; }

        public int duration { get; set; }

        public string downtime_id { get; set; }

        public string downtime_name { get; set; }
    }
}