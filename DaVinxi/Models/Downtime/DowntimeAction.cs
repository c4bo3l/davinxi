﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class DowntimeAction
    {
        public DowntimeAction()
        {
            is_fixed_the_problem = false;
            created_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Required]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        [Index("DowntimeLogIndex")]
        public string downtime_log_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [Required]
        [Index("ActionIndex")]
        public string action_id { get; set; }

        [Required]
        [Index("FixProblemIndex")]
        public bool is_fixed_the_problem { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public string created_by { get; set; }
    }
}