﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class QualityPropRegisterModel
    {
        [Required]
        [Display(Name = "Company", Prompt = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Quality Property ID / Initial", Prompt = "Quality Property ID / Initial")]
        public string property_id { get; set; }

        [Required]
        [Display(Name = "Quality Property Name", Prompt = "Quality Property Name")]
        public string property_name { get; set; }

        [Required]
        [Display(Name = "Quality Unit", Prompt = "Quality Unit")]
        public string unit_id { get; set; }

        [Required]
        [Display(Name = "Test Method", Prompt = "Test Method")]
        public string test_method { get; set; }

        [Required]
        [Display(Name = "Reference", Prompt = "Reference")]
        public string reference { get; set; }

        [Display(Name="Defect when value greater than upper reject limit")]
        public string url_defect { get; set; }

        [Display(Name = "Defect when value below low reject limit")]
        public string lrl_defect { get; set; }
    }
}