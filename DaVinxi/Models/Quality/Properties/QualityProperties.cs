﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class QualityProperties
    {
        public QualityProperties()
        {
            created_date = revised_date = DateTime.Now;
            url_defect = lrl_defect = "";
        }

        [Key]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        [Display(Name = "Company", Prompt = "Company")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Index("PropertyIDIndex")]
        [Display(Name = "ID", Prompt = "ID")]
        public string property_id { get; set; }

        [Required]
        [Display(Name = "Name", Prompt = "Name")]
        public string property_name { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("UnitIndex")]
        [Display(Name = "Unit", Prompt = "Unit")]
        public string unit_id { get; set; }

        [Required]
        [Display(Name = "Test Method", Prompt = "Test Method")]
        public string test_method { get; set; }

        [Required]
        [Display(Name = "Reference", Prompt = "Reference")]
        public string reference { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }

        [MaxLength(128)]
        [Index("URLDefect")]
        public string url_defect { get; set; }

        [MaxLength(128)]
        [Index("LRLDefect")]
        public string lrl_defect { get; set; }
    }
}