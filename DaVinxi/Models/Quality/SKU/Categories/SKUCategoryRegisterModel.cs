﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class SKUCategoryRegisterModel
    {
        [Required]
        [Display(Name = "Company", Prompt = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Category ID / Initial", Prompt = "Category ID / Initial")]
        public string category_id { get; set; }

        [Required]
        [Display(Name = "Category Name", Prompt = "Category Name")]
        public string category_name { get; set; }
    }
}