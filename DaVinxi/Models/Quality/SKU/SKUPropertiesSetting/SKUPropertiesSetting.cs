﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class SKUPropertiesSetting
    {
        public SKUPropertiesSetting()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Display(Name = "Company")]
        [Index("CompanyIndex")]
        [Column(Order = 0)]
        [Required]
        public int company_id { get; set; }

        [Key]
        [Display(Name = "SKU")]
        [Index("SKUIndex")]
        [Column(Order = 1)]
        [Required]
        public string sku_id { get; set; }

        [Key]
        [Display(Name = "Property")]
        [Index("PropertyIndex")]
        [Column(Order = 2)]
        [Required]
        public string property_id { get; set; }

        [Required]
        [Display(Name = "Upper Reject Limit")]
        public double url { get; set; }

        [Required]
        [Display(Name = "Lower Reject Limit")]
        public double lrl { get; set; }

        [Required]
        [Display(Name = "Sigma")]
        public double sigma { get; set; }

        [Required]
        [Display(Name = "Sigma Multiplier")]
        public double sigma_multiplier { get; set; }

        [Required]
        [Display(Name = "Target")]
        public double target { get; set; }

        [Required]
        [Display(Name = "Upper Control Limit")]
        public double ucl { get; set; }

        [Required]
        [Display(Name = "Lower Control Limit")]
        public double lcl { get; set; }

        [Required]
        [Display(Name = "Upper Map")]
        public double upper_map { get; set; }

        [Required]
        [Display(Name = "Lower Map")]
        public double lower_map { get; set; }

        [Required]
        [Display(Name = "Property Weight")]
        public double weight { get; set; }

        [Required]
        [Display(Name = "Process Capability (Cp)")]
        public double cp { get; set; }

        [Required]
        [Display(Name = "Process Capability Index (Cpk)")]
        public double cpk { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}