﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class PostedSKUPropSimulation
    {
        public string property_id
        { get; set; }

        public double target
        { get; set; }

        public double sigma
        { get; set; }

        public double sigmax
        { get; set; }
    }
}