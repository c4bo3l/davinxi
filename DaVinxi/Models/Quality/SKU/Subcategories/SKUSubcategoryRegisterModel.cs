﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class SKUSubcategoryRegisterModel
    {
        [Required]
        [Display(Name = "Company", Prompt = "Prompt")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Subcategory ID / Initial", Prompt = "Subcategory ID / Initial")]
        public string subcategory_id { get; set; }

        [Required]
        [Display(Name = "Subcategory Name", Prompt = "Subcategory Name")]
        public string subcategory_name { get; set; }

        [Required]
        [Display(Name = "Category", Prompt = "Category")]
        public string category_id { get; set; }
    }
}