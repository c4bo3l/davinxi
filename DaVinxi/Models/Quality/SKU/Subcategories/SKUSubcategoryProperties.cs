﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class SKUSubcategoryProperties
    {
        public SKUSubcategoryProperties()
        {
            created_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Index("SubcategoryID")]
        public string subcategory_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [Index("PropertyID")]
        public string property_id { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}