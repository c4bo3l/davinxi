﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class SKUSubcategories
    {
        public SKUSubcategories()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Display(Name = "Company", Prompt = "Prompt")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Subcategory ID / Initial", Prompt = "Subcategory ID / Initial")]
        [Index("SubcategoryIndex")]
        public string subcategory_id { get; set; }

        [Required]
        [Display(Name = "Subcategory Name", Prompt = "Subcategory Name")]
        public string subcategory_name { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("CategoryIndex")]
        [Display(Name = "Category", Prompt = "Category")]
        public string category_id { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}