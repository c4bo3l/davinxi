﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class SKUBusinessRegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Business ID / Initial")]
        public string business_id { get; set; }

        [Required]
        [Display(Name = "Business Name")]
        public string business_name { get; set; }
    }
}