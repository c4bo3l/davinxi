﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class SKURegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "SKU ID")]
        public string sku_id { get; set; }

        [Required]
        [Display(Name = "SKU Name")]
        public string sku_name { get; set; }

        [Required]
        [MaxLength(128)]
        [Display(Name = "Subcategory")]
        public string subcategory_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Display(Name = "Segment")]
        public string segment_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Display(Name = "Business")]
        public string business_id { get; set; }

        [Required]
        [Display(Name = "Basis Weight")]
        public double basis_weight { get; set; }

        [Display(Name = "Additional ID")]
        [MaxLength(128)]
        public string additional_id { get; set; }

        [Display(Name = "Critical Quality Property")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string critical_quality_property { get; set; }

        [Display(Name = "Work Instruction")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string work_instruction { get; set; }
    }
}