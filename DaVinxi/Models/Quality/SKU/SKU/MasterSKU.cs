﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterSKU
    {
        public MasterSKU()
        {
            additional_id = critical_quality_property = work_instruction = "";
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Display(Name = "Company")]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "SKU ID")]
        [Index("SKUIDIndex")]
        public string sku_id { get; set; }

        [Required]
        [Display(Name = "SKU Name")]
        public string sku_name { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("SubcategoryIndex")]
        [Display(Name = "Subcategory")]
        public string subcategory_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("SegmentIndex")]
        [Display(Name="Segment")]
        public string segment_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("BusinessIndex")]
        [Display(Name = "Business")]
        public string business_id { get; set; }

        [Required]
        [Index("BWIndex")]
        [Display(Name = "Basis Weight")]
        public double basis_weight { get; set; }

        [Display(Name = "Additional ID")]
        [MaxLength(128)]
        [Index("AddIDIndex")]
        public string additional_id { get; set; }

        [Display(Name = "Critical Quality Property")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string critical_quality_property { get; set; }

        [Display(Name = "Work Instruction")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        public string work_instruction { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}