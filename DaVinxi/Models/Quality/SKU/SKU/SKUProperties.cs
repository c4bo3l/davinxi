﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class SKUProperties
    {
        public SKUProperties()
        {
            created_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Index("SKUIndex")]
        public string sku_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [Index("PropertyIndex")]
        public string property_id { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        public string created_by { get; set; }
    }
}