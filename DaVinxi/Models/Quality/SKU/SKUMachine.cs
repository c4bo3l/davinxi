﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class SKUMachine
    {
        [Key, Column(Order = 0), Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key, Column(Order = 1), Index("MachineIndex")]
        public string machine_id { get; set; }

        [Key, Column(Order = 2), Index("SKUIndex")]
        public string sku_id { get; set; }

        [Key, Column(Order = 3), Index("ComponentIndex")]
        public string component_id { get; set; }

        public double target { get; set; }

        public double min { get; set; }

        public double max { get; set; }

        public double lower_alarm_limit { get; set; }

        public double upper_alarm_limit { get; set; }

        public double rl { get; set; }

        public double rr { get; set; }

        public DateTime created_date { get; set; }

        public DateTime revised_date { get; set; }

        public string revised_by { get; set; }
    }
}