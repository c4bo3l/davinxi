﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DaVinxi.Models
{
    public class SKUSegmentRegisterModel
    {
        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Segment ID / Initial")]
        public string segment_id { get; set; }

        [Required]
        [Display(Name = "Segment Name")]
        public string segment_name { get; set; }
    }
}