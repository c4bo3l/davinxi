﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class DefectTrackingFilterModel
    {
        public int company_id { get; set; }
        
        public string division_id { get; set; }
        public bool chkDivision { get; set; }

        public string block_id { get; set; }
        public bool chkBlock { get; set; }

        public string machine_id { get; set; }
        public bool chkMachine { get; set; }

        public string category_id { get; set; }
        public bool chkCategory { get; set; }

        public string business_id { get; set; }
        public bool chkBusiness { get; set; }

        public string subcategory_id { get; set; }
        public bool chkSubcategory { get; set; }

        public string segment_id { get; set; }
        public bool chkSegment { get; set; }

        public string sku_id { get; set; }
        public bool chkSKU { get; set; }

        public string batch_id { get; set; }
        public bool chkBatch { get; set; }

        public int severity { get; set; }

        public string targetParentDefect { get; set; }

        public DateTime startDate { get; set; }
        
        public DateTime finishDate { get; set; }

        public double timeNumber { get; set; }

        public int timeGroupedType { get; set; }

        public double GetSeconds()
        {
            switch (timeGroupedType)
            {
                case 0: return 3600 * timeNumber;
                case 1: return 3600 * 24 * timeNumber;
                case 2: return 3600 * 24 * 7 * timeNumber;
                case 3: return 3600 * 24 * 30 * timeNumber;
                case 4: return 3600 * 24 * 365 * timeNumber;
                default: return 1;
            }
        }
    }
}