﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterDefect
    {
        public MasterDefect()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        [Display(Name = "Company")]
        [Required]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Defect ID")]
        [Required]
        public string defect_id { get; set; }

        [Required]
        [Display(Name = "Defect Name")]
        public string defect_name { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("ParentDefectIndex")]
        [Display(Name="Defect Category")]
        public string parent_defect_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("DataTypeIndex")]
        [Display(Name = "Data Type")]
        public string data_type { get; set; }

        [Display(Name = "Description (optional)")]
        public string description { get; set; }

        [Required]
        [Display(Name = "Test Method")]
        public string test_method { get; set; }

        [Display(Name = "PDF Reference File (optional)")]
        public string reference_file { get; set; }

        [Required]
        [Display(Name = "Pass Condition")]
        public string pass_condition { get; set; }

        [Required]
        [Display(Name = "Fail Condition")]
        public string fail_condition { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}