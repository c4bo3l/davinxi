﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class PChartDataModel
    {
        public int company_id { get; set; }

        public string xName { get; set; }

        public int timediff { get; set; }

        public int total_product { get; set; }

        public int count_defect_product { get; set; }

        public double fraction_defective { get; set; }

        public string formated_start_date { get; set; }
    }
}