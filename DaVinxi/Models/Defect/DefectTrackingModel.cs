﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class DefectTrackingModel
    {
        public int company_id { get; set; }

        public string defect_id { get; set; }

        public string defect_name { get; set; }

        public int sum_count { get; set; }

        public double sum_weight { get; set; }
    }
}