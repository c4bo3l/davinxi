﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class DefectPhotos
    {
        public DefectPhotos()
        {
            created_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        public string photo_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("DefectIndex")]
        public string defect_id { get; set; }

        [Required]
        public string image_path { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        public string created_by { get; set; }
    }
}