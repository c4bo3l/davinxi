﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterProduct
    {
        public MasterProduct()
        {
            start_date = finish_date = created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Required]
        [Index("CompanyIndex")]
        [Display(Name="Company")]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        [Index("ProductIndex")]
        [Display(Name = "Product ID")]
        public string product_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("BatchIndex")]
        [Display(Name = "Batch")]
        public string batch_id { get; set; }

        [Index("QIIndex")]
        [Display(Name = "Overall QI")]
        public double qi { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        [Display(Name = "Start date")]
        public DateTime start_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        [Display(Name = "Finish date")]
        public DateTime finish_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}