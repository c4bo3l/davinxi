﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class MasterBatch
    {
        public MasterBatch()
        {
            start_date = finish_date = created_date = revised_date = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        [Display(Name="Company")]
        [Required]
        public int company_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [Index("BatchIndex")]
        [Display(Name = "Batch ID")]
        [Required]
        public string batch_id { get; set; }

        [Required, Index("MachineIndex"), MaxLength(128)]
        public string machine_id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("SKUIndex")]
        public string sku_id { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        [Display(Name = "Start Date")]
        public DateTime start_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        [Display(Name = "Finish Date")]
        public DateTime finish_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime created_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}