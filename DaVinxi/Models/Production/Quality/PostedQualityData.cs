﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class PostedQualityData
    {
        public string property_id { get; set; }
        public double quality_value { get; set; }
        public double qi { get; set; }
    }
}