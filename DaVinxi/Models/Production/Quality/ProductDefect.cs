﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class ProductDefect
    {
        public ProductDefect()
        {
            start_date = revised_date = DateTime.Now;
        }

        [Key]
        [Required]
        [Column(Order = 0)]
        [Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key]
        [Required]
        [Column(Order = 1)]
        [Index("ProductIndex")]
        public string product_id { get; set; }

        [Key]
        [Required]
        [Column(Order = 2)]
        [Index("DefectIndex")]
        public string defect_id { get; set; }

        [Required]
        public double weight { get; set; }

        [Required]
        [Index("SeverityIndex")]
        public int severity { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime start_date { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}