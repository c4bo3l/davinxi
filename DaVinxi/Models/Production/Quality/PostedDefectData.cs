﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaVinxi.Models
{
    public class PostedDefectData
    {
        public string defect_id { get; set; }
        public double weight { get; set; }
        public int severity { get; set; }
    }
}