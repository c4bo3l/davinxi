﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class StateMachineTransition
    {
        public StateMachineTransition() {
            created_date = revised_date = DateTime.Now;
            sequence = -1;
        }

        [Key, Column(Order = 0), Required, Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key, Column(Order = 1), Required, Index("MasterIndex")]
        public string state_machine_id { get; set; }

        [Key, Column(Order = 2), Required, Index("SeqIndex")]
        public int sequence { get; set; }

        [Required, Index("SourceMasterIndex"), MaxLength(128)]
        public string source_state_machine_id { get; set; }

        [MaxLength(128),Index("SourceStateIndex")]
        public string source_state_id { get; set; }

        [Required, Index("TargetMasterIndex"), MaxLength(128)]
        public string target_state_machine_id { get; set; }

        [Required, MaxLength(128), Index("TargetStateIndex")]
        public string target_state_id { get; set; }

        [Required]
        public string condition { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}