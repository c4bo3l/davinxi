﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class StateMachineState
    {
        public StateMachineState()
        {
            created_date = revised_date = DateTime.Now;
        }

        [Key, Column(Order = 0), Required, Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key, Column(Order = 1), Required, Index("MasterIndex")]
        public string state_machine_id { get; set; }

        [Key, Column(Order = 2), Required, Index("StateIndex")]
        public string state_id { get; set; }

        [Required]
        public string state_name { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}