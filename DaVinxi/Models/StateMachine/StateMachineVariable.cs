﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DaVinxi.Models
{
    public class StateMachineVariable
    {
        public StateMachineVariable() {
            created_date = revised_date = DateTime.Now;
        }

        [Key, Column(Order = 0), Required, Index("CompanyIndex")]
        public int company_id { get; set; }

        [Key, Column(Order = 1), Required, Index("MasterIndex")]
        public string state_machine_id { get; set; }

        [Key, Column(Order = 2), Required, Index("VariableIndex")]
        public string variable_id { get; set; }

        [Required]
        public string variable_name { get; set; }

        /// <summary>
        /// 0 : number
        /// 1 : text
        /// </summary>
        [Required]
        public int data_type { get; set; }

        /// <summary>
        /// 0 : Historian or manual input
        /// 1 : Calculated input
        /// </summary>
        [Required]
        public int data_source { get; set; }

        /// <summary>
        /// Higher number has higher priority
        /// </summary>
        [Required]
        public int priority { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [Required]
        public DateTime revised_date { get; set; }

        [Required]
        public string revised_by { get; set; }
    }
}