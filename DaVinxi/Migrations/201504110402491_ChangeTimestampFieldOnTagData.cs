namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTimestampFieldOnTagData : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.TagDatas", "TimestampIndex");
            DropPrimaryKey("dbo.TagDatas");
            DropColumn("dbo.TagDatas", "timestamp");
            AddColumn("dbo.TagDatas", "timestamp", c => c.Double(nullable: false));
            //AlterColumn("dbo.TagDatas", "timestamp", c => c.Double(nullable: false));
            AddPrimaryKey("dbo.TagDatas", new[] { "company_id", "tag_id", "timestamp" });
            CreateIndex("dbo.TagDatas", "timestamp", name: "TimestampIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagDatas", "TimestampIndex");
            DropPrimaryKey("dbo.TagDatas");
            AlterColumn("dbo.TagDatas", "timestamp", c => c.DateTime(nullable: false));
            AddPrimaryKey("dbo.TagDatas", new[] { "company_id", "tag_id", "timestamp" });
            CreateIndex("dbo.TagDatas", "timestamp", name: "TimestampIndex");
        }
    }
}
