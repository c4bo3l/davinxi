namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateHistorianTag : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.historian_tag", "CompanyIndex");
            DropIndex("dbo.historian_tag", "TagIndex");
            DropIndex("dbo.historian_tag", "UoMIndex");
            DropIndex("dbo.historian_tag", "MachineIndex");
            CreateTable(
                "dbo.HistorianTags",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        tag_id = c.String(nullable: false, maxLength: 128),
                        tag_name = c.String(nullable: false, maxLength: 128),
                        tag_description = c.String(),
                        uom = c.String(nullable: false, maxLength: 128),
                        machine_id = c.String(nullable: false, maxLength: 128),
                        compression_method = c.String(nullable: false, maxLength: 128),
                        compression_bandwidth = c.Double(nullable: false),
                        expired_time = c.Double(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(),
                    })
                .PrimaryKey(t => new { t.company_id, t.tag_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.tag_id, name: "TagIndex")
                .Index(t => t.tag_name, name: "NameIndex")
                .Index(t => t.uom, name: "UoMIndex")
                .Index(t => t.machine_id, name: "MachineIndex")
                .Index(t => t.compression_method, name: "CompressionIndex");
            
            DropTable("dbo.historian_tag");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.historian_tag",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        tag_name = c.String(nullable: false, maxLength: 128),
                        tag_description = c.String(),
                        uom = c.String(nullable: false, maxLength: 128),
                        machine_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(),
                    })
                .PrimaryKey(t => new { t.company_id, t.tag_name });
            
            DropIndex("dbo.HistorianTags", "CompressionIndex");
            DropIndex("dbo.HistorianTags", "MachineIndex");
            DropIndex("dbo.HistorianTags", "UoMIndex");
            DropIndex("dbo.HistorianTags", "NameIndex");
            DropIndex("dbo.HistorianTags", "TagIndex");
            DropIndex("dbo.HistorianTags", "CompanyIndex");
            DropTable("dbo.HistorianTags");
            CreateIndex("dbo.historian_tag", "machine_id", name: "MachineIndex");
            CreateIndex("dbo.historian_tag", "uom", name: "UoMIndex");
            CreateIndex("dbo.historian_tag", "tag_name", name: "TagIndex");
            CreateIndex("dbo.historian_tag", "company_id", name: "CompanyIndex");
        }
    }
}
