namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasterBatches",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        batch_id = c.String(nullable: false, maxLength: 128),
                        sku_id = c.String(nullable: false, maxLength: 128),
                        start_date = c.DateTime(nullable: false),
                        finish_date = c.DateTime(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.batch_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.batch_id, name: "BatchIndex")
                .Index(t => t.sku_id, name: "SKUIndex");
            
            CreateTable(
                "dbo.MasterBlocks",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        block_id = c.String(nullable: false, maxLength: 128),
                        division_id = c.String(nullable: false),
                        block_name = c.String(nullable: false),
                        pic_username = c.String(nullable: false, maxLength: 128),
                        description = c.String(),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.block_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.pic_username, name: "PICIndex");
            
            CreateTable(
                "dbo.MasterCompanies",
                c => new
                    {
                        company_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false),
                        address = c.String(nullable: false),
                        phone_number = c.String(nullable: false),
                        fax_number = c.String(),
                        email = c.String(nullable: false),
                        notes = c.String(),
                        logo = c.String(),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.company_id);
            
            CreateTable(
                "dbo.MasterMachineComponents",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        component_id = c.String(nullable: false, maxLength: 128),
                        component_name = c.String(nullable: false),
                        unit_id = c.String(nullable: false, maxLength: 128),
                        is_numeric = c.Boolean(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.component_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.unit_id, name: "UnitIndex")
                .Index(t => t.is_numeric, name: "IsNumericIndex");
            
            CreateTable(
                "dbo.DefectPhotos",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        photo_id = c.String(nullable: false, maxLength: 128),
                        defect_id = c.String(nullable: false, maxLength: 128),
                        image_path = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        created_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.photo_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.defect_id, name: "DefectIndex");
            
            CreateTable(
                "dbo.MasterDefects",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        defect_id = c.String(nullable: false, maxLength: 128),
                        defect_name = c.String(nullable: false),
                        parent_defect_id = c.String(nullable: false, maxLength: 128),
                        data_type = c.String(nullable: false, maxLength: 128),
                        description = c.String(),
                        test_method = c.String(nullable: false),
                        reference_file = c.String(),
                        pass_condition = c.String(nullable: false),
                        fail_condition = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.defect_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.parent_defect_id, name: "ParentDefectIndex")
                .Index(t => t.data_type, name: "DataTypeIndex");
            
            CreateTable(
                "dbo.MasterDivisions",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        division_id = c.String(nullable: false, maxLength: 128),
                        division_name = c.String(nullable: false),
                        pic_username = c.String(nullable: false, maxLength: 128),
                        description = c.String(),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.division_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.pic_username, name: "PICIndex");
            
            CreateTable(
                "dbo.MasterEngineeringUnits",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        unit_id = c.String(nullable: false, maxLength: 128),
                        unit_name = c.String(nullable: false),
                        description = c.String(),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.unit_id })
                .Index(t => t.company_id, name: "CompanyIndex");
            
            CreateTable(
                "dbo.MachineComponents",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        machine_id = c.String(nullable: false, maxLength: 128),
                        component_id = c.String(nullable: false, maxLength: 128),
                        value = c.String(),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.machine_id, t.component_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.machine_id, name: "MachineIndex")
                .Index(t => t.component_id, name: "ComponentIndex");
            
            CreateTable(
                "dbo.MasterMachines",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        machine_type_id = c.String(nullable: false, maxLength: 128),
                        machine_id = c.String(nullable: false, maxLength: 128),
                        block_id = c.String(nullable: false, maxLength: 128),
                        machine_name = c.String(nullable: false),
                        installed_date = c.DateTime(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.machine_type_id, t.machine_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.machine_type_id, name: "MachineTypeIndex")
                .Index(t => t.machine_id, name: "MachineIndex")
                .Index(t => t.block_id, name: "BlockIndex")
                .Index(t => t.installed_date, name: "InstalledDateIndex");
            
            CreateTable(
                "dbo.MachineTypeComponents",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        machine_type_id = c.String(nullable: false, maxLength: 128),
                        component_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        created_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.machine_type_id, t.component_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.machine_type_id, name: "MachTypeIndex")
                .Index(t => t.component_id, name: "ComponentIndex");
            
            CreateTable(
                "dbo.MasterMachineTypes",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        machine_type_id = c.String(nullable: false, maxLength: 128),
                        machine_type_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.machine_type_id })
                .Index(t => t.company_id, name: "CompanyIndex");
            
            CreateTable(
                "dbo.ProductDefects",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        product_id = c.String(nullable: false, maxLength: 128),
                        defect_id = c.String(nullable: false, maxLength: 128),
                        weight = c.Double(nullable: false),
                        severity = c.Int(nullable: false),
                        start_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.product_id, t.defect_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.product_id, name: "ProductIndex")
                .Index(t => t.defect_id, name: "DefectIndex")
                .Index(t => t.severity, name: "SeverityIndex");
            
            CreateTable(
                "dbo.ProductQualities",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        product_id = c.String(nullable: false, maxLength: 128),
                        quality_property_id = c.String(nullable: false, maxLength: 128),
                        quality_value = c.Double(nullable: false),
                        qi = c.Double(nullable: false),
                        start_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.product_id, t.quality_property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.product_id, name: "ProductIndex")
                .Index(t => t.quality_property_id, name: "PropertyIndex");
            
            CreateTable(
                "dbo.MasterProducts",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        product_id = c.String(nullable: false, maxLength: 128),
                        batch_id = c.String(nullable: false, maxLength: 128),
                        machine_id = c.String(nullable: false, maxLength: 128),
                        qi = c.Double(nullable: false),
                        start_date = c.DateTime(nullable: false),
                        finish_date = c.DateTime(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.product_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.product_id, name: "ProductIndex")
                .Index(t => t.batch_id, name: "BatchIndex")
                .Index(t => t.machine_id, name: "MachineIndex")
                .Index(t => t.qi, name: "QIIndex");
            
            CreateTable(
                "dbo.QualityProperties",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        property_id = c.String(nullable: false, maxLength: 128),
                        property_name = c.String(nullable: false),
                        unit_id = c.String(nullable: false, maxLength: 128),
                        test_method = c.String(nullable: false),
                        reference = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                        url_defect = c.String(maxLength: 128),
                        lrl_defect = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.company_id, t.property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.property_id, name: "PropertyIDIndex")
                .Index(t => t.unit_id, name: "UnitIndex")
                .Index(t => t.url_defect, name: "URLDefect")
                .Index(t => t.lrl_defect, name: "LRLDefect");
            
            CreateTable(
                "dbo.MasterSKUs",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        sku_id = c.String(nullable: false, maxLength: 128),
                        sku_name = c.String(nullable: false),
                        subcategory_id = c.String(nullable: false, maxLength: 128),
                        segment_id = c.String(nullable: false, maxLength: 128),
                        business_id = c.String(nullable: false, maxLength: 128),
                        basis_weight = c.Double(nullable: false),
                        additional_id = c.String(maxLength: 128),
                        critical_quality_property = c.String(),
                        work_instruction = c.String(),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.sku_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.sku_id, name: "SKUIDIndex")
                .Index(t => t.subcategory_id, name: "SubcategoryIndex")
                .Index(t => t.segment_id, name: "SegmentIndex")
                .Index(t => t.business_id, name: "BusinessIndex")
                .Index(t => t.basis_weight, name: "BWIndex")
                .Index(t => t.additional_id, name: "AddIDIndex");
            
            CreateTable(
                "dbo.SKUBusinesses",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        business_id = c.String(nullable: false, maxLength: 128),
                        business_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.business_id })
                .Index(t => t.company_id, name: "CompanyIndex");
            
            CreateTable(
                "dbo.SKUBusinessProperties",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        business_id = c.String(nullable: false, maxLength: 128),
                        property_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        created_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.business_id, t.property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.business_id, name: "BusinessIndex")
                .Index(t => t.property_id, name: "PropertyIndex");
            
            CreateTable(
                "dbo.SKUCategories",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        category_id = c.String(nullable: false, maxLength: 128),
                        category_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.category_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.category_id, name: "CategoryIndex");
            
            CreateTable(
                "dbo.SKUCategoryProperties",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        category_id = c.String(nullable: false, maxLength: 128),
                        property_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        created_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.category_id, t.property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.category_id, name: "CategoryIndex")
                .Index(t => t.property_id, name: "PropertyIndex");
            
            CreateTable(
                "dbo.SKUProperties",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        sku_id = c.String(nullable: false, maxLength: 128),
                        property_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        created_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.sku_id, t.property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.sku_id, name: "SKUIndex")
                .Index(t => t.property_id, name: "PropertyIndex");
            
            CreateTable(
                "dbo.SKUPropertiesSettings",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        sku_id = c.String(nullable: false, maxLength: 128),
                        property_id = c.String(nullable: false, maxLength: 128),
                        url = c.Double(nullable: false),
                        lrl = c.Double(nullable: false),
                        sigma = c.Double(nullable: false),
                        sigma_multiplier = c.Double(nullable: false),
                        target = c.Double(nullable: false),
                        ucl = c.Double(nullable: false),
                        lcl = c.Double(nullable: false),
                        upper_map = c.Double(nullable: false),
                        lower_map = c.Double(nullable: false),
                        weight = c.Double(nullable: false),
                        cp = c.Double(nullable: false),
                        cpk = c.Double(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.sku_id, t.property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.sku_id, name: "SKUIndex")
                .Index(t => t.property_id, name: "PropertyIndex");
            
            CreateTable(
                "dbo.SKUSegmentProperties",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        segment_id = c.String(nullable: false, maxLength: 128),
                        property_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        created_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.segment_id, t.property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.segment_id, name: "SegmentIndex")
                .Index(t => t.property_id, name: "PropertyIndex");
            
            CreateTable(
                "dbo.SKUSegments",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        segment_id = c.String(nullable: false, maxLength: 128),
                        segment_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.segment_id })
                .Index(t => t.company_id, name: "CompanyIndex");
            
            CreateTable(
                "dbo.SKUSubcategories",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        subcategory_id = c.String(nullable: false, maxLength: 128),
                        subcategory_name = c.String(nullable: false),
                        category_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.subcategory_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.subcategory_id, name: "SubcategoryIndex")
                .Index(t => t.category_id, name: "CategoryIndex");
            
            CreateTable(
                "dbo.SKUSubcategoryProperties",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        subcategory_id = c.String(nullable: false, maxLength: 128),
                        property_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.subcategory_id, t.property_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.subcategory_id, name: "SubcategoryID")
                .Index(t => t.property_id, name: "PropertyID");
            
            CreateTable(
                "dbo.MasterUsers",
                c => new
                    {
                        username = c.String(nullable: false, maxLength: 128),
                        password = c.String(nullable: false, maxLength: 100),
                        full_name = c.String(nullable: false),
                        email = c.String(nullable: false),
                        phone_number = c.String(nullable: false),
                        department = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                        company_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.username)
                .Index(t => t.company_id, name: "CompanyIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.MasterUsers", "CompanyIndex");
            DropIndex("dbo.SKUSubcategoryProperties", "PropertyID");
            DropIndex("dbo.SKUSubcategoryProperties", "SubcategoryID");
            DropIndex("dbo.SKUSubcategoryProperties", "CompanyIndex");
            DropIndex("dbo.SKUSubcategories", "CategoryIndex");
            DropIndex("dbo.SKUSubcategories", "SubcategoryIndex");
            DropIndex("dbo.SKUSubcategories", "CompanyIndex");
            DropIndex("dbo.SKUSegments", "CompanyIndex");
            DropIndex("dbo.SKUSegmentProperties", "PropertyIndex");
            DropIndex("dbo.SKUSegmentProperties", "SegmentIndex");
            DropIndex("dbo.SKUSegmentProperties", "CompanyIndex");
            DropIndex("dbo.SKUPropertiesSettings", "PropertyIndex");
            DropIndex("dbo.SKUPropertiesSettings", "SKUIndex");
            DropIndex("dbo.SKUPropertiesSettings", "CompanyIndex");
            DropIndex("dbo.SKUProperties", "PropertyIndex");
            DropIndex("dbo.SKUProperties", "SKUIndex");
            DropIndex("dbo.SKUProperties", "CompanyIndex");
            DropIndex("dbo.SKUCategoryProperties", "PropertyIndex");
            DropIndex("dbo.SKUCategoryProperties", "CategoryIndex");
            DropIndex("dbo.SKUCategoryProperties", "CompanyIndex");
            DropIndex("dbo.SKUCategories", "CategoryIndex");
            DropIndex("dbo.SKUCategories", "CompanyIndex");
            DropIndex("dbo.SKUBusinessProperties", "PropertyIndex");
            DropIndex("dbo.SKUBusinessProperties", "BusinessIndex");
            DropIndex("dbo.SKUBusinessProperties", "CompanyIndex");
            DropIndex("dbo.SKUBusinesses", "CompanyIndex");
            DropIndex("dbo.MasterSKUs", "AddIDIndex");
            DropIndex("dbo.MasterSKUs", "BWIndex");
            DropIndex("dbo.MasterSKUs", "BusinessIndex");
            DropIndex("dbo.MasterSKUs", "SegmentIndex");
            DropIndex("dbo.MasterSKUs", "SubcategoryIndex");
            DropIndex("dbo.MasterSKUs", "SKUIDIndex");
            DropIndex("dbo.MasterSKUs", "CompanyIndex");
            DropIndex("dbo.QualityProperties", "LRLDefect");
            DropIndex("dbo.QualityProperties", "URLDefect");
            DropIndex("dbo.QualityProperties", "UnitIndex");
            DropIndex("dbo.QualityProperties", "PropertyIDIndex");
            DropIndex("dbo.QualityProperties", "CompanyIndex");
            DropIndex("dbo.MasterProducts", "QIIndex");
            DropIndex("dbo.MasterProducts", "MachineIndex");
            DropIndex("dbo.MasterProducts", "BatchIndex");
            DropIndex("dbo.MasterProducts", "ProductIndex");
            DropIndex("dbo.MasterProducts", "CompanyIndex");
            DropIndex("dbo.ProductQualities", "PropertyIndex");
            DropIndex("dbo.ProductQualities", "ProductIndex");
            DropIndex("dbo.ProductQualities", "CompanyIndex");
            DropIndex("dbo.ProductDefects", "SeverityIndex");
            DropIndex("dbo.ProductDefects", "DefectIndex");
            DropIndex("dbo.ProductDefects", "ProductIndex");
            DropIndex("dbo.ProductDefects", "CompanyIndex");
            DropIndex("dbo.MasterMachineTypes", "CompanyIndex");
            DropIndex("dbo.MachineTypeComponents", "ComponentIndex");
            DropIndex("dbo.MachineTypeComponents", "MachTypeIndex");
            DropIndex("dbo.MachineTypeComponents", "CompanyIndex");
            DropIndex("dbo.MasterMachines", "InstalledDateIndex");
            DropIndex("dbo.MasterMachines", "BlockIndex");
            DropIndex("dbo.MasterMachines", "MachineIndex");
            DropIndex("dbo.MasterMachines", "MachineTypeIndex");
            DropIndex("dbo.MasterMachines", "CompanyIndex");
            DropIndex("dbo.MachineComponents", "ComponentIndex");
            DropIndex("dbo.MachineComponents", "MachineIndex");
            DropIndex("dbo.MachineComponents", "CompanyIndex");
            DropIndex("dbo.MasterEngineeringUnits", "CompanyIndex");
            DropIndex("dbo.MasterDivisions", "PICIndex");
            DropIndex("dbo.MasterDivisions", "CompanyIndex");
            DropIndex("dbo.MasterDefects", "DataTypeIndex");
            DropIndex("dbo.MasterDefects", "ParentDefectIndex");
            DropIndex("dbo.MasterDefects", "CompanyIndex");
            DropIndex("dbo.DefectPhotos", "DefectIndex");
            DropIndex("dbo.DefectPhotos", "CompanyIndex");
            DropIndex("dbo.MasterMachineComponents", "IsNumericIndex");
            DropIndex("dbo.MasterMachineComponents", "UnitIndex");
            DropIndex("dbo.MasterMachineComponents", "CompanyIndex");
            DropIndex("dbo.MasterBlocks", "PICIndex");
            DropIndex("dbo.MasterBlocks", "CompanyIndex");
            DropIndex("dbo.MasterBatches", "SKUIndex");
            DropIndex("dbo.MasterBatches", "BatchIndex");
            DropIndex("dbo.MasterBatches", "CompanyIndex");
            DropTable("dbo.MasterUsers");
            DropTable("dbo.SKUSubcategoryProperties");
            DropTable("dbo.SKUSubcategories");
            DropTable("dbo.SKUSegments");
            DropTable("dbo.SKUSegmentProperties");
            DropTable("dbo.SKUPropertiesSettings");
            DropTable("dbo.SKUProperties");
            DropTable("dbo.SKUCategoryProperties");
            DropTable("dbo.SKUCategories");
            DropTable("dbo.SKUBusinessProperties");
            DropTable("dbo.SKUBusinesses");
            DropTable("dbo.MasterSKUs");
            DropTable("dbo.QualityProperties");
            DropTable("dbo.MasterProducts");
            DropTable("dbo.ProductQualities");
            DropTable("dbo.ProductDefects");
            DropTable("dbo.MasterMachineTypes");
            DropTable("dbo.MachineTypeComponents");
            DropTable("dbo.MasterMachines");
            DropTable("dbo.MachineComponents");
            DropTable("dbo.MasterEngineeringUnits");
            DropTable("dbo.MasterDivisions");
            DropTable("dbo.MasterDefects");
            DropTable("dbo.DefectPhotos");
            DropTable("dbo.MasterMachineComponents");
            DropTable("dbo.MasterCompanies");
            DropTable("dbo.MasterBlocks");
            DropTable("dbo.MasterBatches");
        }
    }
}
