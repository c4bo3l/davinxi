namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDowntimeLogTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DowntimeLogs",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        downtime_log_id = c.String(nullable: false, maxLength: 128),
                        start_time = c.DateTime(nullable: false),
                        end_time = c.DateTime(nullable: false),
                        machine_id = c.String(nullable: false, maxLength: 128),
                        downtime_id = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.downtime_log_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.downtime_log_id, name: "DowntimeLogIndex")
                .Index(t => t.start_time, name: "StartTimeIndex")
                .Index(t => t.end_time, name: "EndTimeIndex")
                .Index(t => t.machine_id, name: "MachineIndex")
                .Index(t => t.downtime_id, name: "DowntimeIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.DowntimeLogs", "DowntimeIndex");
            DropIndex("dbo.DowntimeLogs", "MachineIndex");
            DropIndex("dbo.DowntimeLogs", "EndTimeIndex");
            DropIndex("dbo.DowntimeLogs", "StartTimeIndex");
            DropIndex("dbo.DowntimeLogs", "DowntimeLogIndex");
            DropIndex("dbo.DowntimeLogs", "CompanyIndex");
            DropTable("dbo.DowntimeLogs");
        }
    }
}
