namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSKUMachineTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SKUMachines",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        machine_id = c.String(nullable: false, maxLength: 128),
                        sku_id = c.String(nullable: false, maxLength: 128),
                        component_id = c.String(nullable: false, maxLength: 128),
                        target = c.Double(nullable: false),
                        min = c.Double(nullable: false),
                        max = c.Double(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(),
                    })
                .PrimaryKey(t => new { t.company_id, t.machine_id, t.sku_id, t.component_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.machine_id, name: "MachineIndex")
                .Index(t => t.sku_id, name: "SKUIndex")
                .Index(t => t.component_id, name: "ComponentIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.SKUMachines", "ComponentIndex");
            DropIndex("dbo.SKUMachines", "SKUIndex");
            DropIndex("dbo.SKUMachines", "MachineIndex");
            DropIndex("dbo.SKUMachines", "CompanyIndex");
            DropTable("dbo.SKUMachines");
        }
    }
}
