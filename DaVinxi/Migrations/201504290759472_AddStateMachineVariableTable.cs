namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStateMachineVariableTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StateMachineVariables",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        state_machine_id = c.String(nullable: false, maxLength: 128),
                        variable_id = c.String(nullable: false, maxLength: 128),
                        variable_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.state_machine_id, t.variable_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.state_machine_id, name: "MasterIndex")
                .Index(t => t.variable_id, name: "VariableIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StateMachineVariables", "VariableIndex");
            DropIndex("dbo.StateMachineVariables", "MasterIndex");
            DropIndex("dbo.StateMachineVariables", "CompanyIndex");
            DropTable("dbo.StateMachineVariables");
        }
    }
}
