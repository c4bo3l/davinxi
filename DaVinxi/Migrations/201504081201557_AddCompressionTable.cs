namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompressionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TagCompressions",
                c => new
                    {
                        compression_id = c.String(nullable: false, maxLength: 128),
                        compression_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.compression_id)
                .Index(t => t.compression_id, name: "CompressionIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagCompressions", "CompressionIndex");
            DropTable("dbo.TagCompressions");
        }
    }
}
