namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMachineDataToTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.historian_tag", "machine_id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.historian_tag", "machine_id", name: "MachineIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.historian_tag", "MachineIndex");
            DropColumn("dbo.historian_tag", "machine_id");
        }
    }
}
