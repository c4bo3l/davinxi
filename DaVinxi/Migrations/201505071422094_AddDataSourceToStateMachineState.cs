namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataSourceToStateMachineState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StateMachineStates", "data_source", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StateMachineStates", "data_source");
        }
    }
}
