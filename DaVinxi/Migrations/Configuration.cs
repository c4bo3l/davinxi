namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DaVinxi.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<DaVinxi.Models.MyDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DaVinxi.Models.MyDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Users.AddOrUpdate(u => u.username,
                new MasterUser
                {
                    username = "cabz",
                    password = Constant.AESSecure.Encrypt("administrator"),
                    full_name = "CabZ BullZ",
                    email = "c4bo3l@frameworxs.com",
                    phone_number = "+628156110901",
                    department = "Web Administrator",
                    revised_by = "cabz",
                    company_id = -1
                }
            );

            context.TagCompressions.AddOrUpdate(t => t.compression_id,
                new TagCompression
                {
                    compression_id = "-",
                    compression_name = "None",
                    revised_by = "cabz",
                },
                new TagCompression
                {
                    compression_id = "db",
                    compression_name = "Deadband",
                    revised_by = "cabz",
                },
                new TagCompression
                {
                    compression_id = "ffp",
                    compression_name = "Farthest Feasible Point",
                    revised_by = "cabz",
                },
                new TagCompression
                {
                    compression_id = "sd",
                    compression_name = "Swinging Door",
                    revised_by = "cabz",
                },
                new TagCompression
                {
                    compression_id = "th",
                    compression_name = "Threshold",
                    revised_by = "cabz",
                }
            );
        }
    }
}
