namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDowntimeActionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DowntimeActions",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        downtime_log_id = c.String(nullable: false, maxLength: 128),
                        action_id = c.String(nullable: false, maxLength: 128),
                        is_fixed_the_problem = c.Boolean(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        created_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.downtime_log_id, t.action_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.downtime_log_id, name: "DowntimeLogIndex")
                .Index(t => t.action_id, name: "ActionIndex")
                .Index(t => t.is_fixed_the_problem, name: "FixProblemIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.DowntimeActions", "FixProblemIndex");
            DropIndex("dbo.DowntimeActions", "ActionIndex");
            DropIndex("dbo.DowntimeActions", "DowntimeLogIndex");
            DropIndex("dbo.DowntimeActions", "CompanyIndex");
            DropTable("dbo.DowntimeActions");
        }
    }
}
