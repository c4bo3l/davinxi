// <auto-generated />
namespace DaVinxi.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddDataTypeToStateVariable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddDataTypeToStateVariable));
        
        string IMigrationMetadata.Id
        {
            get { return "201505011056338_AddDataTypeToStateVariable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
