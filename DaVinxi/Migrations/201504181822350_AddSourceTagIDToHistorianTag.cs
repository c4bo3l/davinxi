namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSourceTagIDToHistorianTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HistorianTags", "source_tag_id", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HistorianTags", "source_tag_id");
        }
    }
}
