namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOperativeActionTable1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MasterOperativeActions", "note", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MasterOperativeActions", "note");
        }
    }
}
