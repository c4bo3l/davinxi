namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLastDateToHistorianTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HistorianTags", "last_date_data", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HistorianTags", "last_date_data");
        }
    }
}
