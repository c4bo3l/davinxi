namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStateMachineTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasterStateMachines",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        state_machine_id = c.String(nullable: false, maxLength: 128),
                        state_machine_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.state_machine_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.state_machine_id, name: "StateMachineIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.MasterStateMachines", "StateMachineIndex");
            DropIndex("dbo.MasterStateMachines", "CompanyIndex");
            DropTable("dbo.MasterStateMachines");
        }
    }
}
