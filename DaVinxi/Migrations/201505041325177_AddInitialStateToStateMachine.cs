namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInitialStateToStateMachine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MasterStateMachines", "initial_state", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.MasterStateMachines", "initial_state", name: "InitialStateIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MasterStateMachines", "InitialStateIndex");
            DropColumn("dbo.MasterStateMachines", "initial_state");
        }
    }
}
