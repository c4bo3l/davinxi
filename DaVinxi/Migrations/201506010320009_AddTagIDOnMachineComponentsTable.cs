namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTagIDOnMachineComponentsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MachineComponents", "tag_id", c => c.String(maxLength: 128));
            CreateIndex("dbo.MachineComponents", "tag_id", name: "TagIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MachineComponents", "TagIndex");
            DropColumn("dbo.MachineComponents", "tag_id");
        }
    }
}
