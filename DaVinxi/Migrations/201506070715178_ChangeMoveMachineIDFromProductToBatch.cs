namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeMoveMachineIDFromProductToBatch : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.MasterProducts", "MachineIndex");
            AddColumn("dbo.MasterBatches", "machine_id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.MasterBatches", "machine_id", name: "MachineIndex");
            DropColumn("dbo.MasterProducts", "machine_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MasterProducts", "machine_id", c => c.String(nullable: false, maxLength: 128));
            DropIndex("dbo.MasterBatches", "MachineIndex");
            DropColumn("dbo.MasterBatches", "machine_id");
            CreateIndex("dbo.MasterProducts", "machine_id", name: "MachineIndex");
        }
    }
}
