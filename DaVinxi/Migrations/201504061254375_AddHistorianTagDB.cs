namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHistorianTagDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.historian_tag",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        tag_name = c.String(nullable: false, maxLength: 128),
                        tag_description = c.String(),
                        uom = c.String(nullable: false, maxLength: 128),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(),
                    })
                .PrimaryKey(t => new { t.company_id, t.tag_name })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.tag_name, name: "TagIndex")
                .Index(t => t.uom, name: "UoMIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.historian_tag", "UoMIndex");
            DropIndex("dbo.historian_tag", "TagIndex");
            DropIndex("dbo.historian_tag", "CompanyIndex");
            DropTable("dbo.historian_tag");
        }
    }
}
