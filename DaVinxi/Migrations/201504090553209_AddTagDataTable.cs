namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTagDataTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TagDatas",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        tag_id = c.String(nullable: false, maxLength: 128),
                        timestamp = c.DateTime(nullable: false),
                        value = c.Double(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.tag_id, t.timestamp })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.tag_id, name: "TagIndex")
                .Index(t => t.timestamp, name: "TimestampIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagDatas", "TimestampIndex");
            DropIndex("dbo.TagDatas", "TagIndex");
            DropIndex("dbo.TagDatas", "CompanyIndex");
            DropTable("dbo.TagDatas");
        }
    }
}
