namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataTypeToStateVariable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StateMachineVariables", "data_type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StateMachineVariables", "data_type");
        }
    }
}
