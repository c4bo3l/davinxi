namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOperativeActionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasterOperativeActions",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        action_id = c.String(nullable: false, maxLength: 128),
                        action_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.action_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.action_id, name: "ActionIDIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.MasterOperativeActions", "ActionIDIndex");
            DropIndex("dbo.MasterOperativeActions", "CompanyIndex");
            DropTable("dbo.MasterOperativeActions");
        }
    }
}
