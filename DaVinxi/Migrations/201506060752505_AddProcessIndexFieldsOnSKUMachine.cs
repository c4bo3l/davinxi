namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProcessIndexFieldsOnSKUMachine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SKUMachines", "lower_alarm_limit", c => c.Double(nullable: false));
            AddColumn("dbo.SKUMachines", "upper_alarm_limit", c => c.Double(nullable: false));
            AddColumn("dbo.SKUMachines", "rl", c => c.Double(nullable: false));
            AddColumn("dbo.SKUMachines", "rr", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SKUMachines", "rr");
            DropColumn("dbo.SKUMachines", "rl");
            DropColumn("dbo.SKUMachines", "upper_alarm_limit");
            DropColumn("dbo.SKUMachines", "lower_alarm_limit");
        }
    }
}
