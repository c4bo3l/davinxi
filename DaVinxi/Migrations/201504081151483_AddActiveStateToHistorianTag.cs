namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActiveStateToHistorianTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HistorianTags", "active", c => c.Boolean(nullable: false));
            CreateIndex("dbo.HistorianTags", "active", name: "ActiveIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.HistorianTags", "ActiveIndex");
            DropColumn("dbo.HistorianTags", "active");
        }
    }
}
