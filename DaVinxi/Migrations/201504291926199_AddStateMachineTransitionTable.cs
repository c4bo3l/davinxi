namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStateMachineTransitionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StateMachineTransitions",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        state_machine_id = c.String(nullable: false, maxLength: 128),
                        sequence = c.Int(nullable: false),
                        source_state_machine_id = c.String(nullable: false, maxLength: 128),
                        source_state_id = c.String(maxLength: 128),
                        target_state_machine_id = c.String(nullable: false, maxLength: 128),
                        target_state_id = c.String(nullable: false, maxLength: 128),
                        condition = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.state_machine_id, t.sequence })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.state_machine_id, name: "MasterIndex")
                .Index(t => t.sequence, name: "SeqIndex")
                .Index(t => t.source_state_machine_id, name: "SourceMasterIndex")
                .Index(t => t.source_state_id, name: "SourceStateIndex")
                .Index(t => t.target_state_machine_id, name: "TargetMasterIndex")
                .Index(t => t.target_state_id, name: "TargetStateIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StateMachineTransitions", "TargetStateIndex");
            DropIndex("dbo.StateMachineTransitions", "TargetMasterIndex");
            DropIndex("dbo.StateMachineTransitions", "SourceStateIndex");
            DropIndex("dbo.StateMachineTransitions", "SourceMasterIndex");
            DropIndex("dbo.StateMachineTransitions", "SeqIndex");
            DropIndex("dbo.StateMachineTransitions", "MasterIndex");
            DropIndex("dbo.StateMachineTransitions", "CompanyIndex");
            DropTable("dbo.StateMachineTransitions");
        }
    }
}
