namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDowntimeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasterDowntimes",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        downtime_id = c.String(nullable: false, maxLength: 128),
                        downtime_name = c.String(nullable: false),
                        parent_downtime_id = c.String(nullable: false, maxLength: 128),
                        description = c.String(),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.downtime_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.downtime_id, name: "DowntimeIndex")
                .Index(t => t.parent_downtime_id, name: "ParentDowntimeIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.MasterDowntimes", "ParentDowntimeIndex");
            DropIndex("dbo.MasterDowntimes", "DowntimeIndex");
            DropIndex("dbo.MasterDowntimes", "CompanyIndex");
            DropTable("dbo.MasterDowntimes");
        }
    }
}
