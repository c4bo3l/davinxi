namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStateMachineStateTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StateMachineStates",
                c => new
                    {
                        company_id = c.Int(nullable: false),
                        state_machine_id = c.String(nullable: false, maxLength: 128),
                        state_id = c.String(nullable: false, maxLength: 128),
                        state_name = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        revised_date = c.DateTime(nullable: false),
                        revised_by = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company_id, t.state_machine_id, t.state_id })
                .Index(t => t.company_id, name: "CompanyIndex")
                .Index(t => t.state_machine_id, name: "MasterIndex")
                .Index(t => t.state_id, name: "StateIndex");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StateMachineStates", "StateIndex");
            DropIndex("dbo.StateMachineStates", "MasterIndex");
            DropIndex("dbo.StateMachineStates", "CompanyIndex");
            DropTable("dbo.StateMachineStates");
        }
    }
}
