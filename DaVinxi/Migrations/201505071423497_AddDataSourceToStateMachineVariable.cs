namespace DaVinxi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataSourceToStateMachineVariable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StateMachineVariables", "data_source", c => c.Int(nullable: false));
            DropColumn("dbo.StateMachineStates", "data_source");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StateMachineStates", "data_source", c => c.Int(nullable: false));
            DropColumn("dbo.StateMachineVariables", "data_source");
        }
    }
}
