﻿using System.Web.Optimization;

namespace DaVinxi
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", "~/Scripts/moment*", "~/Scripts/bootstrap*",
                        "~/Scripts/chosen*", "~/Scripts/qrcode*"));

            bundles.Add(new ScriptBundle("~/bundles/state-machine").Include("~/Scripts/jquery.hash-table*", 
                "~/Scripts/jquery.state-machine*"));

            bundles.Add(new ScriptBundle("~/bundles/base64").Include("~/Scripts/Base64*"));

            bundles.Add(new ScriptBundle("~/bundles/jMigrate").Include("~/Scripts/jquery-migrate-1.2.1*"));
            bundles.Add(new ScriptBundle("~/bundles/jPrintElement").Include("~/Scripts/jquery.printElement*"));

            bundles.Add(new ScriptBundle("~/bundles/highstock").Include(
                        "~/Scripts/Highstock/highstock*", "~/Scripts/Highstock/Modules/highstock*"));

            bundles.Add(new ScriptBundle("~/bundles/highcharts").Include(
                        "~/Scripts/Highcharts/highcharts*", "~/Scripts/Highcharts/Modules/highcharts*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            /*bundles.Add(new ScriptBundle("~/bundles/signalr").Include(
                        "~/Scripts/jquery.signalR-*"));*/

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap*"));
            bundles.Add(new StyleBundle("~/Content/custom").Include("~/Content/custom*"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/*.css"));
        }
    }
}