﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class DowntimeLogController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: DowntimeLog
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize, HttpPost]
        public ActionResult GetDowntimeLog(PostedDowntimeLogFilter filter)
        {
            var Log = from downtimeLog in DB.DowntimeLogs
                      join machine in DB.Machines on
                        new { c1 = downtimeLog.company_id, c2 = downtimeLog.machine_id } equals
                        new { c1 = machine.company_id, c2 = machine.machine_id }
                      join block in DB.Blocks on
                        new { c1 = machine.company_id, c2 = machine.block_id } equals
                        new { c1 = block.company_id, c2 = block.block_id }
                      join division in DB.Divisions on
                        new { c1 = block.company_id, c2 = block.division_id } equals
                        new { c1 = division.company_id, c2 = division.division_id }
                      join downtime in DB.Downtimes on
                        new { c1 = downtimeLog.company_id, c2 = downtimeLog.downtime_id } equals
                        new { c1 = downtime.company_id, c2 = downtime.downtime_id }
                      join downtimeAction in DB.DowntimeActions on
                        new { c1 = downtimeLog.company_id, c2 = downtimeLog.downtime_log_id } equals
                        new { c1 = downtimeAction.company_id, c2 = downtimeAction.downtime_log_id }
                      into rs
                      from result in rs.DefaultIfEmpty()
                      join oAction in DB.OperativeActions on
                        new { c1 = result.company_id, c2 = result.action_id } equals
                        new { c1 = oAction.company_id, c2 = oAction.action_id }
                      into rs2
                      from result2 in rs2.DefaultIfEmpty()
                      select new
                      {
                          company_id = downtimeLog.company_id,
                          division_id = division.division_id,
                          block_id = block.block_id,
                          machine_id = machine.machine_id,
                          machine_name = machine.machine_name,
                          downtime_log_id = downtimeLog.downtime_log_id,
                          downtime_id = downtimeLog.downtime_id,
                          downtime_name = downtime.downtime_name,
                          start_time = downtimeLog.start_time,
                          end_time = downtimeLog.end_time,
                          action = result == null ? null : new
                          {
                              action_id = result.action_id,
                              action_name = result2.action_name,
                              is_fixed = result.is_fixed_the_problem
                          }
                      };

            if (filter != null)
            {
                if (filter.company_id > 0)
                    Log = Log.Where(l => l.company_id == filter.company_id);

                if (filter.division_id != null && filter.division_id.Length > 0)
                    Log = Log.Where(l => string.Compare(l.division_id, filter.division_id, true) == 0);

                if (filter.block_id != null && filter.block_id.Length > 0)
                    Log = Log.Where(l => string.Compare(l.block_id, filter.block_id, true) == 0);

                if (filter.machine_id != null && filter.machine_id.Length > 0)
                    Log = Log.Where(l => string.Compare(l.machine_id, filter.machine_id, true) == 0);

                Log = Log.Where(l => l.start_time >= filter.start_date && 
                    l.start_time <= filter.finish_date);
            }

            return Json(Log.OrderByDescending(l => l.start_time).ThenBy(l => l.downtime_log_id).ThenBy(l => l.action.action_name).ToList().Select(l => new
            {
                company_id = l.company_id,
                division_id = l.division_id,
                block_id = l.block_id,
                machine_id = l.machine_id,
                machine_name = l.machine_name,
                downtime_log_id = l.downtime_log_id,
                downtime_id = l.downtime_id,
                downtime_name = l.downtime_name,
                start_time = l.start_time.ToString("MMMM dd yyyy HH:mm"),
                end_time = l.end_time.ToString("MMMM dd yyyy HH:mm"),
                action = l.action == null ? null : new
                {
                    action_id = l.action.action_id,
                    action_name = l.action.action_name,
                    is_fixed = l.action.is_fixed ? 1 : 0
                },
                duration = Math.Round(l.end_time.Subtract(l.start_time).TotalMinutes, 2, MidpointRounding.AwayFromZero)
            }));
        }

        [Authorize]
        public ActionResult Register(int companyID)
        {
            DowntimeLog NewLog = new DowntimeLog();
            NewLog.downtime_log_id = "replace this";
            NewLog.company_id = companyID;
            NewLog.revised_by = User.Identity.Name;
            return View(NewLog);
        }

        [Authorize, HttpPost]
        public ActionResult Register(DowntimeLog registeredLog, IEnumerable<DowntimeAction> downtime_action)
        {
            try
            {
                registeredLog.downtime_log_id = string.Format("{0}_{1}_{2}", registeredLog.machine_id,
                        registeredLog.start_time.ToString("yyyyMMddHHmmss"),
                        registeredLog.end_time.ToString("yyyyMMddHHmmss"));
                if (DB.DowntimeLogs.Where(d => d.company_id == registeredLog.company_id &&
                    string.Compare(d.downtime_log_id, registeredLog.downtime_log_id, true) == 0).Count() <= 0)
                {
                    DB.DowntimeLogs.Add(registeredLog);

                    if (downtime_action != null && downtime_action.Count() > 0)
                    {
                        List<string> LstActions = new List<string>();
                        foreach (DowntimeAction action in downtime_action)
                        {
                            if (DB.DowntimeActions.Where(d => d.company_id == registeredLog.company_id &&
                                string.Compare(d.downtime_log_id, registeredLog.downtime_log_id, true) == 0 &&
                                string.Compare(d.action_id, action.action_id, true) == 0).Count() <= 0 &&
                                LstActions.Where(l => string.Compare(l, action.action_id, true) == 0).Count() <= 0)
                            {
                                action.company_id = registeredLog.company_id;
                                action.downtime_log_id = registeredLog.downtime_log_id;
                                action.created_by = User.Identity.Name;
                                DB.DowntimeActions.Add(action);
                                LstActions.Add(action.action_id);
                            }
                        }
                    }

                    DB.SaveChanges();

                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "Log already exist");
            }
            catch (Exception ex)
            {
                DB.DetachAll();
                ModelState.AddModelError("ex", ex.Message);
            }
            return View(registeredLog);
        }

        [Authorize]
        public ActionResult Update(int company_id, string downtime_log_id)
        {
            DowntimeLog Log = DB.DowntimeLogs.Where(l => l.company_id == company_id && 
                string.Compare(l.downtime_log_id, downtime_log_id, true) == 0).FirstOrDefault();
            if (Log == null)
                return RedirectToAction("Index");
            return View(Log);
        }

        [Authorize, HttpPost]
        public ActionResult Update(DowntimeLog editedLog, IEnumerable<DowntimeAction> downtime_action)
        {
            try
            {
                DowntimeLog ExistedLog = DB.DowntimeLogs.Where(l => l.company_id == editedLog.company_id && 
                    string.Compare(l.downtime_log_id, editedLog.downtime_log_id, true) == 0).FirstOrDefault();
                if (ExistedLog != null)
                {
                    bool IsDirty = false;
                    if (string.Compare(ExistedLog.downtime_id, editedLog.downtime_id, true) != 0)
                    {
                        IsDirty = true;
                        ExistedLog.downtime_id = editedLog.downtime_id;
                    }

                    if (IsDirty)
                    {
                        ExistedLog.revised_date = DateTime.Now;
                        ExistedLog.revised_by = User.Identity.Name;
                        DB.Entry(ExistedLog).State = System.Data.Entity.EntityState.Modified;
                    }

                    if (downtime_action != null && downtime_action.Count() > 0)
                    {
                        HashSet<string> PostedAction = new HashSet<string>(downtime_action.Select(d => d.action_id));

                        IEnumerable<DowntimeAction> ExistedActions = DB.DowntimeActions.Where(a =>
                            a.company_id == ExistedLog.company_id && 
                            string.Compare(a.downtime_log_id, ExistedLog.downtime_log_id, true) == 0);
                        HashSet<string> CurrentAction = new HashSet<string>(ExistedActions.Select(a => a.action_id));

                        IEnumerable<DowntimeAction> DeletedAction = ExistedActions.Where(a => !PostedAction.Contains(a.action_id));
                        if (DeletedAction != null && DeletedAction.Count() > 0)
                        {
                            IsDirty = true;
                            foreach (DowntimeAction deleted in DeletedAction)
                                DB.Entry(deleted).State = System.Data.Entity.EntityState.Deleted;
                        }

                        IEnumerable<DowntimeAction> NewActions = downtime_action.Where(a => !CurrentAction.Contains(a.action_id));
                        if (NewActions != null && NewActions.Count() > 0)
                        {
                            IsDirty = true;
                            foreach (DowntimeAction newAction in NewActions)
                            {
                                newAction.company_id = ExistedLog.company_id;
                                newAction.downtime_log_id = ExistedLog.downtime_log_id;
                                newAction.created_date = DateTime.Now;
                                newAction.created_by = User.Identity.Name;

                                DB.DowntimeActions.Add(newAction);
                            }
                        }

                        var ModifiedActions = from eAction in ExistedActions
                                              join pAction in downtime_action on eAction.action_id equals pAction.action_id
                                              select new
                                              {
                                                  eAction,
                                                  m_fixed = pAction.is_fixed_the_problem
                                              };

                        if (ModifiedActions != null && ModifiedActions.Count() > 0)
                        {
                            foreach (var mAction in ModifiedActions)
                            {
                                if (mAction.eAction.is_fixed_the_problem != mAction.m_fixed)
                                {
                                    IsDirty = true;
                                    mAction.eAction.is_fixed_the_problem = mAction.m_fixed;
                                    DB.Entry(mAction.eAction).State = System.Data.Entity.EntityState.Modified;
                                }
                            }
                        }

                        if (IsDirty)
                            DB.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                DB.DetachAll();
                ModelState.AddModelError("ex", ex.Message);
            }
            return View(editedLog);
        }

        [Authorize, HttpPost]
        public ActionResult GetDowntimeAction(int company_id, string downtime_log_id)
        {
            var Actions = from dAction in DB.DowntimeActions
                          join oAction in DB.OperativeActions on new { c1 = dAction.company_id, c2 = dAction.action_id } equals new { c1 = oAction.company_id, c2 = oAction.action_id }
                          where dAction.company_id == company_id && string.Compare(dAction.downtime_log_id, downtime_log_id, true) == 0
                          select new
                          {
                              company_id = dAction.company_id,
                              downtime_log_id = dAction.downtime_log_id,
                              action_id = oAction.action_id,
                              action_name = oAction.action_name,
                              is_fixed = dAction.is_fixed_the_problem ? 1 : 0
                          };
            return Json(Actions.OrderBy(a => a.action_name));
        }

        [Authorize, HttpPost]
        public ActionResult Delete(int companyID, string downtimeLogID)
        {
            try
            {
                bool IsDirty = false;
                DowntimeLog Log = DB.DowntimeLogs.Where(l => l.company_id == companyID && 
                    string.Compare(l.downtime_log_id, downtimeLogID, true) == 0).FirstOrDefault();

                if (Log != null)
                {
                    IsDirty = true;
                    DB.Entry(Log).State = System.Data.Entity.EntityState.Deleted;
                }

                IEnumerable<DowntimeAction> Actions = DB.DowntimeActions.Where(a => a.company_id == companyID && 
                    string.Compare(a.downtime_log_id, downtimeLogID, true) == 0);
                if (Actions != null && Actions.Count() > 0)
                {
                    IsDirty = true;
                    foreach (DowntimeAction dAction in Actions)
                        DB.Entry(dAction).State = System.Data.Entity.EntityState.Deleted;
                }

                if (IsDirty)
                    DB.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json("success");
        }
    }
}