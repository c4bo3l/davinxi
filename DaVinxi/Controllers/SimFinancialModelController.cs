﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace DaVinxi.Controllers
{
    public class SimFinancialModelController : Controller
    {
        // GET: SimFinancialModel
        public ActionResult Index()
        {
            return View();
        }

        [WordDocument(DefaultFilename = "SimFinancialModel")]
        public ActionResult ExportToWord(string table_str) {
            ViewBag.HtmlTable = Encoding.ASCII.GetString(Convert.FromBase64String(table_str));
            return View();
        }

        [PDFDocument(SavedFilename = "SimFinancialModel")]
        public ActionResult ExportToPDF(string table_str) {
            ViewBag.HtmlTable = Encoding.ASCII.GetString(Convert.FromBase64String(table_str));
            return View();
        }
    }
}