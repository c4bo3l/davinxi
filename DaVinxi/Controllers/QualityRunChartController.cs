﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class QualityRunChartController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        [Authorize]
        // GET: QualityRunChart
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View();
        }

        [AllowCrossSiteJson]
        public ActionResult GetQualityData(int company_id, string[] machine_id, string sku_id, 
            string[] batch_id, string[] property_id)
        {
            try
            {
                var QualityData = from qData in DB.ProductQualities
                                  join product in DB.Products on
                                    new { c1 = qData.company_id, c2 = qData.product_id } equals
                                    new { c1 = product.company_id, c2 = product.product_id }
                                  join batch in DB.Batches on
                                    new { c1 = product.company_id, c2 = product.batch_id } equals
                                    new { c1 = batch.company_id, c2 = batch.batch_id }
                                  join machine in DB.Machines on
                                    new { c1 = batch.company_id, c2 = batch.machine_id } equals
                                    new { c1 = machine.company_id, c2 = machine.machine_id }
                                  join sku in DB.SKU on
                                    new { c1 = batch.company_id, c2 = batch.sku_id } equals
                                    new { c1 = sku.company_id, c2 = sku.sku_id }
                                  join propSetting in DB.SKUPropertySettings on
                                    new { c1 = sku.company_id, c2 = sku.sku_id, c3 = qData.quality_property_id } equals
                                    new { c1 = propSetting.company_id, c2 = propSetting.sku_id, c3 = propSetting.property_id }
                                  join qProperty in DB.QualityProperties on
                                    new { c1 = qData.company_id, c2 = qData.quality_property_id } equals
                                    new { c1 = qProperty.company_id, c2 = qProperty.property_id }
                                  join mUnit in DB.EngineeringUnits on
                                    new { c1 = qProperty.company_id, c2 = qProperty.unit_id } equals
                                    new { c1 = mUnit.company_id, c2 = mUnit.unit_id }
                                  where string.Compare(sku.sku_id, sku_id, true) == 0 &&
                                    property_id.Contains(qData.quality_property_id)
                                  select new
                                  {
                                      company_id = qData.company_id,
                                      batch_id = batch.batch_id,
                                      machine_id = machine.machine_id,
                                      machine_name = machine.machine_name,
                                      start_date = product.start_date,
                                      batch_start = batch.start_date,
                                      batch_end = batch.finish_date,
                                      sku_id = sku.sku_id,
                                      sku_name = sku.sku_name,
                                      property_id = qProperty.property_id,
                                      property_name = qProperty.property_name,
                                      target = propSetting.target,
                                      sigma = propSetting.sigma,
                                      ucl = propSetting.ucl,
                                      lcl = propSetting.lcl,
                                      url = propSetting.url,
                                      lrl = propSetting.lrl,
                                      actual_value = qData.quality_value,
                                      unit_name = mUnit.unit_name
                                  };

                if (company_id > 0)
                    QualityData = QualityData.Where(q => q.company_id == company_id);

                if (batch_id != null && batch_id.Length > 0)
                    QualityData = QualityData.Where(q => batch_id.Contains(q.batch_id));

                if (machine_id != null && machine_id.Length > 0)
                    QualityData = QualityData.Where(q => machine_id.Contains(q.machine_id));

                QualityData = QualityData.OrderBy(q => q.property_id).ThenBy
                    (q => q.start_date);

                var Result = QualityData.ToList().Select(q => new
                {
                    company_id = q.company_id,
                    batch_id = q.batch_id,
                    batch_start = q.batch_start.ToString("MMM dd yyyy HH:mm"),
                    batch_end = q.batch_end.ToString("MMM dd yyyy HH:mm"),
                    machine_id = q.machine_id,
                    machine_name = q.machine_name,
                    start_date = q.start_date,
                    totalMs = q.start_date.Subtract(Constant.Epoch).TotalMilliseconds,
                    sku_id = q.sku_id,
                    sku_name = q.sku_name,
                    property_id = q.property_id,
                    property_name = q.property_name,
                    target = q.target,
                    sigma = q.sigma,
                    ucl = q.ucl,
                    lcl = q.lcl,
                    url = q.url,
                    lrl = q.lrl,
                    actual_value = q.actual_value,
                    unit_name = q.unit_name
                });
                return new JsonResult()
                {
                    Data = Result,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}