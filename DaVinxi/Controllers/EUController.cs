﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class EUController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /EU/

        [Authorize]
        public ActionResult Index()
        {
            List<MasterEngineeringUnits> EUs = Helper.GetEngineeringUnits(
                Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();

            foreach (MasterEngineeringUnits eu in EUs)
            {
                if (!RelatedCompanies.ContainsKey(eu.company_id))
                {
                    String CompanyName = "All Companies";

                    if (eu.company_id > 0)
                    {
                        MasterCompanies Company = DB.Companies.Find(eu.company_id);
                        if (Company != null)
                            CompanyName = Company.company_name;
                    }

                    RelatedCompanies.Add(eu.company_id, CompanyName);
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;

            return View(EUs);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(
                Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(EngineeringUnitRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.EngineeringUnits.Where(eu => 
                    eu.company_id == register.company_id && 
                    string.Compare(eu.unit_id, register.unit_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Enginerring unit already existed");
                else
                {
                    MasterEngineeringUnits NewEU = new MasterEngineeringUnits();
                    NewEU.company_id = register.company_id;
                    NewEU.unit_id = register.unit_id;
                    NewEU.unit_name = register.unit_name;
                    NewEU.description = register.description;
                    NewEU.revised_by = User.Identity.Name;

                    DB.EngineeringUnits.Add(NewEU);
                    DB.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot register new engineering units");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string unitID)
        {
            IEnumerable<MasterEngineeringUnits> EUs = DB.EngineeringUnits.Where(eu =>
                    eu.company_id == companyID &&
                    string.Compare(eu.unit_id, unitID, true) == 0);

            if (EUs == null || EUs.Count() <= 0)
                return HttpNotFound();

            EngineeringUnitRegisterModel EditEU = new EngineeringUnitRegisterModel();
            EditEU.company_id = EUs.ElementAt(0).company_id;
            EditEU.unit_id = EUs.ElementAt(0).unit_id;
            EditEU.unit_name = EUs.ElementAt(0).unit_name;
            EditEU.description = EUs.ElementAt(0).description;

            return View(EditEU);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(EngineeringUnitRegisterModel editedEU)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<MasterEngineeringUnits> EUs = DB.EngineeringUnits.Where(eu =>
                    eu.company_id == editedEU.company_id &&
                    string.Compare(eu.unit_id, editedEU.unit_id, true) == 0);

                if (EUs == null || EUs.Count() <= 0)
                    return HttpNotFound();

                bool IsDirty = false;

                if (string.Compare(editedEU.unit_name, EUs.ElementAt(0).unit_name) != 0)
                {
                    EUs.ElementAt(0).unit_name = editedEU.unit_name;
                    IsDirty = true;
                }

                if (string.Compare(editedEU.description, EUs.ElementAt(0).description) != 0)
                {
                    EUs.ElementAt(0).description = editedEU.description;
                    IsDirty = true;
                }

                if (IsDirty)
                {
                    DB.Entry(EUs.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;
                    DB.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Cannot update engineering unit");
            return View(editedEU);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string unitID, int companyID)
        {
            IEnumerable<MasterEngineeringUnits> EUs = DB.EngineeringUnits.Where(eu =>
                        eu.company_id == companyID &&
                        string.Compare(eu.unit_id, unitID, true) == 0);

            if (EUs != null && EUs.Count() > 0)
            {
                DB.Entry(EUs.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                DB.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}
