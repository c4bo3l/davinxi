﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class DowntimeTreeController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        public string DowntimeRootParentID = "0x0000";
        // GET: Downtime
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            if (CompanyID > 0 && DB.Downtimes.Where(d => d.company_id == CompanyID &&
                string.Compare(d.downtime_id, "root-node", true) == 0).Count() <= 0)
            {
                MasterDowntime DefaultDowntime = new MasterDowntime();
                DefaultDowntime.company_id = CompanyID;
                DefaultDowntime.downtime_id = "root-node";
                DefaultDowntime.downtime_name = "Uncategorized";
                DefaultDowntime.parent_downtime_id = DowntimeRootParentID;
                DefaultDowntime.revised_by = User.Identity.Name;

                DB.Downtimes.Add(DefaultDowntime);
                DB.SaveChanges();
            }

            ViewBag.Companies = Helper.GetCompanies(CompanyID);

            return View();
        }

        #region GetDowntimeInfo
        [Authorize,HttpPost]
        public ActionResult GetDowntimeInfoJSON(int companyID, string downtimeID)
        {
            try
            {
                return Json(DB.Downtimes.Where(d => d.company_id == companyID && 
                    string.Compare(d.downtime_id, downtimeID, true) == 0).FirstOrDefault());
            }
            catch(Exception)
            { return null; }
        }
        #endregion

        #region Get Downtime()
        [Authorize, HttpPost]
        public ActionResult GetDowntimeJSON(int companyID, string parentID = "0x0000")
        {
            try
            {
                var Downtime = from downtime in DB.Downtimes
                               where downtime.company_id == companyID && string.Compare(downtime.parent_downtime_id, parentID, true) == 0
                               join down2 in DB.Downtimes on
                               new { c1 = downtime.company_id, c2 = downtime.downtime_id } equals
                               new { c1 = down2.company_id, c2 = down2.parent_downtime_id }
                               into result
                               from rs in result.DefaultIfEmpty()
                               select new
                               {
                                   downtimeID = downtime.downtime_id,
                                   downtime,
                                   count = rs == null ? 0 : result.Count()
                               };
                return Json(Downtime.GroupBy(d => d.downtimeID).Select(d => d.FirstOrDefault()).OrderBy(d => d.downtime.downtime_name));
            }
            catch (Exception)
            { return null; }
        }
        #endregion

        #region Get Leaves Downtime
        [Authorize, HttpPost]
        public ActionResult GetLeavesDowntimeJSON(int companyID)
        {
            try
            {
                var Downtime = from downtime in DB.Downtimes
                               where downtime.company_id == companyID 
                               join down2 in DB.Downtimes on
                               new { c1 = downtime.company_id, c2 = downtime.downtime_id } equals
                               new { c1 = down2.company_id, c2 = down2.parent_downtime_id }
                               into result
                               from rs in result.DefaultIfEmpty()
                               select new
                               {
                                   downtimeID = downtime.downtime_id,
                                   downtime,
                                   count = rs == null ? 0 : result.Count()
                               };
                Downtime = Downtime.Where(d => d.count == 0);
                return Json(Downtime.GroupBy(d => d.downtimeID).Select(d => d.FirstOrDefault()).OrderBy(d => d.downtime.downtime_name));
            }
            catch (Exception)
            { return null; }
        }
        #endregion

        [Authorize]
        public ActionResult Register(int companyID, string parentID)
        {
            MasterDowntime NewDowntime = new MasterDowntime();
            NewDowntime.company_id = companyID;
            NewDowntime.revised_by = User.Identity.Name;
            NewDowntime.parent_downtime_id = parentID;
            return View(NewDowntime);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(MasterDowntime newDowntime)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (DB.Downtimes.Where(d => d.company_id == newDowntime.company_id &&
                        string.Compare(d.downtime_id, newDowntime.downtime_id, true) == 0).Count() > 0)
                        ModelState.AddModelError("", "Downtime already exist");
                    else
                    {
                        DB.Downtimes.Add(newDowntime);
                        DB.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ex", ex.Message);
            }
            return View(newDowntime);
        }

        [Authorize]
        public ActionResult Update(int companyID, string downtimeID)
        {
            MasterDowntime Downtime = DB.Downtimes.Where(d => d.company_id == companyID && 
                string.Compare(d.downtime_id, downtimeID, true) == 0).FirstOrDefault();
            if (Downtime == null)
                return RedirectToAction("Index");
            ViewBag.ParentDowntime = DB.Downtimes.Where(d => d.company_id == companyID &&
                string.Compare(d.downtime_id, downtimeID, true) != 0).OrderBy(d => d.downtime_name).ToList();
            return View(Downtime);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(MasterDowntime editedDowntime)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    MasterDowntime ExistDowntime = DB.Downtimes.Where(d => d.company_id == editedDowntime.company_id && 
                        string.Compare(d.downtime_id, editedDowntime.downtime_id, true) == 0).FirstOrDefault();
                    if (ExistDowntime != null)
                    {
                        bool IsDirty = false;
                        if (string.Compare(ExistDowntime.downtime_name, editedDowntime.downtime_name) != 0)
                        {
                            IsDirty = true;
                            ExistDowntime.downtime_name = editedDowntime.downtime_name;
                        }

                        if (string.Compare(ExistDowntime.description, editedDowntime.description) != 0)
                        {
                            IsDirty = true;
                            ExistDowntime.description = editedDowntime.description;
                        }

                        if (string.Compare(ExistDowntime.parent_downtime_id, editedDowntime.parent_downtime_id) != 0)
                        {
                            IsDirty = true;
                            ExistDowntime.parent_downtime_id = editedDowntime.parent_downtime_id;
                        }

                        if (IsDirty)
                        {
                            ExistDowntime.revised_date = DateTime.Now;
                            ExistDowntime.revised_by = User.Identity.Name;

                            DB.Entry(ExistDowntime).State = System.Data.Entity.EntityState.Modified;
                            DB.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ex", ex.Message);
            }
            return View(editedDowntime);
        }

        [Authorize]
        public ActionResult Delete(int companyID, string downtimeID)
        {
            MasterDowntime DeletedDowntime = DB.Downtimes.Where(d => d.company_id == companyID && 
                string.Compare(d.downtime_id, downtimeID, true) == 0).FirstOrDefault();
            MasterDowntime DefaultDowntime = DB.Downtimes.Where(d => d.company_id == companyID &&
                string.Compare(d.downtime_id, "root-node", true) == 0).FirstOrDefault();

            if (DeletedDowntime != null && DefaultDowntime != null)
            {
                IEnumerable<MasterDowntime> Child = DB.Downtimes.Where(d => d.company_id == companyID && 
                    string.Compare(d.parent_downtime_id, downtimeID, true) == 0);
                if (Child != null && Child.Count() > 0)
                {
                    foreach (MasterDowntime d in Child)
                    {
                        d.parent_downtime_id = DefaultDowntime.downtime_id;
                        d.revised_date = DateTime.Now;
                        d.revised_by = User.Identity.Name;
                        DB.Entry(d).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                DB.Entry(DeletedDowntime).State = System.Data.Entity.EntityState.Deleted;
                DB.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}