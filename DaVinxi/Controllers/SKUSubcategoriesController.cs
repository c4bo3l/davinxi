﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class SKUSubcategoriesController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /SKUSubcategories/
        [Authorize]
        public ActionResult Index()
        {
            List<SKUSubcategories> SubCategories = Helper.GetSKUSubCategories(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, string> RelatedCategories = new Dictionary<string, string>();
            Dictionary<string, List<QualityProperties>> RelatedProperties = new Dictionary<string, List<QualityProperties>>();

            if (SubCategories.Count > 0)
            {
                foreach (SKUSubcategories skuSubCat in SubCategories)
                {
                    if (!RelatedCompanies.ContainsKey(skuSubCat.company_id))
                    {
                        string CompanyName = "All Companies";
                        if (skuSubCat.company_id > 0)
                        {
                            MasterCompanies Company = DB.Companies.Find(skuSubCat.company_id);
                            if (Company != null)
                                CompanyName = Company.company_name;
                        }

                        RelatedCompanies.Add(skuSubCat.company_id, CompanyName);
                    }

                    if (!RelatedProperties.ContainsKey(skuSubCat.subcategory_id))
                    {
                        List<QualityProperties> QProp =
                            Helper.GetQualityPropertiesBySKUSubCategory(skuSubCat.company_id, skuSubCat.subcategory_id);
                        if (QProp.Count > 0)
                            RelatedProperties.Add(skuSubCat.subcategory_id, QProp);
                    }

                    if(!RelatedCategories.ContainsKey(skuSubCat.category_id))
                    {
                        IEnumerable<SKUCategories> Categories = 
                            DB.SKUCategories.Where(c => c.company_id == skuSubCat.company_id && 
                                string.Compare(c.category_id, skuSubCat.category_id, true) == 0);
                        string CategoryName = "";

                        if (Categories != null && Categories.Count() > 0)
                            CategoryName = Categories.ElementAt(0).category_name;
                        RelatedCategories.Add(skuSubCat.category_id, CategoryName);
                    }
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedProperties = RelatedProperties;
            ViewBag.RelatedCategories = RelatedCategories;

            return View(SubCategories);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(SKUSubcategoryRegisterModel registered)
        {
            if(ModelState.IsValid)
            {
                if (DB.SKUSubcategories.Where(s => 
                    s.company_id == registered.company_id && 
                    string.Compare(s.subcategory_id, registered.subcategory_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Subcategory already exist");
                else
                {
                    SKUSubcategories NewSubCategory = new SKUSubcategories();
                    NewSubCategory.company_id = registered.company_id;
                    NewSubCategory.subcategory_id = registered.subcategory_id;
                    NewSubCategory.subcategory_name = registered.subcategory_name;
                    NewSubCategory.category_id = registered.category_id;
                    NewSubCategory.revised_by = User.Identity.Name;

                    DB.SKUSubcategories.Add(NewSubCategory);

                    if(Request["properties"]!=null)
                    {
                        foreach(string prop in Request["properties"].Split(','))
                        {
                            SKUSubcategoryProperties Properties = new SKUSubcategoryProperties();
                            Properties.company_id = registered.company_id;
                            Properties.subcategory_id = registered.subcategory_id;
                            Properties.property_id = prop;
                            Properties.revised_by = User.Identity.Name;

                            DB.SKUSubcategoryProperties.Add(Properties);
                        }
                    }

                    DB.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot register new SKU Subcategory");
            return View(registered);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string subcategoryID)
        {
            IEnumerable<SKUSubcategories> SubCategory = DB.SKUSubcategories.Where(s => s.company_id == companyID && 
                string.Compare(s.subcategory_id, subcategoryID, true) == 0);
            if (SubCategory.Count() <= 0)
                return RedirectToAction("Index");

            SKUSubcategoryRegisterModel EditSubCategory = new SKUSubcategoryRegisterModel();
            EditSubCategory.company_id = SubCategory.ElementAt(0).company_id;
            EditSubCategory.subcategory_id = SubCategory.ElementAt(0).subcategory_id;
            EditSubCategory.subcategory_name = SubCategory.ElementAt(0).subcategory_name;
            EditSubCategory.category_id = SubCategory.ElementAt(0).category_id;

            ViewBag.Categories = Helper.GetSKUCategories(EditSubCategory.company_id);
            ViewBag.Properties = Helper.GetQualityProperties(EditSubCategory.company_id);

            return View(EditSubCategory);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(SKUSubcategoryRegisterModel edited)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<SKUSubcategories> SubCategory = DB.SKUSubcategories.Where(s => 
                    s.company_id == edited.company_id &&
                    string.Compare(s.subcategory_id, edited.subcategory_id, true) == 0);
                if (SubCategory.Count() > 0)
                {
                    bool IsDirty = false;

                    if (string.Compare(SubCategory.ElementAt(0).subcategory_name, edited.subcategory_name) != 0)
                    {
                        SubCategory.ElementAt(0).subcategory_name = edited.subcategory_name;
                        IsDirty = true;
                    }

                    if (string.Compare(SubCategory.ElementAt(0).category_id, edited.category_id, true) != 0)
                    {
                        SubCategory.ElementAt(0).category_id = edited.category_id;
                        IsDirty = true;
                    }

                    if (IsDirty)
                    {
                        SubCategory.ElementAt(0).revised_date = DateTime.Now;
                        SubCategory.ElementAt(0).revised_by = User.Identity.Name;
                        DB.Entry(SubCategory.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;
                    }

                    var SelectedProp = new HashSet<string>();
                    if (Request["properties"] != null)
                        SelectedProp = new HashSet<string>(Request["properties"].Split(','));

                    var Properties = DB.SKUSubcategoryProperties.Where(p =>
                            p.company_id == edited.company_id &&
                            string.Compare(p.subcategory_id, edited.subcategory_id, true) == 0);
                    var CurrentProp = new HashSet<string>(Properties.Select(p => p.property_id));

                    var DeletedProp = Properties.Where(p => !SelectedProp.Contains(p.property_id));
                    if (DeletedProp != null && DeletedProp.Count() > 0)
                    {
                        foreach (var deleted in DeletedProp)
                            DB.Entry(deleted).State = System.Data.Entity.EntityState.Deleted;
                        IsDirty = true;
                    }

                    var NewProp = SelectedProp.Where(p => !CurrentProp.Contains(p));
                    if (NewProp != null && NewProp.Count() > 0)
                    {
                        foreach (var added in NewProp)
                        {
                            SKUSubcategoryProperties AddedProp = new SKUSubcategoryProperties();
                            AddedProp.company_id = edited.company_id;
                            AddedProp.subcategory_id = edited.subcategory_id;
                            AddedProp.property_id = added;
                            AddedProp.revised_by = User.Identity.Name;

                            DB.SKUSubcategoryProperties.Add(AddedProp);
                        }

                        IsDirty = true;
                    }

                    if (IsDirty)
                        DB.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Cannot edit subcategory");
            return View(edited);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string subcategoryID,int companyID)
        {
            bool IsDirty = false;

            IEnumerable<SKUSubcategories> Subcategories = DB.SKUSubcategories.Where(s => 
                s.company_id == companyID && string.Compare(s.subcategory_id, subcategoryID, true) == 0);
            if (Subcategories != null && Subcategories.Count() > 0)
            {
                DB.Entry(Subcategories.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            IEnumerable<SKUSubcategoryProperties> Properties = DB.SKUSubcategoryProperties.Where(p => 
                p.company_id == companyID && string.Compare(p.subcategory_id, subcategoryID, true) == 0);
            if(Properties!=null && Properties.Count()>0)
            {
                foreach (SKUSubcategoryProperties prop in Properties)
                    DB.Entry(prop).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            if (IsDirty)
                DB.SaveChanges();

            return RedirectToAction("Index");
        }
	}
}