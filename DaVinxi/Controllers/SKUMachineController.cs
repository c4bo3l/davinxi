﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class SKUMachineController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: SKUMachine
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);

            return View();
        }

        [Authorize]
        public ActionResult GetProcessSetting(int company_id, string machine_id, string sku_id)
        {
            var Result = from machine in DB.Machines
                         join type in DB.MachineTypes on new { c1 = machine.company_id, c2 = machine.machine_type_id }
                            equals new { c1 = type.company_id, c2 = type.machine_type_id }
                         join machComponents in DB.MachineTypeComponents on
                            new { c1 = type.company_id, c2 = type.machine_type_id }
                            equals new { c1 = machComponents.company_id, c2 = machComponents.machine_type_id }
                         join components in DB.Components on
                            new { c1 = machComponents.company_id, c2 = machComponents.component_id }
                            equals new { c1 = components.company_id, c2 = components.component_id }
                         join setting in DB.SKUMachineSettings on
                            new
                            {
                                c1 = machine.company_id,
                                c2 = machine.machine_id,
                                c3 = components.component_id,
                                c4 = sku_id
                            } equals
                            new
                            {
                                c1 = setting.company_id,
                                c2 = setting.machine_id,
                                c3 = setting.component_id,
                                c4 = setting.sku_id
                            }
                         into rSetting
                         from r in rSetting.DefaultIfEmpty()
                         where machine.company_id == company_id && string.Compare(machine.machine_id, machine_id, true) == 0
                         orderby components.component_name
                         select new
                         {
                             machine_id = machine.machine_id,
                             component_id = components.component_id,
                             component_name = components.component_name,
                             sku_id = sku_id,
                             min = (r == null ? 0 : r.min),
                             max = (r == null ? 0 : r.max),
                             target = (r == null ? 0 : r.target),
                             lower_alarm_limit = (r == null ? 0 : r.lower_alarm_limit),
                             upper_alarm_limit = (r == null ? 0 : r.upper_alarm_limit),
                             rl = (r == null ? 1 : r.rl),
                             rr = (r == null ? 1 : r.rr)
                         };
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = Result
            };
        }

        struct Message {
            public int code;
            public string message;
        }

        [Authorize]
        public ActionResult Save(int company_id, string machine_id, string sku_id, IEnumerable<SKUMachine> setting)
        {
            Message ResultMessage = new Message();
            ResultMessage.code = 1;
            ResultMessage.message = "";
            try
            {
                if (company_id > 0 && machine_id != null && sku_id != null && setting != null)
                {
                    foreach (SKUMachine s in setting) 
                    {
                        bool IsNew = false, IsDirty = true;
                        SKUMachine CurrentSetting = DB.SKUMachineSettings.Where(m => m.company_id == company_id && 
                            string.Compare(m.machine_id, machine_id, true) == 0 && 
                            string.Compare(m.sku_id, sku_id, true) == 0 && 
                            string.Compare(m.component_id, s.component_id, true) == 0).FirstOrDefault();
                        if (CurrentSetting == null)
                        {
                            IsDirty = IsNew = true;

                            s.company_id = company_id;
                            s.machine_id = machine_id;
                            s.sku_id = sku_id;
                            s.created_date = s.revised_date = DateTime.Now;
                            s.revised_by = User.Identity.Name;

                            DB.SKUMachineSettings.Add(s);
                        }
                        else
                        {
                            if (CurrentSetting.min != s.min)
                            {
                                CurrentSetting.min = s.min;
                                IsDirty = true;
                            }
                            
                            if (CurrentSetting.max != s.max)
                            {
                                CurrentSetting.max = s.max;
                                IsDirty = true;
                            }
                            
                            if (CurrentSetting.target != s.target)
                            {
                                CurrentSetting.target = s.target;
                                IsDirty = true;
                            }

                            if (CurrentSetting.lower_alarm_limit != s.lower_alarm_limit)
                            {
                                CurrentSetting.lower_alarm_limit = s.lower_alarm_limit;
                                IsDirty = true;
                            }

                            if (CurrentSetting.upper_alarm_limit != s.upper_alarm_limit)
                            {
                                CurrentSetting.upper_alarm_limit = s.upper_alarm_limit;
                                IsDirty = true;
                            }

                            if (CurrentSetting.rl != s.rl)
                            {
                                CurrentSetting.rl = s.rl;
                                IsDirty = true;
                            }

                            if (CurrentSetting.rr != s.rr)
                            {
                                CurrentSetting.rr = s.rr;
                                IsDirty = true;
                            }
                        }

                        if (IsDirty)
                        {
                            if(!IsNew)
                                DB.Entry(CurrentSetting).State = System.Data.Entity.EntityState.Modified;
                            DB.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex) {
                ResultMessage.code = -1;
                ResultMessage.message = ex.Message;
            }

            return Json(ResultMessage);
        }
    }
}