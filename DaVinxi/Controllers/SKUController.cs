﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class SKUController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /SKU/
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0;

            List<MasterSKU> SKUs = Helper.GetSKU(CompanyID);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, List<QualityProperties>> RelatedProperties = new Dictionary<string, List<QualityProperties>>();
            Dictionary<string, string> RelatedSubCategories = new Dictionary<string, string>();
            Dictionary<string, string> RelatedCategories = new Dictionary<string, string>();
            Dictionary<string, string> RelatedSegments = new Dictionary<string, string>();
            Dictionary<string, string> RelatedBusiness = new Dictionary<string, string>();

            if(SKUs!=null && SKUs.Count>0)
            {
                foreach(MasterSKU sku in SKUs)
                {
                    if (!RelatedCompanies.ContainsKey(sku.company_id))
                    {
                        string CompanyName = "All Companies";
                        if (sku.company_id > 0)
                        {
                            MasterCompanies Company = DB.Companies.Find(sku.company_id);
                            if (Company != null)
                                CompanyName = Company.company_name;
                        }

                        RelatedCompanies.Add(sku.company_id, CompanyName);
                    }

                    if (!RelatedProperties.ContainsKey(sku.sku_id))
                    {
                        List<QualityProperties> QProp =
                            Helper.GetQualityPropertiesBySKU(sku.company_id, sku.sku_id);
                        if (QProp.Count > 0)
                            RelatedProperties.Add(sku.sku_id, QProp);
                    }

                    if(!RelatedSubCategories.ContainsKey(sku.subcategory_id))
                    {
                        string SubcategoryName = "";

                        IEnumerable<SKUSubcategories> Subcategories = 
                            DB.SKUSubcategories.Where(s => s.company_id == sku.company_id && 
                                string.Compare(s.subcategory_id, sku.subcategory_id, true) == 0);
                        if(Subcategories!=null && Subcategories.Count()>0)
                        {
                            SubcategoryName = Subcategories.ElementAt(0).subcategory_name;
                            string CategoryID = Subcategories.ElementAt(0).category_id;

                            if(!RelatedCategories.ContainsKey(Subcategories.ElementAt(0).subcategory_id))
                            {
                                IEnumerable<SKUCategories> Categories = DB.SKUCategories.Where(c => 
                                    c.company_id == sku.company_id && 
                                    string.Compare(c.category_id, CategoryID, true) == 0);
                                string CategoryName = "";

                                if (Categories != null && Categories.Count() > 0)
                                    CategoryName = Categories.ElementAt(0).category_name;

                                RelatedCategories.Add(Subcategories.ElementAt(0).subcategory_id, CategoryName);
                            }
                        }

                        RelatedSubCategories.Add(Subcategories.ElementAt(0).subcategory_id, SubcategoryName);
                    }

                    if(!RelatedSegments.ContainsKey(sku.segment_id))
                    {
                        string SegmentName = "";

                        IEnumerable<SKUSegments> Segments = DB.SKUSegments.Where(s => 
                            s.company_id == sku.company_id && string.Compare(s.segment_id, sku.segment_id, true) == 0);
                        if(Segments!=null && Segments.Count()>0)
                            SegmentName = Segments.ElementAt(0).segment_name;
                        RelatedSegments.Add(sku.segment_id, SegmentName);
                    }

                    if (!RelatedBusiness.ContainsKey(sku.business_id))
                    {
                        string BusinessName = "";

                        IEnumerable<SKUBusinesses> Businesses = DB.SKUBusiness.Where(s =>
                            s.company_id == sku.company_id && string.Compare(s.business_id, sku.business_id, true) == 0);
                        if (Businesses != null && Businesses.Count() > 0)
                            BusinessName = Businesses.ElementAt(0).business_name;
                        RelatedBusiness.Add(sku.business_id, BusinessName);
                    }
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedCategories = RelatedCategories;
            ViewBag.RelatedSubcategories = RelatedSubCategories;
            ViewBag.RelatedSegments = RelatedSegments;
            ViewBag.RelatedBusinesses = RelatedBusiness;
            ViewBag.RelatedProperties = RelatedProperties;

            return View(SKUs);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(SKURegisterModel newSKU)
        {
            if(ModelState.IsValid)
            {
                if (DB.SKU.Where(s => s.company_id == newSKU.company_id && string.Compare(s.sku_id, newSKU.sku_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "SKU already exist");
                else
                {
                    MasterSKU SKU = new MasterSKU();
                    SKU.company_id = newSKU.company_id;
                    SKU.subcategory_id = newSKU.subcategory_id;
                    SKU.sku_id = newSKU.sku_id;
                    SKU.additional_id = newSKU.sku_id;
                    SKU.sku_name = newSKU.sku_name;
                    SKU.segment_id = newSKU.segment_id;
                    SKU.business_id = newSKU.business_id;
                    SKU.basis_weight = newSKU.basis_weight;
                    SKU.critical_quality_property = newSKU.critical_quality_property;
                    SKU.work_instruction = newSKU.work_instruction;
                    SKU.revised_by = User.Identity.Name;

                    DB.SKU.Add(SKU);

                    if(Request["properties"]!=null)
                    {
                        string[] Properties = Request["properties"].Split(',');

                        foreach(string prop in Properties)
                        {
                            SKUProperties SKUProp = new SKUProperties();
                            SKUProp.company_id = SKU.company_id;
                            SKUProp.sku_id = SKU.sku_id;
                            SKUProp.property_id = prop;
                            SKUProp.created_by = User.Identity.Name;

                            DB.SKUProperties.Add(SKUProp);
                        }
                    }

                    DB.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);

            ModelState.AddModelError("", "Cannot register new SKU");

            return View(newSKU);
        }

        [Authorize]
        public ActionResult Edit(int companyID,string skuID)
        {
            IEnumerable<MasterSKU> SKUs = DB.SKU.Where(s => 
                s.company_id == companyID && string.Compare(s.sku_id, skuID, true) == 0);
            if (SKUs == null || SKUs.Count() <= 0)
                return RedirectToAction("Index");

            SKURegisterModel EditSKU = new SKURegisterModel();
            EditSKU.company_id = SKUs.ElementAt(0).company_id;
            EditSKU.subcategory_id = SKUs.ElementAt(0).subcategory_id;
            EditSKU.sku_id = SKUs.ElementAt(0).sku_id;
            EditSKU.additional_id = SKUs.ElementAt(0).additional_id;
            EditSKU.sku_id = SKUs.ElementAt(0).sku_id;
            EditSKU.sku_name = SKUs.ElementAt(0).sku_name;
            EditSKU.segment_id = SKUs.ElementAt(0).segment_id;
            EditSKU.business_id = SKUs.ElementAt(0).business_id;
            EditSKU.basis_weight = SKUs.ElementAt(0).basis_weight;
            EditSKU.critical_quality_property = SKUs.ElementAt(0).critical_quality_property;
            EditSKU.work_instruction = SKUs.ElementAt(0).work_instruction;

            ViewBag.Categories = DB.SKUCategories.Where(c => c.company_id == EditSKU.company_id);
            ViewBag.SelectedCategory = (from subCat in DB.SKUSubcategories
                                        where string.Compare(subCat.subcategory_id, EditSKU.subcategory_id, true) == 0
                                        select subCat.category_id).First();
            ViewBag.Segments = DB.SKUSegments.Where(s => s.company_id == EditSKU.company_id);
            ViewBag.Businesses = DB.SKUBusiness.Where(b => b.company_id == EditSKU.company_id);
            ViewBag.Properties = DB.QualityProperties.Where(q => q.company_id == EditSKU.company_id);

            return View(EditSKU);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(SKURegisterModel edited)
        {
            if(ModelState.IsValid)
            {
                IEnumerable<MasterSKU> SKUs = DB.SKU.Where(s => 
                    s.company_id == edited.company_id && string.Compare(s.sku_id, edited.sku_id, true) == 0);
                if(SKUs!=null && SKUs.Count()>0)
                {
                    bool IsDirty = false;
                    if(string.Compare(edited.subcategory_id,SKUs.ElementAt(0).subcategory_id,true)!=0)
                    {
                        SKUs.ElementAt(0).subcategory_id = edited.subcategory_id;
                        IsDirty = true;
                    }

                    if (string.Compare(edited.sku_name, SKUs.ElementAt(0).sku_name, true) != 0)
                    {
                        SKUs.ElementAt(0).sku_name = edited.sku_name;
                        IsDirty = true;
                    }

                    if (string.Compare(edited.additional_id, SKUs.ElementAt(0).additional_id) != 0)
                    {
                        SKUs.ElementAt(0).additional_id = edited.additional_id;
                        IsDirty = true;
                    }

                    if (string.Compare(edited.segment_id, SKUs.ElementAt(0).segment_id, true) != 0)
                    {
                        SKUs.ElementAt(0).segment_id = edited.segment_id;
                        IsDirty = true;
                    }

                    if (string.Compare(edited.business_id, SKUs.ElementAt(0).business_id, true) != 0)
                    {
                        SKUs.ElementAt(0).business_id = edited.business_id;
                        IsDirty = true;
                    }

                    if (edited.basis_weight != SKUs.ElementAt(0).basis_weight)
                    {
                        SKUs.ElementAt(0).basis_weight = edited.basis_weight;
                        IsDirty = true;
                    }

                    if (string.Compare(edited.critical_quality_property, SKUs.ElementAt(0).critical_quality_property) != 0)
                    {
                        SKUs.ElementAt(0).critical_quality_property = edited.critical_quality_property;
                        IsDirty = true;
                    }

                    if (string.Compare(edited.work_instruction, SKUs.ElementAt(0).work_instruction) != 0)
                    {
                        SKUs.ElementAt(0).work_instruction = edited.work_instruction;
                        IsDirty = true;
                    }

                    if (IsDirty)
                    {
                        SKUs.ElementAt(0).revised_by = User.Identity.Name;
                        SKUs.ElementAt(0).revised_date = DateTime.Now;
                        DB.Entry(SKUs.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;
                    }

                    var SelectedProp = new HashSet<string>();
                    if (Request["properties"] != null)
                        SelectedProp = new HashSet<string>(Request["properties"].Split(','));

                    var Properties = DB.SKUProperties.Where(p =>
                            p.company_id == edited.company_id &&
                            string.Compare(p.sku_id, edited.sku_id, true) == 0);
                    var CurrentProp = new HashSet<string>(Properties.Select(p => p.property_id));

                    var DeletedProp = Properties.Where(p => !SelectedProp.Contains(p.property_id));
                    if (DeletedProp != null && DeletedProp.Count() > 0)
                    {
                        foreach (var deleted in DeletedProp)
                            DB.Entry(deleted).State = System.Data.Entity.EntityState.Deleted;
                        IsDirty = true;
                    }

                    var NewProp = SelectedProp.Where(p => !CurrentProp.Contains(p));
                    if (NewProp != null && NewProp.Count() > 0)
                    {
                        foreach (var added in NewProp)
                        {
                            SKUProperties AddedProp = new SKUProperties();
                            AddedProp.company_id = edited.company_id;
                            AddedProp.sku_id = edited.sku_id;
                            AddedProp.property_id = added;
                            AddedProp.created_by = User.Identity.Name;

                            DB.SKUProperties.Add(AddedProp);
                        }

                        IsDirty = true;
                    }

                    if (IsDirty)
                        DB.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.Categories = DB.SKUCategories.Where(c => c.company_id == edited.company_id);
            ViewBag.SelectedCategory = (from subCat in DB.SKUSubcategories
                                        where string.Compare(subCat.subcategory_id, edited.subcategory_id, true) == 0
                                        select subCat.category_id).First();
            ViewBag.Segments = DB.SKUSegments.Where(s => s.company_id == edited.company_id);
            ViewBag.Businesses = DB.SKUBusiness.Where(b => b.company_id == edited.company_id);
            ViewBag.Properties = DB.QualityProperties.Where(q => q.company_id == edited.company_id);

            ModelState.AddModelError("", "Cannot update SKU information");

            return View(edited);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string skuID, int companyID)
        {
            bool IsDirty = false;

            IEnumerable<MasterSKU> SKUs = DB.SKU.Where(s =>
                s.company_id == companyID && string.Compare(s.sku_id, skuID, true) == 0);
            if (SKUs != null && SKUs.Count() > 0)
            {
                DB.Entry(SKUs.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            IEnumerable<SKUProperties> Properties = DB.SKUProperties.Where(p =>
                p.company_id == companyID && string.Compare(p.sku_id, skuID, true) == 0);
            if (Properties != null && Properties.Count() > 0)
            {
                foreach (SKUProperties prop in Properties)
                    DB.Entry(prop).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            if (IsDirty)
                DB.SaveChanges();

            return RedirectToAction("Index");
        }
	}
}