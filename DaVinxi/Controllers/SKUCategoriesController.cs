﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class SKUCategoriesController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /SKUCategories/
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0;

            List<SKUCategories> Categories = Helper.GetSKUCategories(CompanyID);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, List<QualityProperties>> RelatedProperties = new Dictionary<string, List<QualityProperties>>();

            if(Categories.Count>0)
            {
                foreach(SKUCategories skuCat in Categories)
                {
                    if(!RelatedCompanies.ContainsKey(skuCat.company_id))
                    {
                        string CompanyName = "All Companies";
                        if (skuCat.company_id > 0)
                        {
                            MasterCompanies Company = DB.Companies.Find(skuCat.company_id);
                            if (Company != null)
                                CompanyName = Company.company_name;
                        }

                        RelatedCompanies.Add(skuCat.company_id, CompanyName);
                    }

                    if (!RelatedProperties.ContainsKey(skuCat.category_id))
                    {
                        List<QualityProperties> QProp = 
                            Helper.GetQualityPropertiesBySKUCategory(skuCat.company_id, skuCat.category_id);
                        if (QProp.Count > 0)
                            RelatedProperties.Add(skuCat.category_id, QProp);
                    }
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedProperties = RelatedProperties;

            return View(Categories);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(SKUCategoryRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.SKUCategories.Where(q => q.company_id == register.company_id && 
                    string.Compare(q.category_id, register.category_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "SKU Category already exist");
                else
                {
                    SKUCategories NewCategory = new SKUCategories();
                    NewCategory.company_id = register.company_id;
                    NewCategory.category_id = register.category_id;
                    NewCategory.category_name = register.category_name;
                    NewCategory.revised_by = User.Identity.Name;
                    DB.SKUCategories.Add(NewCategory);

                    if(Request["properties"]!=null)
                    {
                        string[] Properties = Request["properties"].Split(',');
                        if(Properties.Length>0)
                        {
                            foreach(string prop in Properties)
                            {
                                SKUCategoryProperties CatProp = new SKUCategoryProperties();
                                CatProp.company_id = NewCategory.company_id;
                                CatProp.category_id = NewCategory.category_id;
                                CatProp.property_id = prop;
                                CatProp.created_by = User.Identity.Name;
                                DB.SKUCategoryProperties.Add(CatProp);
                            }
                        }
                    }

                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot register new SKU category");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string categoryID)
        {
            IEnumerable<SKUCategories> Category = DB.SKUCategories.Where(q => 
                q.company_id == companyID && string.Compare(q.category_id, categoryID, true) == 0);

            if (Category.Count() <= 0)
                return RedirectToAction("Index");

            SKUCategoryRegisterModel EditCategory = new SKUCategoryRegisterModel();
            EditCategory.company_id = Category.ElementAt(0).company_id;
            EditCategory.category_id = Category.ElementAt(0).category_id;
            EditCategory.category_name = Category.ElementAt(0).category_name;

            return View(EditCategory);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(SKUCategoryRegisterModel edited)
        {
            if(ModelState.IsValid)
            {
                IEnumerable<SKUCategories> Categories = DB.SKUCategories.Where(q => 
                    q.company_id == edited.company_id && string.Compare(q.category_id, edited.category_id, true) == 0);
                if (Categories.Count() > 0)
                {
                    bool IsDirty = false;

                    if (string.Compare(Categories.ElementAt(0).category_name, edited.category_name) != 0)
                    {
                        Categories.ElementAt(0).category_name = edited.category_name;
                        IsDirty = true;
                    }

                    var SelectedProperties = new HashSet<string>();
                    if (Request["properties"] != null)
                        SelectedProperties = new HashSet<string>(Request["properties"].Split(','));
                        
                    IEnumerable<SKUCategoryProperties> CatProp = DB.SKUCategoryProperties.Where(q => q.company_id == edited.company_id && string.Compare(q.category_id, edited.category_id, true) == 0);

                    var CurrentProperties = new HashSet<string>(CatProp.Select(c => c.property_id));

                    IEnumerable<string> NewProperties = SelectedProperties.Where(p => !CurrentProperties.Contains(p));
                    if (NewProperties != null && NewProperties.Count() > 0)
                    {
                        foreach (string prop in NewProperties)
                        {
                            SKUCategoryProperties NewProp = new SKUCategoryProperties();
                            NewProp.company_id = edited.company_id;
                            NewProp.category_id = edited.category_id;
                            NewProp.property_id = prop;
                            NewProp.created_by = User.Identity.Name;

                            DB.SKUCategoryProperties.Add(NewProp);
                            IsDirty = true;
                        }
                    }

                    IEnumerable<SKUCategoryProperties> DeletedProperties = CatProp.Where(c =>
                        !SelectedProperties.Contains(c.property_id));
                    if (DeletedProperties != null && DeletedProperties.Count() > 0)
                    {
                        foreach (SKUCategoryProperties deleted in DeletedProperties)
                        {
                            DB.Entry(deleted).State = System.Data.Entity.EntityState.Deleted;
                            IsDirty = true;
                        }
                    }

                    if (IsDirty)
                    {
                        Categories.ElementAt(0).revised_by = User.Identity.Name;
                        Categories.ElementAt(0).revised_date = DateTime.Now;

                        DB.Entry(Categories.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;

                        DB.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Cannot update category information");
            return View(edited);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string categoryID, int companyID)
        {
            IEnumerable<SKUCategories> Categories = DB.SKUCategories.Where(c => 
                c.company_id == companyID && string.Compare(c.category_id, categoryID, true) == 0);
            
            bool IsDirty = false;
            
            if (Categories != null && Categories.Count() > 0)
            {
                DB.Entry(Categories.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            IEnumerable<SKUCategoryProperties> Properties = DB.SKUCategoryProperties.Where(p => 
                p.company_id == companyID && string.Compare(p.category_id, categoryID, true) == 0);
            if(Properties!=null && Properties.Count()>0)
            {
                foreach (SKUCategoryProperties prop in Properties)
                    DB.Entry(prop).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            if (IsDirty)
                DB.SaveChanges();

            return RedirectToAction("Index");
        }
	}
}