﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using Newtonsoft.Json;
using System.IO;

namespace DaVinxi.Controllers
{
    public class DefectController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        public string DefectRootParentID = "0x0000";
        public static string LastClickedDefectID = "";

        // GET: Defect
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            if (CompanyID > 0 && DB.Defects.Where(d => d.company_id == CompanyID &&
                string.Compare(d.defect_id, "root-node", true) == 0).Count() <= 0)
            {
                MasterDefect DefaultDefect = new MasterDefect();
                DefaultDefect.company_id = CompanyID;
                DefaultDefect.defect_id = "root-node";
                DefaultDefect.defect_name = "Uncategorized";
                DefaultDefect.parent_defect_id = DefectRootParentID;
                DefaultDefect.data_type = "-";
                DefaultDefect.test_method = "-";
                DefaultDefect.pass_condition = "-";
                DefaultDefect.fail_condition = "-";
                DefaultDefect.revised_by = User.Identity.Name;

                DB.Defects.Add(DefaultDefect);
                DB.SaveChanges();
            }

            ViewBag.Companies = Helper.GetCompanies(CompanyID);

            return View();
        }

        [Authorize]
        public string GetAncestry(int companyID, string defectID, string targetParentID)
        {
            while (string.Compare(defectID, DefectRootParentID, true) != 0 && string.Compare(defectID, targetParentID, true) != 0)
            {
                MasterDefect Defect = DB.Defects.Where(d => d.company_id == companyID && 
                    string.Compare(d.defect_id, defectID, true) == 0).FirstOrDefault();
                if (Defect == null)
                    return DefectRootParentID;
                else
                    defectID = Defect.parent_defect_id;
            }

            if (string.Compare(defectID, targetParentID, true) != 0)
                return DefectRootParentID;
            return defectID;
        }

        [Authorize]
        public ActionResult Register(int companyID,string parentID)
        {
            MasterDefect NewDefect = new MasterDefect();
            NewDefect.company_id = companyID;
            NewDefect.parent_defect_id = parentID;
            NewDefect.revised_by = User.Identity.Name;
            return View(NewDefect);
        }

        [Authorize]
        private string SaveRefFile(MasterDefect defect, HttpPostedFileBase reference_file)
        {
            string RefFileName = defect.reference_file;
            try
            {
                if (reference_file != null && reference_file.ContentLength > 0 &&
                            reference_file.ContentType.ToLower().Contains("pdf"))
                {
                    if (RefFileName == null || RefFileName.Length <= 0)
                        RefFileName = string.Format("ref_file_{0}_{1}_{2}.pdf", defect.defect_id,
                                defect.company_id, DateTime.Now.ToString("yyyyMMddHHmmss"));
                    
                    if (!Directory.Exists(Server.MapPath(Constant.DefectRefFilePath)))
                        Directory.CreateDirectory(Server.MapPath(Constant.DefectRefFilePath));

                    reference_file.SaveAs(Path.Combine(
                        Server.MapPath(Constant.DefectRefFilePath), RefFileName));

                    if (defect.reference_file == null || defect.reference_file.Length <= 0)
                        defect.reference_file = RefFileName;
                }
            }
            catch(Exception)
            {
                FileInfo RefFile = new FileInfo(Path.Combine(
                            Server.MapPath(Constant.DefectRefFilePath), RefFileName));
                if (RefFileName.Length > 0 && RefFile.Exists)
                    RefFile.Delete();

                throw;
            }

            return RefFileName;
        }

        [Authorize]
        private List<string> SavePhotos(MasterDefect defect, IEnumerable<HttpPostedFileBase> photos)
        {
            List<string> PhotoFilenames = null;

            try
            {
                if (photos != null && photos.Count() > 0)
                {
                    PhotoFilenames = new List<string>();
                    foreach (HttpPostedFileBase photo in photos)
                    {
                        if (photo == null || photo.ContentLength <= 0 || !photo.ContentType.Contains("image"))
                            continue;

                        string PhotoID = string.Format("defect_photos_{0}_{1}_{2}", defect.defect_id,
                            defect.company_id, DateTime.Now.ToString("yyyyMMddHHmmssfffffff"));

                        PhotoFilenames.Add(string.Format("{0}{1}", PhotoID, Path.GetExtension(photo.FileName)));

                        if (!Directory.Exists(Server.MapPath(Constant.DefectImagePath)))
                            Directory.CreateDirectory(Server.MapPath(Constant.DefectImagePath));

                        photo.SaveAs(Path.Combine(
                            Server.MapPath(Constant.DefectImagePath), PhotoFilenames[PhotoFilenames.Count - 1]));

                        DefectPhotos NewPhotos = new DefectPhotos();
                        NewPhotos.company_id = defect.company_id;
                        NewPhotos.photo_id = PhotoID;
                        NewPhotos.defect_id = defect.defect_id;
                        NewPhotos.image_path = PhotoFilenames[PhotoFilenames.Count - 1];
                        NewPhotos.created_by = User.Identity.Name;

                        DB.DefectPhotos.Add(NewPhotos);
                    }
                }
            }
            catch(Exception)
            {
                if(PhotoFilenames!=null)
                {
                    foreach(string filename in PhotoFilenames)
                    {
                        FileInfo Photo = new FileInfo(Path.Combine(
                            Server.MapPath(Constant.DefectImagePath), filename));
                        if (filename.Length > 0 && Photo.Exists)
                            Photo.Delete();
                    }
                }

                throw;
            }

            return PhotoFilenames;
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(MasterDefect register, HttpPostedFileBase ref_file, IEnumerable<HttpPostedFileBase> photos)
        {
            string RefFileName = "";
            List<string> PhotoFilenames = null;

            try
            {
                if (ModelState.IsValid && register.defect_id != null && register.defect_id.Length > 0 &&
                    DB.Defects.Where(d => d.company_id == register.company_id &&
                        string.Compare(d.defect_id, register.defect_id, true) == 0).Count() <= 0)
                {
                    RefFileName = SaveRefFile(register, ref_file);

                    PhotoFilenames = SavePhotos(register, photos);

                    DB.Defects.Add(register);
                    DB.SaveChanges();

                    LastClickedDefectID = register.defect_id;

                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError("", "Defect already existed");
            }
            catch (Exception ex)
            {
                FileInfo RefFile = new FileInfo(Path.Combine(
                            Server.MapPath(Constant.DefectRefFilePath), RefFileName));
                if (RefFileName.Length > 0 && RefFile.Exists)
                    RefFile.Delete();

                if (PhotoFilenames != null)
                {
                    foreach (string filename in PhotoFilenames)
                    {
                        FileInfo Photo = new FileInfo(Path.Combine(
                            Server.MapPath(Constant.DefectImagePath), filename));
                        if (filename.Length > 0 && Photo.Exists)
                            Photo.Delete();
                    }
                }

                ModelState.AddModelError("exception", ex.Message);
            }

            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            return View(register);
        }

        [Authorize]
        public ActionResult Update(int companyID, string defectID)
        {
            MasterDefect Defect = DB.Defects.Where(d => d.company_id == companyID &&
                string.Compare(d.defect_id, defectID, true) == 0).FirstOrDefault();
            if (Defect == null)
                return RedirectToAction("Index");

            ViewBag.AvailableDefect = DB.Defects.Where(d => d.company_id == companyID && 
                string.Compare(d.defect_id, defectID, true) != 0);

            return View(Defect);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(MasterDefect defect, HttpPostedFileBase ref_file, 
            IEnumerable<HttpPostedFileBase> photos)
        {
            if (ModelState.IsValid)
            {
                MasterDefect CurrentDefect = DB.Defects.Where(d => d.company_id == defect.company_id && 
                    string.Compare(d.defect_id, defect.defect_id, true) == 0).FirstOrDefault();
                if (CurrentDefect != null)
                {
                    SaveRefFile(defect, ref_file);
                    SavePhotos(defect, photos);

                    if (string.Compare(defect.defect_name, CurrentDefect.defect_name) != 0)
                        CurrentDefect.defect_name = defect.defect_name;

                    if (string.Compare(defect.parent_defect_id, CurrentDefect.parent_defect_id, true) != 0)
                        CurrentDefect.parent_defect_id = defect.parent_defect_id;

                    if (string.Compare(defect.data_type, CurrentDefect.data_type) != 0)
                        CurrentDefect.data_type = defect.data_type;

                    if (string.Compare(defect.test_method, CurrentDefect.test_method) != 0)
                        CurrentDefect.test_method = defect.test_method;

                    if (string.Compare(defect.reference_file, CurrentDefect.reference_file) != 0)
                        CurrentDefect.reference_file = defect.reference_file;

                    if (string.Compare(defect.pass_condition, CurrentDefect.pass_condition) != 0)
                        CurrentDefect.pass_condition = defect.pass_condition;

                    if (string.Compare(defect.fail_condition, CurrentDefect.fail_condition) != 0)
                        CurrentDefect.fail_condition = defect.fail_condition;

                    if (string.Compare(defect.description, CurrentDefect.description) != 0)
                        CurrentDefect.description = defect.description;

                    CurrentDefect.revised_date = DateTime.Now;
                    CurrentDefect.revised_by = User.Identity.Name;
                    DB.Entry(CurrentDefect).State = System.Data.Entity.EntityState.Modified;

                    LastClickedDefectID = CurrentDefect.defect_id;

                    DB.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ViewBag.AvailableDefect = DB.Defects.Where(d => d.company_id == defect.company_id &&
                string.Compare(d.defect_id, defect.defect_id, true) != 0);

            return View(defect);
        }

        [Authorize]
        public ActionResult Delete(int companyID, string defectID)
        {
            bool IsDirty = false;

            MasterDefect Defect = DB.Defects.Where(d => d.company_id == companyID &&
                string.Compare(d.defect_id, defectID, true) == 0).FirstOrDefault();
            if (Defect != null)
            {
                if (Defect.reference_file != null && Defect.reference_file.Length > 0)
                {
                    FileInfo RefFile = new FileInfo(Path.Combine(Server.MapPath(Constant.DefectRefFilePath), Defect.reference_file));
                    if (RefFile.Exists)
                        RefFile.Delete();
                }
                DB.Entry(Defect).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            List<MasterDefect> ChildDefects = DB.Defects.Where(d => d.company_id == companyID && 
                string.Compare(d.parent_defect_id, defectID, true) == 0).ToList();
            if(ChildDefects!=null && ChildDefects.Count>0)
            {
                foreach(MasterDefect child in ChildDefects)
                {
                    child.parent_defect_id = "root-node";
                    DB.Entry(child).State = System.Data.Entity.EntityState.Modified;
                    IsDirty = true;
                }
            }

            List<DefectPhotos> Photos = DB.DefectPhotos.Where(d => d.company_id == companyID && 
                string.Compare(d.defect_id, defectID, true) == 0).ToList();
            if (Photos != null && Photos.Count > 0)
            {
                foreach (DefectPhotos photo in Photos)
                {
                    if (photo.image_path != null && photo.image_path.Length > 0)
                    {
                        FileInfo PhotoFile = new FileInfo(Path.Combine(Server.MapPath(Constant.DefectImagePath), photo.image_path));
                        if (PhotoFile.Exists)
                            PhotoFile.Delete();
                    }
                    DB.Entry(photo).State = System.Data.Entity.EntityState.Deleted;

                    IsDirty = true;
                }
            }

            if (IsDirty)
                DB.SaveChanges();

            LastClickedDefectID = null;

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public string DeleteRefFile(int companyID, string defectID)
        {
            MasterDefect Defect = DB.Defects.Where(d => d.company_id == companyID &&
                string.Compare(d.defect_id, defectID, true) == 0).FirstOrDefault();
            if (Defect != null && Defect.reference_file != null && Defect.reference_file.Length > 0)
            {
                FileInfo RefFile = new FileInfo(Path.Combine(Server.MapPath(Constant.DefectRefFilePath), Defect.reference_file));
                if (RefFile.Exists)
                    RefFile.Delete();
                Defect.reference_file = null;
                Defect.revised_date = DateTime.Now;
                Defect.revised_by = User.Identity.Name;
                DB.Entry(Defect).State = System.Data.Entity.EntityState.Modified;
                DB.SaveChanges();
            }
            return "0x00";
        }

        [Authorize]
        [HttpPost]
        public string DeleteSinglePhoto(int companyID, string photoID)
        {
            DefectPhotos Photo = DB.DefectPhotos.Where(d => d.company_id == companyID &&
                string.Compare(d.photo_id, photoID, true) == 0).FirstOrDefault();
            if (Photo != null && Photo.image_path != null && Photo.image_path.Length > 0)
            {
                FileInfo PhotoFile = new FileInfo(Path.Combine(Server.MapPath(Constant.DefectImagePath), Photo.image_path));
                if (PhotoFile.Exists)
                    PhotoFile.Delete();
                DB.Entry(Photo).State = System.Data.Entity.EntityState.Deleted;
                DB.SaveChanges();
            }
            return "0x00";
        }

        [Authorize]
        [HttpPost]
        public string DeleteAllPhotos(int companyID, string defectID)
        {
            List<DefectPhotos> Photos = DB.DefectPhotos.Where(d => d.company_id == companyID &&
                string.Compare(d.defect_id, defectID, true) == 0).ToList();
            if (Photos != null)
            {
                foreach (DefectPhotos photo in Photos)
                {
                    FileInfo PhotoFile = new FileInfo(Path.Combine(Server.MapPath(Constant.DefectImagePath), photo.image_path));
                    if (PhotoFile.Exists)
                        PhotoFile.Delete();
                    DB.Entry(photo).State = System.Data.Entity.EntityState.Deleted;
                }
                DB.SaveChanges();
            }
            return "0x00";
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetBreadCrumb(int companyID)
        {
            List<string> BreadCrumbs = null;


            if(LastClickedDefectID!=null && LastClickedDefectID.Length>0)
            {                
                string DefectID = LastClickedDefectID;

                while(string.Compare(DefectID,DefectRootParentID,true)!=0)
                {
                    MasterDefect Defect = DB.Defects.Where(d => d.company_id == companyID && 
                        string.Compare(d.defect_id, DefectID, true) == 0).FirstOrDefault();

                    if (Defect == null)
                        break;
                    else
                    {
                        if (BreadCrumbs == null)
                            BreadCrumbs = new List<string>();

                        BreadCrumbs.Add(Defect.defect_id);
                        DefectID = Defect.parent_defect_id;
                    }
                }
            }

            return Json(BreadCrumbs);
        }
    }
}