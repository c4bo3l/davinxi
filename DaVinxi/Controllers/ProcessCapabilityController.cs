﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using CabZFramework;

namespace DaVinxi.Controllers
{
    public class ProcessCapabilityController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: ProcessCapability
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [AllowCrossSiteJson]
        public ActionResult GetCapability(int company_id, string division_id, string block_id, string machine_id, 
            string category_id, string business_id, string segment_id, 
            string subcategory_id, string sku_id, string batch_id, string property_id)
        {
            try
            {
                var Capabilities = from qualityData in DB.ProductQualities
                                   join product in DB.Products on
                                       new { c1 = qualityData.company_id, c2 = qualityData.product_id } equals
                                       new { c1 = product.company_id, c2 = product.product_id }
                                   join batch in DB.Batches on
                                       new { c1 = product.company_id, c2 = product.batch_id } equals
                                       new { c1 = batch.company_id, c2 = batch.batch_id }
                                   join sku in DB.SKU on
                                       new { c1 = batch.company_id, c2 = batch.sku_id } equals
                                       new { c1 = sku.company_id, c2 = sku.sku_id }
                                   join segment in DB.SKUSegments on
                                      new { c1 = sku.company_id, c2 = sku.segment_id } equals
                                      new { c1 = segment.company_id, c2 = segment.segment_id }
                                   join business in DB.SKUBusiness on
                                       new { c1 = sku.company_id, c2 = sku.business_id } equals
                                       new { c1 = business.company_id, c2 = business.business_id }
                                   join subcategory in DB.SKUSubcategories on
                                       new { c1 = sku.company_id, c2 = sku.subcategory_id } equals
                                       new { c1 = subcategory.company_id, c2 = subcategory.subcategory_id }
                                   join category in DB.SKUCategories on
                                       new { c1 = subcategory.company_id, c2 = subcategory.category_id } equals
                                       new { c1 = category.company_id, c2 = category.category_id }
                                   join machine in DB.Machines on
                                       new { c1 = batch.company_id, c2 = batch.machine_id } equals
                                       new { c1 = machine.company_id, c2 = machine.machine_id }
                                   join block in DB.Blocks on
                                       new { c1 = machine.company_id, c2 = machine.block_id } equals
                                       new { c1 = block.company_id, c2 = block.block_id }
                                   join division in DB.Divisions on
                                       new { c1 = block.company_id, c2 = block.division_id } equals
                                       new { c1 = division.company_id, c2 = division.division_id }
                                   join property in DB.QualityProperties on
                                       new { c1 = qualityData.company_id, c2 = qualityData.quality_property_id } equals
                                       new { c1 = property.company_id, c2 = property.property_id }
                                   join propertySetting in DB.SKUPropertySettings on
                                       new { c1 = qualityData.company_id, c2 = qualityData.quality_property_id, c3 = batch.sku_id } equals
                                       new { c1 = propertySetting.company_id, c2 = propertySetting.property_id, c3 = propertySetting.sku_id }
                                   select new
                                   {
                                       qualityData,
                                       propertySetting,
                                       property.property_name,
                                       division.division_id,
                                       block.block_id,
                                       machine.machine_id,
                                       category.category_id,
                                       subcategory.subcategory_id,
                                       business.business_id,
                                       segment.segment_id,
                                       sku.sku_id,
                                       batch.batch_id
                                   };

                if (company_id > 0)
                    Capabilities = Capabilities.Where(c => c.qualityData.company_id == company_id);

                if (division_id != null && division_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.division_id.Equals(division_id));

                if (block_id != null && block_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.block_id.Equals(block_id));

                if (machine_id != null && machine_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.machine_id.Equals(machine_id));

                if (category_id != null && category_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.category_id.Equals(category_id));

                if (subcategory_id != null && subcategory_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.subcategory_id.Equals(subcategory_id));

                if (business_id != null && business_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.business_id.Equals(business_id));

                if (segment_id != null && segment_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.segment_id.Equals(segment_id));

                if (sku_id != null && sku_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.sku_id.Equals(sku_id));

                if (batch_id != null && batch_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.batch_id.Equals(batch_id));

                if (property_id != null && property_id.Length > 0)
                    Capabilities = Capabilities.Where(c => c.qualityData.quality_property_id.Equals(property_id));

                if (Capabilities.Count() <= 0)
                    return null;

                double Target = Math.Round(Capabilities.Select(c => c.qualityData.quality_value).Average(), 3, MidpointRounding.AwayFromZero),
                    StdDev = Math.Round(Statistics.StandardDeviation(Capabilities.Select(c => c.qualityData.quality_value).ToArray()), 3, MidpointRounding.AwayFromZero),
                    Pp = 0, Ppk = 0;

                if (Capabilities.Count() > 0)
                {
                    double URL = Capabilities.Select(c => c.propertySetting.url).FirstOrDefault(), 
                        LRL = Capabilities.Select(c => c.propertySetting.lrl).FirstOrDefault(),
                        ActTarget = Capabilities.Select(c => c.propertySetting.target).FirstOrDefault();
                    Pp = Math.Round((URL - LRL) / (6.0 * StdDev), 3, MidpointRounding.AwayFromZero);
                    Ppk = Math.Round(Math.Min((URL - ActTarget) / (3.0 * StdDev), (ActTarget - LRL) / (3.0 * StdDev)), 3, MidpointRounding.AwayFromZero);
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        oData = Capabilities,
                        target = Target,
                        stdDev = StdDev,
                        pp = Pp,
                        ppk = Ppk,
                        cp = Math.Round(Capabilities.Select(c => c.propertySetting.cp).FirstOrDefault(), 3, MidpointRounding.AwayFromZero),
                        cpk = Math.Round(Capabilities.Select(c => c.propertySetting.cpk).FirstOrDefault(), 3, MidpointRounding.AwayFromZero),
                        MinOutside = new
                        {
                            point = Target - (4 * StdDev),
                            count = Capabilities.Where(c => c.qualityData.quality_value < (Target - (3 * StdDev))).Count()
                        },
                        Min3s = new
                        {
                            point = Target - (3 * StdDev),
                            count = Capabilities.Where(c => c.qualityData.quality_value >= (Target - (3 * StdDev)) &&
                                    c.qualityData.quality_value < (Target - (2 * StdDev))).Count()
                        },
                        Min2s = new
                        {
                            point = Target - (2 * StdDev),
                            count = Capabilities.Where(c => c.qualityData.quality_value >= (Target - (2 * StdDev)) &&
                            c.qualityData.quality_value < (Target - (1 * StdDev))).Count()
                        },
                        Min1s = new
                        {
                            point = Target - StdDev,
                            count = Capabilities.Where(c => c.qualityData.quality_value >= (Target - (1 * StdDev)) &&
                                c.qualityData.quality_value < (Target - (0 * StdDev))).Count()
                        },
                        Max1s = new
                        {
                            point = Target,
                            count = Capabilities.Where(c => c.qualityData.quality_value >= (Target + (0 * StdDev)) &&
                                c.qualityData.quality_value < (Target + (1 * StdDev))).Count()
                        },
                        Max2s = new
                        {
                            point = Target + StdDev,
                            count = Capabilities.Where(c => c.qualityData.quality_value >= (Target + (1 * StdDev)) &&
                                c.qualityData.quality_value < (Target + (2 * StdDev))).Count()
                        },
                        Max3s = new
                        {
                            point = Target + (2 * StdDev),
                            count = Capabilities.Where(c => c.qualityData.quality_value >= (Target + (2 * StdDev)) &&
                                c.qualityData.quality_value < (Target + (3 * StdDev))).Count()
                        },
                        MaxOutside = new
                        {
                            point = Target + (3 * StdDev),
                            count = Capabilities.Where(c => c.qualityData.quality_value > (Target + (3 * StdDev))).Count()
                        }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception)
            {
                return Json(null);
            }
        }
    }
}