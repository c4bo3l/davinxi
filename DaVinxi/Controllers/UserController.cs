﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using System.Web.Security;

namespace DaVinxi.Controllers
{
    public class UserController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /User/

        [Authorize]
        public ActionResult Index()
        {
            List<MasterUser> Users = Helper.GetUsers(Request.Cookies["company_id"] != null ? int.Parse(Request.Cookies["company_id"].Value) : 0);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            foreach (MasterUser user in Users)
            {
                if (!RelatedCompanies.ContainsKey(user.company_id))
                {
                    String CompanyName = "All Companies";

                    if (user.company_id > 0)
                    {
                        MasterCompanies Company = DB.Companies.Find(user.company_id);
                        if (Company != null)
                            CompanyName = Company.company_name;
                    }

                    RelatedCompanies.Add(user.company_id, CompanyName);
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;

            return View(Users);
        }


        [AllowAnonymous]
        public ActionResult Login(string returnURL)
        {
            ViewBag.ReturnURL = returnURL;
            return View();
        }

        // POST : /User/Login

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel login, string returnURL)
        {
            if (ModelState.IsValid)
            {
                MasterUser User = DB.Users.Find(login.UserName);
                if (User != null && 
                    string.Compare(User.password, Constant.AESSecure.Encrypt(login.Password)) == 0)
                {
                    FormsAuthentication.SetAuthCookie(User.username, login.RememberMe);

                    Response.Cookies.Remove("real_name");
                    Response.SetCookie(new HttpCookie("real_name", User.full_name));

                    Response.Cookies.Remove("company_id");
                    Response.SetCookie(new HttpCookie("company_id", User.company_id.ToString()));

                    return RedirectToLocal(returnURL);
                }
            }

            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(login);
        }

        [HttpGet]
        [AllowAnonymous]
        [AllowCrossSiteJson]
        public ActionResult LoginJSON(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                MasterUser User = DB.Users.Find(login.UserName);
                if (User != null &&
                    string.Compare(User.password, Constant.AESSecure.Encrypt(login.Password)) == 0)
                {
                    return Json(new
                    {
                        status = "OK",
                        data = new
                        {
                            name = User.full_name,
                            company_id = User.company_id
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new
            {
                status = "Failed"
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangePassword(string new_password, string re_new_password) {
            string Message = "";
            int Status = 1;
            try
            {
                if (string.Compare(new_password, re_new_password) != 0)
                    throw new Exception("Passwords don't match");
                MasterUser LoggedInUser = DB.Users.Where(u => string.Compare(u.username, User.Identity.Name, true) == 0).FirstOrDefault();
                if (LoggedInUser == null)
                    throw new Exception("User not found");
                LoggedInUser.password = Constant.AESSecure.Encrypt(new_password);
                LoggedInUser.revised_date = DateTime.Now;
                LoggedInUser.revised_by = User.Identity.Name;
                DB.Entry(LoggedInUser).State = System.Data.Entity.EntityState.Modified;
                DB.SaveChanges();
            }
            catch (Exception ex) {
                Status = -1;
                Message = ex.Message;
            }

            return Json(new
            {
                status = Status,
                message = Message
            });
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            
            Response.Cookies.Remove("real_name");
            Response.Cookies.Remove("company_id");

            return RedirectToAction("Index", "Home");
        }

        private List<MasterCompanies> GetCompanies(int companyID = 0)
        {
            List<MasterCompanies> Companies = Helper.GetCompanies(companyID);

            if (companyID <= 0)
            {
                MasterCompanies NewCompany = new MasterCompanies();
                NewCompany.company_id = -1;
                NewCompany.company_name = "All Companies";

                Companies.Insert(0, NewCompany);
            }
            
            return Companies;
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = GetCompanies(int.Parse(Request.Cookies["company_id"].Value));
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(UserRegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                MasterUser MUser = DB.Users.Find(registerModel.username);
                if (MUser != null)
                    ModelState.AddModelError("", "The user name has been used.");
                else
                {
                    MUser = new MasterUser();

                    MUser.username = registerModel.username;
                    MUser.password = Constant.AESSecure.Encrypt(registerModel.password);
                    MUser.full_name = registerModel.full_name;
                    MUser.email = registerModel.email;
                    MUser.phone_number = registerModel.phone_number;
                    MUser.department = registerModel.department;
                    MUser.revised_by = User.Identity.Name;
                    MUser.company_id = registerModel.company_id;

                    DB.Users.Add(MUser);
                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ViewBag.Companies = GetCompanies(int.Parse(Request.Cookies["company_id"].Value));
            return View(registerModel);
        }

        [Authorize]
        public ActionResult Edit(string username)
        {
            MasterUser User = DB.Users.Find(username);
            if (User == null)
                return HttpNotFound();

            UserEditModel UserEdit = new UserEditModel();
            UserEdit.username = User.username;
            UserEdit.full_name = User.full_name;
            UserEdit.email = User.email;
            UserEdit.phone_number = User.phone_number;
            UserEdit.department = User.department;
            UserEdit.company_id = User.company_id;

            ViewBag.Companies = GetCompanies(int.Parse(Request.Cookies["company_id"].Value));

            return View(UserEdit);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(UserEditModel editUser)
        {
            if(ModelState.IsValid)
            {
                MasterUser MUser = DB.Users.Find(editUser.username);
                if (MUser == null)
                    return HttpNotFound();

                bool IsDirty = false;

                if (string.Compare(editUser.full_name, MUser.full_name,true) != 0)
                {
                    MUser.full_name = editUser.full_name;
                    IsDirty = true;

                    if (string.Compare(User.Identity.Name, editUser.username, true) == 0)
                        Response.Cookies["real_name"].Value = editUser.full_name;
                }

                if (string.Compare(editUser.email, MUser.email, true) != 0)
                {
                    MUser.email = editUser.email;
                    IsDirty = true;
                }

                if (string.Compare(editUser.phone_number, MUser.phone_number) != 0)
                {
                    MUser.phone_number = editUser.phone_number;
                    IsDirty = true;
                }

                if (editUser.company_id != MUser.company_id)
                {
                    MUser.company_id = editUser.company_id;
                    IsDirty = true;
                }

                if (string.Compare(editUser.department, MUser.department, true) != 0)
                {
                    MUser.department = editUser.department;
                    IsDirty = true;
                }

                if (IsDirty)
                {
                    MUser.revised_by = User.Identity.Name;
                    MUser.revised_date = DateTime.Now;
                    DB.Entry(MUser).State = System.Data.Entity.EntityState.Modified;
                    DB.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.Companies = GetCompanies(int.Parse(Request.Cookies["company_id"].Value));
            return View(editUser);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string username)
        {
            if (username != null && username.Length > 0)
            {
                MasterUser MUser = DB.Users.Find(username);
                if (MUser == null)
                    return HttpNotFound();

                DB.Entry(MUser).State = System.Data.Entity.EntityState.Deleted;
                DB.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}
