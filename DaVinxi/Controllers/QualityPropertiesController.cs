﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class QualityPropertiesController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /QualityProperties/
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0;

            List<QualityProperties> QCProc = Helper.GetQualityProperties(CompanyID);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, string> RelatedUnits = new Dictionary<string, string>();

            Dictionary<string, string> RelatedDefect = Helper.GetDefects(CompanyID).Where(
                d => QCProc.Select(q => q.lrl_defect).Contains(d.defect_id) ||
                    QCProc.Select(q => q.url_defect).Contains(d.defect_id)).Select(d =>
                        new { key = d.defect_id, value = d.defect_name }).ToDictionary(d => d.key, d => d.value);

            if (QCProc.Count > 0)
            {
                foreach (QualityProperties qProp in QCProc)
                {
                    if (!RelatedCompanies.ContainsKey(qProp.company_id))
                    {
                        String CompanyName = "All Companies";

                        if (qProp.company_id > 0)
                        {
                            MasterCompanies Company = DB.Companies.Find(qProp.company_id);
                            if (Company != null)
                                CompanyName = Company.company_name;
                        }

                        RelatedCompanies.Add(qProp.company_id, CompanyName);
                    }

                    if (!RelatedUnits.ContainsKey(qProp.unit_id))
                    {
                        String UnitName = "";

                        IEnumerable<MasterEngineeringUnits> Units = 
                            DB.EngineeringUnits.Where(
                            u => u.company_id == qProp.company_id &&
                                string.Compare(u.unit_id, qProp.unit_id, true) == 0);

                        if (Units != null && Units.Count() > 0)
                            UnitName = Units.ElementAt(0).unit_name;

                        RelatedUnits.Add(qProp.unit_id, UnitName);
                    }
                }
            }
            ViewBag.RelatedDefect = RelatedDefect;
            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedUnits = RelatedUnits;
            return View(QCProc);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(
                Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(QualityPropRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.QualityProperties.Where(q => q.company_id == register.company_id && 
                    string.Compare(q.property_id, register.property_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Quality property already existed");
                else
                {
                    QualityProperties NewProperty = new QualityProperties();
                    NewProperty.company_id = register.company_id;
                    NewProperty.property_id = register.property_id;
                    NewProperty.property_name = register.property_name;
                    NewProperty.unit_id = register.unit_id;
                    NewProperty.test_method = register.test_method;
                    NewProperty.reference = register.reference;
                    NewProperty.lrl_defect = register.lrl_defect;
                    NewProperty.url_defect = register.url_defect;
                    NewProperty.revised_by = User.Identity.Name;

                    DB.QualityProperties.Add(NewProperty);
                    DB.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot register new quality property");

            ViewBag.Companies = Helper.GetCompanies(
                Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string id)
        {
            IEnumerable<QualityProperties> QProp = 
                DB.QualityProperties.Where(q => q.company_id == companyID && 
                    string.Compare(q.property_id, id, true) == 0);
            if (QProp == null || QProp.Count() <= 0)
                return RedirectToAction("Index");

            QualityPropRegisterModel QPropEdit = new QualityPropRegisterModel();
            QPropEdit.company_id = QProp.ElementAt(0).company_id;
            QPropEdit.property_id = QProp.ElementAt(0).property_id;
            QPropEdit.property_name = QProp.ElementAt(0).property_name;
            QPropEdit.unit_id = QProp.ElementAt(0).unit_id;
            QPropEdit.test_method = QProp.ElementAt(0).test_method;
            QPropEdit.reference = QProp.ElementAt(0).reference;
            QPropEdit.lrl_defect = QProp.ElementAt(0).lrl_defect;
            QPropEdit.url_defect = QProp.ElementAt(0).url_defect;

            return View(QPropEdit);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(QualityPropRegisterModel edited)
        {
            if(ModelState.IsValid)
            {
                IEnumerable<QualityProperties> QProp = 
                    DB.QualityProperties.Where(q => q.company_id == edited.company_id && 
                        string.Compare(q.property_id, edited.property_id, true) == 0).ToList();
                if (QProp != null && QProp.Count() > 0)
                {

                    bool IsDirty = false;

                    if (string.Compare(edited.property_name, QProp.ElementAt(0).property_name, true) != 0)
                    {
                        IsDirty = true;
                        QProp.ElementAt(0).property_name = edited.property_name;
                    }

                    if (string.Compare(edited.unit_id, QProp.ElementAt(0).unit_id, true) != 0)
                    {
                        IsDirty = true;
                        QProp.ElementAt(0).unit_id = edited.unit_id;
                    }

                    if (string.Compare(edited.test_method, QProp.ElementAt(0).test_method, true) != 0)
                    {
                        IsDirty = true;
                        QProp.ElementAt(0).test_method = edited.test_method;
                    }

                    if (string.Compare(edited.reference, QProp.ElementAt(0).reference, true) != 0)
                    {
                        IsDirty = true;
                        QProp.ElementAt(0).reference = edited.reference;
                    }

                    if (string.Compare(edited.lrl_defect, QProp.ElementAt(0).lrl_defect, true) != 0)
                    {
                        IsDirty = true;
                        QProp.ElementAt(0).lrl_defect = edited.lrl_defect;
                    }

                    if (string.Compare(edited.url_defect, QProp.ElementAt(0).url_defect, true) != 0)
                    {
                        IsDirty = true;
                        QProp.ElementAt(0).url_defect = edited.url_defect;
                    }

                    if (IsDirty)
                    {
                        QProp.ElementAt(0).revised_date = DateTime.Now;
                        QProp.ElementAt(0).revised_by = User.Identity.Name;

                        DB.Entry(QProp.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;
                        DB.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            return View(edited);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string propertyID, int companyID)
        {
            IEnumerable<QualityProperties> QProp = DB.QualityProperties.Where(q => q.company_id == companyID && string.Compare(q.property_id, propertyID, true) == 0);

            if (QProp != null && QProp.Count() > 0)
            {
                DB.Entry(QProp.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                DB.SaveChanges();
            }

            return RedirectToAction("Index");
        }
	}
}