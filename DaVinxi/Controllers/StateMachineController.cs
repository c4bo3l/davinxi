﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
namespace DaVinxi.Controllers
{
    public class StateMachineController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        [Authorize]
        // GET: StateMachine
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View();
        }

        [Authorize]
        public ActionResult Register(int company_id,string state_machine_id)
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                    int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                    int.Parse(Request.Cookies["company_id"].Value) : 0;
            if (company_id > 0)
                CompanyID = company_id;
            MasterStateMachine StateMachine = new MasterStateMachine();
            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            if (state_machine_id != null && state_machine_id.Length > 0)
            {
                ViewBag.States = DB.StateMachineStates.Where(s => s.company_id == company_id && 
                    string.Compare(s.state_machine_id, state_machine_id, true) == 0);
                ViewBag.Variables = DB.StateMachineVariables.Where(v => v.company_id == company_id && 
                    string.Compare(v.state_machine_id, state_machine_id, true) == 0);
                StateMachine = DB.StateMachines.Where(s => s.company_id == company_id && 
                    string.Compare(s.state_machine_id, state_machine_id, true) == 0).FirstOrDefault();
            }
            return View(StateMachine);
        }

        [Authorize]
        public ActionResult GetStateMachines(int company_id)
        {
            IEnumerable<MasterStateMachine> SMachines = DB.StateMachines.ToList();

            if (company_id > 0)
                SMachines = SMachines.Where(s => s.company_id == company_id);
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
                Data = SMachines
            };
        }

        [Authorize]
        public ActionResult GetStates(int company_id, string state_machine_id)
        {
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
                Data = DB.StateMachineStates.Where(s => s.company_id == company_id && 
                    string.Compare(s.state_machine_id, state_machine_id, true) == 0)
            };
        }

        [Authorize]
        public ActionResult GetVariables(int company_id, string state_machine_id)
        {
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = DB.StateMachineVariables.Where(v => v.company_id == company_id && 
                    string.Compare(v.state_machine_id, state_machine_id, true) == 0)
            };
        }

        [Authorize]
        public ActionResult GetTransitions(int company_id, string state_machine_id)
        {
            var Results = from t in DB.StateMachineTransitions
                          join source_master in DB.StateMachines on new { c1 = t.company_id, c2 = t.source_state_machine_id }
                          equals new { c1 = source_master.company_id, c2 = source_master.state_machine_id }
                          join target_master in DB.StateMachines on new { c1 = t.company_id, c2 = t.target_state_machine_id }
                          equals new { c1 = target_master.company_id, c2 = target_master.state_machine_id }
                          join source_state in DB.StateMachineStates on new { c1 = t.company_id, c2 = t.source_state_machine_id, c3 = t.source_state_id }
                          equals new { c1 = source_state.company_id, c2 = source_state.state_machine_id, c3 = source_state.state_id } into sState
                          from sourceState in sState.DefaultIfEmpty()
                          join target_state in DB.StateMachineStates on new { c1 = t.company_id, c2 = t.target_state_machine_id, c3 = t.target_state_id }
                          equals new { c1 = target_state.company_id, c2 = target_state.state_machine_id, c3 = target_state.state_id }
                          where t.company_id == company_id && string.Compare(t.state_machine_id, state_machine_id, true) == 0
                          select new
                          {
                              Trans = t,
                              source_master = source_master,
                              target_master = target_master,
                              source_state = sourceState,
                              target_state = target_state
                          };
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = Results
            };
        }

        [Authorize, HttpPost]
        public ActionResult GetTransitionsByState(int company_id, string state_machine_id, 
            string source_state_machine_id, string state_id)
        {
            var Results = from t in DB.StateMachineTransitions
                          join source_master in DB.StateMachines on new { c1 = t.company_id, c2 = t.source_state_machine_id }
                          equals new { c1 = source_master.company_id, c2 = source_master.state_machine_id }
                          join target_master in DB.StateMachines on new { c1 = t.company_id, c2 = t.target_state_machine_id }
                          equals new { c1 = target_master.company_id, c2 = target_master.state_machine_id }
                          join source_state in DB.StateMachineStates on new { c1 = t.company_id, c2 = t.source_state_machine_id, c3 = t.source_state_id }
                          equals new { c1 = source_state.company_id, c2 = source_state.state_machine_id, c3 = source_state.state_id } into sState
                          from sourceState in sState.DefaultIfEmpty()
                          join target_state in DB.StateMachineStates on new { c1 = t.company_id, c2 = t.target_state_machine_id, c3 = t.target_state_id }
                          equals new { c1 = target_state.company_id, c2 = target_state.state_machine_id, c3 = target_state.state_id }
                          where t.company_id == company_id
                          select new
                          {
                              Trans = t,
                              source_master = source_master,
                              target_master = target_master,
                              source_state = sourceState,
                              target_state = target_state,
                              state_length = (sourceState == null ? 0 : t.source_state_id.Length),
                              is_same_machine = (string.Compare(t.source_state_machine_id, t.state_machine_id, true) == 0 ? 0 : 1)
                          };
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = Results.Where(r => (string.Compare(r.Trans.state_machine_id, state_machine_id, true) == 0 &&
                    r.state_length == 0) || (
                    string.Compare(r.Trans.source_state_id, state_id, true) == 0 &&
                    string.Compare(r.Trans.source_state_machine_id, source_state_machine_id, true) == 0)).OrderBy(r => r.is_same_machine).OrderBy(r => r.state_length)
            };
        }

        [Authorize, HttpPost]
        public ActionResult Register(MasterStateMachine sm, IEnumerable<StateMachineState> states, IEnumerable<StateMachineVariable> variables, IEnumerable<StateMachineTransition> transitions)
        {
            int status = 1;
            string message = "success";
            try
            {
                DeleteStateMachine(sm.company_id, sm.state_machine_id);
                sm.revised_by = User.Identity.Name;
                DB.StateMachines.Add(sm);

                if (states != null && states.Count() > 0)
                {
                    foreach (StateMachineState state in states)
                    {
                        state.company_id = sm.company_id;
                        state.state_machine_id = sm.state_machine_id;
                        state.revised_by = User.Identity.Name;
                        DB.StateMachineStates.Add(state);
                    }
                }

                if (variables != null && variables.Count() > 0)
                {
                    foreach (StateMachineVariable variable in variables)
                    {
                        variable.company_id = sm.company_id;
                        variable.state_machine_id = sm.state_machine_id;
                        variable.revised_by = User.Identity.Name;
                        DB.StateMachineVariables.Add(variable);
                    }
                }

                if (transitions != null && transitions.Count() > 0)
                {
                    int MaxSeq = DB.StateMachineTransitions.Where(t => t.company_id == sm.company_id &&
                        string.Compare(t.state_machine_id, sm.state_machine_id, true) == 0).Select(t => t.sequence).FirstOrDefault();
                    foreach (StateMachineTransition trans in transitions)
                    {
                        MaxSeq++;
                        trans.company_id = sm.company_id;
                        trans.state_machine_id = sm.state_machine_id;
                        trans.sequence = MaxSeq;
                        if (trans.source_state_machine_id == null)
                            trans.source_state_machine_id = sm.state_machine_id;
                        if (trans.target_state_machine_id == null)
                            trans.target_state_machine_id = sm.state_machine_id;
                        trans.revised_by = User.Identity.Name;

                        DB.StateMachineTransitions.Add(trans);
                    }
                }

                DB.SaveChanges();
            }
            catch (Exception ex)
            {
                DB.DetachAll();
                status = -1;
                message = ex.Message;
            }

            return new JsonResult()
            {
                Data = new
                {
                    status = status,
                    message = message
                }
            };
        }

        [Authorize]
        public bool DeleteStateMachine(int company_id, string state_machine_id)
        {
            try
            {
                bool IsDirty = false;
                MasterStateMachine StateMachine = DB.StateMachines.Where(s => s.company_id == company_id &&
                    string.Compare(s.state_machine_id, state_machine_id, true) == 0).FirstOrDefault();
                if (StateMachine != null)
                {
                    DB.Entry(StateMachine).State = System.Data.Entity.EntityState.Deleted;
                    IsDirty = true;
                }

                IEnumerable<StateMachineState> States = DB.StateMachineStates.Where(s => s.company_id == company_id &&
                    string.Compare(s.state_machine_id, state_machine_id, true) == 0);
                if (States != null && States.Count() > 0)
                {
                    foreach (StateMachineState s in States)
                        DB.Entry(s).State = System.Data.Entity.EntityState.Deleted;
                    IsDirty = true;
                }

                IEnumerable<StateMachineVariable> Variables = DB.StateMachineVariables.Where(s => s.company_id == company_id &&
                    string.Compare(s.state_machine_id, state_machine_id, true) == 0);
                if (Variables != null && Variables.Count() > 0)
                {
                    foreach (StateMachineVariable s in Variables)
                        DB.Entry(s).State = System.Data.Entity.EntityState.Deleted;
                    IsDirty = true;
                }

                IEnumerable<StateMachineTransition> Transitions = DB.StateMachineTransitions.Where(s => s.company_id == company_id &&
                    string.Compare(s.state_machine_id, state_machine_id, true) == 0);
                if (Transitions != null && Transitions.Count() > 0)
                {
                    foreach (StateMachineTransition s in Transitions)
                        DB.Entry(s).State = System.Data.Entity.EntityState.Deleted;
                    IsDirty = true;
                }

                if (IsDirty)
                    DB.SaveChanges();
            }
            catch (Exception)
            { return false; }
            return true;
        }

        [Authorize]
        public ActionResult Delete(int company_id, string state_machine_id)
        {
            int status = 1;
            string message = "success";
            try 
            {
                if (!DeleteStateMachine(company_id, state_machine_id))
                {
                    status = 0;
                    message = "";
                }
            }
            catch(Exception ex)
            {
                status = -1;
                message = ex.Message;
            }

            return Json(new { status = status, message = message });
        }

        [Authorize, HttpPost]
        public ActionResult GetTagData(int company_id, IEnumerable<string> tags, DateTime start_date, DateTime finish_date)
        {
            double StartOADate = start_date.ToOADate(), FinishOADate = finish_date.ToOADate();
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            List<TagData> Data = DB.TagValues.Where(t => t.company_id == company_id && tags.Contains(t.tag_id) &&
                t.timestamp >= StartOADate && t.timestamp <= FinishOADate).ToList();

            if (Data != null && Data.Count > 0 && tags != null && tags.Count() > 0)
            {
                foreach (string tag in tags)
                {
                    var TempData = Data.Where(d => d.company_id == company_id && string.Compare(d.tag_id, tag, true) == 0);

                    if (TempData != null && TempData.Count() > 0)
                    {
                        TagData FirstData = TempData.OrderBy(t => t.timestamp).First();
                        if (FirstData.timestamp < StartOADate)
                            Data[0].timestamp = StartOADate;
                        else if (FirstData.timestamp > StartOADate)
                        {
                            TagData LookForFirstData = DB.TagValues.Where(t => t.company_id == company_id &&
                                string.Compare(t.tag_id, tag, true) == 0 &&
                                t.timestamp < StartOADate).OrderByDescending(t => t.timestamp).FirstOrDefault();
                            if (LookForFirstData == null)
                                Data[0].timestamp = StartOADate;
                            else
                            {
                                /*double Gradient = (FirstData.value - LookForFirstData.value) /
                                    (FirstData.timestamp - LookForFirstData.timestamp);
                                double Intercept = FirstData.value - (Gradient * FirstData.timestamp);*/
                                LookForFirstData.timestamp = StartOADate;
                                //LookForFirstData.value = (Gradient * LookForFirstData.timestamp) + Intercept;
                                Data.Add(LookForFirstData);
                            }
                        }

                        TagData LastData = TempData.OrderByDescending(t => t.timestamp).First();
                        if (LastData.timestamp != FinishOADate)
                        {
                            TagData TempLastData = new TagData();
                            TempLastData.timestamp = FinishOADate;
                            TempLastData.tag_id = LastData.tag_id;
                            TempLastData.value = LastData.value;
                            Data.Add(TempLastData);
                        }
                    }
                    else
                    {
                        TagData LookForFirstData = DB.TagValues.Where(t => t.company_id == company_id &&
                                string.Compare(t.tag_id, tag, true) == 0 &&
                                t.timestamp < StartOADate).OrderByDescending(t => t.timestamp).FirstOrDefault();
                        if (LookForFirstData != null)
                            LookForFirstData.timestamp = StartOADate;
                        else {
                            LookForFirstData = new TagData();
                            LookForFirstData.tag_id = tag;
                            LookForFirstData.timestamp = StartOADate;
                            LookForFirstData.value = 0;
                        }
                        Data.Add(LookForFirstData);
                    }
                }
            }

            Data = Data.OrderBy(t => t.timestamp).ToList();

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = Data.Select(d => new
                {
                    info = d,
                    epoch_time = DateTime.FromOADate(d.timestamp).Subtract(Epoch).TotalMilliseconds
                })
            };
        }
    }
}