﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class SKUBusinessController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /SKUBusiness/
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0;

            List<SKUBusinesses> Businesses = Helper.GetSKUBusiness(CompanyID);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, List<QualityProperties>> RelatedProperties = new Dictionary<string, List<QualityProperties>>();

            if (Businesses.Count > 0)
            {
                foreach (SKUBusinesses business in Businesses)
                {
                    if (!RelatedCompanies.ContainsKey(business.company_id))
                    {
                        string CompanyName = "All Companies";
                        if (business.company_id > 0)
                        {
                            MasterCompanies Company = DB.Companies.Find(business.company_id);
                            if (Company != null)
                                CompanyName = Company.company_name;
                        }

                        RelatedCompanies.Add(business.company_id, CompanyName);
                    }

                    if (!RelatedProperties.ContainsKey(business.business_id))
                    {
                        List<QualityProperties> QProp =
                            Helper.GetQualityPropertiesBySKUBusiness(business.company_id, business.business_id);
                        if (QProp.Count > 0)
                            RelatedProperties.Add(business.business_id, QProp);
                    }
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedProperties = RelatedProperties;

            return View(Businesses);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(SKUBusinessRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.SKUBusiness.Where(q => q.company_id == register.company_id &&
                    string.Compare(q.business_id, register.business_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "SKU business already exist");
                else
                {
                    SKUBusinesses NewBusiness = new SKUBusinesses();
                    NewBusiness.company_id = register.company_id;
                    NewBusiness.business_id = register.business_id;
                    NewBusiness.business_name = register.business_name;
                    NewBusiness.revised_by = User.Identity.Name;
                    DB.SKUBusiness.Add(NewBusiness);

                    if (Request["properties"] != null)
                    {
                        string[] Properties = Request["properties"].Split(',');
                        if (Properties.Length > 0)
                        {
                            foreach (string prop in Properties)
                            {
                                SKUBusinessProperties BusinessProp = new SKUBusinessProperties();
                                BusinessProp.company_id = NewBusiness.company_id;
                                BusinessProp.business_id = NewBusiness.business_id;
                                BusinessProp.property_id = prop;
                                BusinessProp.created_by = User.Identity.Name;
                                DB.SKUBusinessProperties.Add(BusinessProp);
                            }
                        }
                    }

                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot register new SKU business");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string businessID)
        {
            IEnumerable<SKUBusinesses> Businesses = DB.SKUBusiness.Where(q =>
                q.company_id == companyID && string.Compare(q.business_id, businessID, true) == 0);

            if (Businesses.Count() <= 0)
                return RedirectToAction("Index");

            SKUBusinessRegisterModel EditBusiness = new SKUBusinessRegisterModel();
            EditBusiness.company_id = Businesses.ElementAt(0).company_id;
            EditBusiness.business_id = Businesses.ElementAt(0).business_id;
            EditBusiness.business_name = Businesses.ElementAt(0).business_name;

            ViewBag.Properties = Helper.GetQualityProperties(EditBusiness.company_id);

            return View(EditBusiness);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(SKUBusinessRegisterModel edited)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<SKUBusinesses> Businesses = DB.SKUBusiness.Where(q =>
                    q.company_id == edited.company_id && string.Compare(q.business_id, edited.business_id, true) == 0);
                if (Businesses.Count() > 0)
                {
                    bool IsDirty = false;

                    if (string.Compare(Businesses.ElementAt(0).business_name, edited.business_name) != 0)
                    {
                        Businesses.ElementAt(0).business_name = edited.business_name;
                        IsDirty = true;
                    }

                    var SelectedProperties = new HashSet<string>();
                    if (Request["properties"] != null)
                        SelectedProperties = new HashSet<string>(Request["properties"].Split(','));

                    IEnumerable<SKUBusinessProperties> BusinessProp = DB.SKUBusinessProperties.Where(q =>
                        q.company_id == edited.company_id && string.Compare(q.business_id, edited.business_id, true) == 0);

                    var CurrentProperties = new HashSet<string>(BusinessProp.Select(c => c.property_id));

                    IEnumerable<string> NewProperties = SelectedProperties.Where(p => !CurrentProperties.Contains(p));
                    if (NewProperties != null && NewProperties.Count() > 0)
                    {
                        foreach (string prop in NewProperties)
                        {
                            SKUBusinessProperties NewProp = new SKUBusinessProperties();
                            NewProp.company_id = edited.company_id;
                            NewProp.business_id = edited.business_id;
                            NewProp.property_id = prop;
                            NewProp.created_by = User.Identity.Name;

                            DB.SKUBusinessProperties.Add(NewProp);
                            IsDirty = true;
                        }
                    }

                    IEnumerable<SKUBusinessProperties> DeletedProperties = BusinessProp.Where(c =>
                        !SelectedProperties.Contains(c.property_id));
                    if (DeletedProperties != null && DeletedProperties.Count() > 0)
                    {
                        foreach (SKUBusinessProperties deleted in DeletedProperties)
                        {
                            DB.Entry(deleted).State = System.Data.Entity.EntityState.Deleted;
                            IsDirty = true;
                        }
                    }

                    if (IsDirty)
                    {
                        Businesses.ElementAt(0).revised_by = User.Identity.Name;
                        Businesses.ElementAt(0).revised_date = DateTime.Now;

                        DB.Entry(Businesses.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;

                        DB.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Cannot update business information");
            ViewBag.Properties = Helper.GetQualityProperties(edited.company_id);

            return View(edited);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string businessID, int companyID)
        {
            IEnumerable<SKUBusinesses> Businesses = DB.SKUBusiness.Where(c =>
                c.company_id == companyID && string.Compare(c.business_id, businessID, true) == 0);

            bool IsDirty = false;

            if (Businesses != null && Businesses.Count() > 0)
            {
                DB.Entry(Businesses.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            IEnumerable<SKUBusinessProperties> Properties = DB.SKUBusinessProperties.Where(p =>
                p.company_id == companyID && string.Compare(p.business_id, businessID, true) == 0);
            if (Properties != null && Properties.Count() > 0)
            {
                foreach (SKUBusinessProperties prop in Properties)
                    DB.Entry(prop).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            if (IsDirty)
                DB.SaveChanges();

            return RedirectToAction("Index");
        }
	}
}