﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using Newtonsoft.Json;

namespace DaVinxi.Controllers
{
    public class HelperClassController : Controller
    {

        private  MyDBContext _DB;
        public  MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        public HelperClassController() { }

        public HelperClassController(MyDBContext context)
        {
            _DB = context;
        }

        #region Get Companies
        [Authorize]
        public  List<MasterCompanies> GetCompanies(int companyID = 0)
        {
            List<MasterCompanies> Companies = DB.Companies.ToList();
            if (companyID > 0)
            {
                Companies = Companies.Where(c =>
                    c.company_id == companyID).ToList();
            }

            if (Companies.Count() > 0)
                Companies.Sort((company1, company2) =>
                    company1.company_name.ToLower().CompareTo(company2.company_name.ToLower()));

            return Companies;
        }

        [AllowCrossSiteJson]
        public ActionResult GetCompaniesJSON(int company_id)
        {
            return new JsonResult()
            {
                Data = GetCompanies(company_id),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion

        #region Get Users
        [Authorize]
        public  List<MasterUser> GetUsers(int companyID = 0)
        {
            List<MasterUser> Users = DB.Users.ToList();
            if (companyID > 0)
            {
                Users = Users.Where(u =>
                    u.company_id == companyID).ToList();
            }

            if (Users.Count() > 0)
                Users.Sort((user1, user2) => 
                    user1.full_name.ToLower().CompareTo(user2.full_name.ToLower()));

            return Users;
        }
        #endregion

        #region Get Divisions
        [Authorize]
        public  List<MasterDivisions> GetDivisions(int companyID = 0)
        {
            List<MasterDivisions> Divisions = DB.Divisions.ToList();
            if (companyID > 0)
            {
                Divisions = Divisions.Where(u =>
                    u.company_id == companyID).ToList();
            }

            return Divisions.OrderBy(d => d.division_name).ToList();
        }

        [AllowCrossSiteJson]
        public ActionResult GetDivisionsJSON(int companyID)
        {
            return new JsonResult()
            {
                Data = GetDivisions(companyID),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion

        #region Block
        [Authorize]
        public  List<MasterBlocks> GetBlocks(int companyID)
        {
            List<MasterBlocks> Blocks = DB.Blocks.ToList();
            if (companyID > 0)
                Blocks = Blocks.Where(b => b.company_id == companyID).ToList();
            return Blocks.OrderBy(b => b.block_name.ToLower()).ToList();
        }

        [AllowCrossSiteJson]
        public ActionResult GetBlocksJSON(int companyID, string divisionID)
        {
            var Blocks = from block in DB.Blocks
                         join division in DB.Divisions
                         on new { c1 = block.company_id, c2 = block.division_id } equals new { c1 = division.company_id, c2 = division.division_id }
                         select new { 
                             company_id = block.company_id, block_id = block.block_id, 
                             block_name = block.block_name, division_id = division.division_id, 
                             division_name = division.division_name 
                         };

            if (companyID > 0)
                Blocks = Blocks.Where(b => b.company_id == companyID);

            if (divisionID != null && divisionID.Length > 0)
                Blocks = Blocks.Where(b => string.Compare(b.division_id, divisionID, true) == 0);

            return new JsonResult()
            {
                Data = Blocks.OrderBy(b => b.division_name.ToLower()).ThenBy(b => b.block_name.ToLower()),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion

        #region Get Engineering Units
        [Authorize]
        public  List<MasterEngineeringUnits> GetEngineeringUnits(int companyID = 0)
        {
            List<MasterEngineeringUnits> EUs = DB.EngineeringUnits.ToList();
            if (companyID > 0)
            {
                EUs = EUs.Where(u =>
                    u.company_id == companyID).ToList();
            }

            if (EUs.Count() > 0)
                EUs.Sort((eu1, eu2) =>
                    eu1.unit_name.ToLower().CompareTo(eu2.unit_name.ToLower()));

            return EUs;
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetEngineeringUnitsJSON(int companyID = 0)
        {
            return Json(GetEngineeringUnits(companyID));
        }
        #endregion

        #region Get Machine Type
        [Authorize]
        public  List<MasterMachineType> GetMachineType(int companyID = 0)
        {
            List<MasterMachineType> MachTypes = DB.MachineTypes.ToList();
            if (companyID > 0)
            {
                MachTypes = MachTypes.Where(u =>
                    u.company_id == companyID).ToList();
            }

            if (MachTypes.Count() > 0)
                MachTypes.Sort((eu1, eu2) =>
                    eu1.machine_type_name.ToLower().CompareTo(eu2.machine_type_name.ToLower()));

            return MachTypes;
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetMachineTypeJSON(int companyID = 0)
        {
            return Json(GetMachineType(companyID));
        }
        #endregion

        #region Get Components
        [Authorize]
        public  List<MasterMachineComponents> GetComponents(int companyID = 0)
        {
            List<MasterMachineComponents> Components = DB.Components.ToList();
            if (companyID > 0)
            {
                Components = Components.Where(u =>
                    u.company_id == companyID).ToList();
            }

            if (Components.Count() > 0)
                Components.Sort((eu1, eu2) =>
                    eu1.component_name.ToLower().CompareTo(eu2.component_name.ToLower()));

            return Components;
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetComponentsJSON(int companyID = 0)
        {
            return Json(GetComponents(companyID));
        }

        [Authorize]
        public  List<MasterMachineComponents> GetMachineComponentByMachineType(string machineTypeID, 
            int companyID = 0)
        {
            var Components = from machComponent in DB.MachineTypeComponents
                             join component in DB.Components on
                             new { c1 = machComponent.company_id, c2 = machComponent.component_id }
                                equals new { c1 = component.company_id, c2 = component.component_id }
                             where machComponent.company_id == companyID &&
                             machComponent.machine_type_id == machineTypeID
                             orderby component.component_name
                             select component;
            return Components.ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetMachineComponentByMachineTypeJSON(string machTypeID, int companyID = 0)
        {
            var Result = from machComponent in DB.MachineTypeComponents
                         join component in DB.Components on
                         machComponent.component_id equals component.component_id
                         join uom in DB.EngineeringUnits on new { c1 = component.company_id, c2 = component.unit_id }
                            equals new { c1 = uom.company_id, c2 = uom.unit_id }
                         where machComponent.company_id == companyID && 
                         machComponent.machine_type_id == machTypeID
                         orderby component.component_name
                         select new { component = component, uom = uom.unit_name };
            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = Result
            };
        }

        [Authorize]
        public  List<MachineTypeComponents> GetMachineTypeComponent(string machineTypeID,
            int companyID = 0)
        {
            return DB.MachineTypeComponents.Where(m => 
                m.company_id == companyID && 
                string.Compare(m.machine_type_id, machineTypeID, true) == 0).ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetMachineTypeComponentJSON(string machTypeID, int companyID = 0)
        {
            return Json(GetMachineTypeComponent(machTypeID, companyID));
        }
        #endregion

        #region Get Machines
        [Authorize]
        public  List<MasterMachine> GetMachines(int companyID = 0)
        {
            List<MasterMachine> Machines = DB.Machines.ToList();
            if (companyID > 0)
            {
                Machines = Machines.Where(u =>
                    u.company_id == companyID).ToList();
            }

            return Machines.OrderBy(m => m.machine_name.ToLower()).ToList();
        }

        [AllowCrossSiteJson]
        public ActionResult GetMachinesJSON(int companyID, string divisionID=null, string blockID=null)
        {
            var Machines = from machine in DB.Machines
                           join block in DB.Blocks 
                            on new { c1 = machine.company_id, c2 = machine.block_id } 
                            equals new { c1 = block.company_id, c2 = block.block_id }
                           join division in DB.Divisions 
                            on new { c1 = block.company_id, c2 = block.division_id } 
                            equals new { c1 = division.company_id, c2 = division.division_id }
                           select new { machine = machine, divisionID = division.division_id };

            if (divisionID != null && divisionID.Length > 0)
                Machines = Machines.Where(m => string.Compare(m.divisionID, divisionID, true) == 0);

            if (blockID != null && blockID.Length > 0)
                Machines = Machines.Where(m => string.Compare(m.machine.block_id, blockID, true) == 0);

            return new JsonResult()
            {
                Data = Machines.OrderBy(m => m.machine.machine_name.ToLower()),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        public ActionResult GetMachineDetailsJSON(string machineID, int company)
        {
            var MachineDetails = from machine in DB.Machines
                                 where machine.company_id == company &&
                                    string.Compare(machine.machine_id, machineID, true) == 0
                                 join machineType in DB.MachineTypeComponents on
                                     new { machine.machine_type_id, machine.company_id } equals
                                     new { machineType.machine_type_id, machineType.company_id }
                                 join component in DB.Components on
                                     new { machineType.component_id, machineType.company_id } equals
                                     new { component.component_id, component.company_id }
                                 join unit in DB.EngineeringUnits on
                                     new { component.unit_id, component.company_id } equals
                                     new { unit.unit_id, unit.company_id }
                                 join componentValue in DB.MachineComponents on
                                     new { machine.machine_id, component.component_id, component.company_id } equals
                                     new { componentValue.machine_id, componentValue.component_id, componentValue.company_id }
                                    into result
                                 from r in result.DefaultIfEmpty()
                                 join tag in DB.HistorianTags on new { c1 = r.company_id, c2 = r.tag_id } equals
                                    new { c1 = tag.company_id, c2 = tag.tag_id }
                                    into tagResult
                                 from t in tagResult.DefaultIfEmpty()
                                 select new
                                 {
                                     machine_id = machine.machine_id,
                                     company_id = machine.company_id,
                                     component_id = component.component_id,
                                     unit_id = unit.unit_id,
                                     unit_name = unit.unit_name,
                                     is_numeric = component.is_numeric,
                                     component_name = component.component_name,
                                     value = r == null ? (component.is_numeric ? "0" : "") : r.value,
                                     tag_id = r == null ? "" : r.tag_id,
                                     tag_name = t == null ? "" : t.tag_name
                                 };
            return Json(MachineDetails);
        }

        #endregion

        #region Quality
        #region Quality Properties
        [Authorize]
        public  List<QualityProperties> GetQualityProperties(int companyID)
        {
            List<QualityProperties> QCProc = DB.QualityProperties.ToList();
            if (companyID > 0 && QCProc.Count() > 0)
                QCProc = QCProc.Where(q => q.company_id == companyID).ToList();

            if (QCProc.Count > 0)
                QCProc.Sort((q1, q2) => q1.property_name.ToLower().CompareTo(q2.property_name.ToLower()));

            return QCProc;
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetQualityPropertiesJSON(int companyID)
        {
            return Json(GetQualityProperties(companyID));
        }
        #endregion

        #region SKU Categories
        [Authorize]
        public  List<SKUCategories> GetSKUCategories(int companyID)
        {
            List<SKUCategories> Categories = DB.SKUCategories.ToList();
            if (companyID > 0 && Categories.Count() > 0)
                Categories = Categories.Where(q => q.company_id == companyID).ToList();

            if (Categories.Count > 0)
                Categories.Sort((c1, c2) => c1.category_name.ToLower().CompareTo(c2.category_name.ToLower()));

            return Categories;
        }

        [AllowCrossSiteJson]
        public ActionResult GetSKUCategoriesJSON(int companyID)
        {
            return new JsonResult()
            {
                Data = GetSKUCategories(companyID),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        public  List<QualityProperties> GetQualityPropertiesBySKUCategory(int companyID, string categoryID)
        {
            var Prop = from skuCat in DB.SKUCategoryProperties
                       join property in DB.QualityProperties
                       on new { skuCat.company_id, skuCat.property_id } equals new { property.company_id, property.property_id }
                       where skuCat.company_id == companyID && string.Compare(skuCat.category_id, categoryID, true) == 0
                       orderby property.property_name
                       select property;
            return Prop.ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetQualityPropertiesBySKUCategoryJSON(int companyID, string categoryID)
        {
            return Json(GetQualityPropertiesBySKUCategory(companyID, categoryID));
        }
        #endregion

        #region SKU Subcategories
        [Authorize]
        public  List<SKUSubcategories> GetSKUSubCategories(int companyID)
        {
            List<SKUSubcategories> SubCategories = DB.SKUSubcategories.ToList();
            if (companyID > 0 && SubCategories.Count() > 0)
                SubCategories = SubCategories.Where(q => q.company_id == companyID).ToList();

            if (SubCategories.Count > 0)
                SubCategories.Sort((c1, c2) => c1.subcategory_name.ToLower().CompareTo(c2.subcategory_name.ToLower()));

            return SubCategories;
        }

        [AllowCrossSiteJson]
        public ActionResult GetSKUSubCategoriesJSON(int companyID, string categoryID = null)
        {
            List<SKUSubcategories> Subcategories = GetSKUSubCategories(companyID);
            if (categoryID != null && categoryID.Length > 0)
                Subcategories = Subcategories.Where(s => string.Compare(s.category_id, categoryID, true) == 0).ToList();

            return new JsonResult()
            {
                Data = Subcategories,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        public  List<QualityProperties> GetQualityPropertiesBySKUSubCategory(int companyID, string subcategoryID)
        {
            var Prop = from skuCat in DB.SKUSubcategoryProperties
                       join property in DB.QualityProperties
                       on new { skuCat.company_id, skuCat.property_id } equals new { property.company_id, property.property_id }
                       where skuCat.company_id == companyID && string.Compare(skuCat.subcategory_id, subcategoryID, true) == 0
                       orderby property.property_name
                       select property;
            return Prop.ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetQualityPropertiesBySKUSubCategoryJSON(int companyID, string subcategoryID)
        {
            return Json(GetQualityPropertiesBySKUSubCategory(companyID, subcategoryID));
        }
        #endregion

        #region SKU Segments
        [Authorize]
        public  List<SKUSegments> GetSKUSegments(int companyID)
        {
            List<SKUSegments> Segments = DB.SKUSegments.ToList();
            if (companyID > 0 && Segments.Count() > 0)
                Segments = Segments.Where(q => q.company_id == companyID).ToList();

            if (Segments.Count > 0)
                Segments.Sort((c1, c2) => c1.segment_name.ToLower().CompareTo(c2.segment_name.ToLower()));

            return Segments;
        }

        [AllowCrossSiteJson]
        public ActionResult GetSKUSegmentsJSON(int companyID)
        {
            return new JsonResult()
            {
                Data = GetSKUSegments(companyID),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        public  List<QualityProperties> GetQualityPropertiesBySKUSegment(int companyID, string segmentID)
        {
            var Prop = from skuCat in DB.SKUSegmentProperties
                       join property in DB.QualityProperties
                       on new { skuCat.company_id, skuCat.property_id } equals new { property.company_id, property.property_id }
                       where skuCat.company_id == companyID && string.Compare(skuCat.segment_id, segmentID, true) == 0
                       orderby property.property_name
                       select property;
            return Prop.ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetQualityPropertiesBySKUSegmentJSON(int companyID, string segmentID)
        {
            return Json(GetQualityPropertiesBySKUSegment(companyID, segmentID));
        }
        #endregion

        #region SKU Business
        [Authorize]
        public  List<SKUBusinesses> GetSKUBusiness(int companyID)
        {
            List<SKUBusinesses> Business = DB.SKUBusiness.ToList();
            if (companyID > 0 && Business.Count() > 0)
                Business = Business.Where(q => q.company_id == companyID).ToList();

            if (Business.Count > 0)
                Business.Sort((c1, c2) => c1.business_name.ToLower().CompareTo(c2.business_name.ToLower()));

            return Business;
        }

        [AllowCrossSiteJson]
        public ActionResult GetSKUBusinessJSON(int companyID)
        {
            return new JsonResult()
            {
                Data = GetSKUBusiness(companyID),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        public  List<QualityProperties> GetQualityPropertiesBySKUBusiness(int companyID, string businessID)
        {
            var Prop = from skuCat in DB.SKUBusinessProperties
                       join property in DB.QualityProperties
                       on new { skuCat.company_id, skuCat.property_id } equals new { property.company_id, property.property_id }
                       where skuCat.company_id == companyID && string.Compare(skuCat.business_id, businessID, true) == 0
                       orderby property.property_name
                       select property;
            return Prop.ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetQualityPropertiesBySKUBusinessJSON(int companyID, string businessID)
        {
            return Json(GetQualityPropertiesBySKUBusiness(companyID, businessID));
        }
        #endregion

        #region SKU
        [Authorize]
        public  List<MasterSKU> GetSKU(int companyID, 
            string categoryID = null, string subcategoryID = null, 
            string segmentID = null, string businessID = null)
        {
            var SKU = from sku in DB.SKU
                      join subcategory in DB.SKUSubcategories on
                      new { sku.company_id, sku.subcategory_id } equals new { subcategory.company_id, subcategory.subcategory_id }
                      orderby sku.sku_name
                      select new { sku = sku, category_id = subcategory.category_id };

            if (companyID > 0 && SKU.Count() > 0)
                SKU = SKU.Where(q => q.sku.company_id == companyID);

            if (categoryID != null && categoryID.Length > 0 && SKU.Count() > 0)
                SKU = SKU.Where(q => string.Compare(q.category_id, categoryID, true) == 0);

            if (subcategoryID != null && subcategoryID.Length > 0 && SKU.Count() > 0)
                SKU = SKU.Where(q => string.Compare(q.sku.subcategory_id, subcategoryID, true) == 0);

            if (segmentID != null && segmentID.Length > 0 && SKU.Count() > 0)
                SKU = SKU.Where(q => string.Compare(q.sku.segment_id, segmentID, true) == 0);

            if (businessID != null && businessID.Length > 0 && SKU.Count() > 0)
                SKU = SKU.Where(q => string.Compare(q.sku.business_id, businessID, true) == 0);

            return SKU.Select(s => s.sku).ToList();
        }

        [AllowCrossSiteJson]
        public ActionResult GetSKUJSON(int companyID,
            string categoryID = null, string subcategoryID = null,
            string segmentID = null, string businessID = null, string skuID=null)
        {
            List<MasterSKU> SKUs = GetSKU(companyID, categoryID, subcategoryID, segmentID, businessID);
            if (skuID != null && skuID.Trim().Length > 0) {
                SKUs = SKUs.Where(s => s.sku_id.ToLower().Contains(skuID.ToLower()) || 
                    s.sku_name.ToLower().Contains(skuID.ToLower())).ToList();
            }
            return new JsonResult()
            {
                Data = SKUs,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public  List<QualityProperties> GetQualityPropertiesBySKU(int companyID, string skuID)
        {
            var Prop = from skuCat in DB.SKUProperties
                       join property in DB.QualityProperties
                       on new { skuCat.company_id, skuCat.property_id } equals new { property.company_id, property.property_id }
                       where skuCat.company_id == companyID && string.Compare(skuCat.sku_id, skuID, true) == 0
                       orderby property.property_name
                       select property;
            return Prop.ToList();
        }

        [AllowCrossSiteJson]
        public ActionResult GetQualityPropertiesBySKUJSON(int companyID, string skuID)
        {
            return Json(GetQualityPropertiesBySKU(companyID, skuID), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Property Setting
        [AllowCrossSiteJson]
        public ActionResult GetPropertySettingJSON(int companyID, string skuID)
        {
            MasterSKU SKU = DB.SKU.Where(s => s.company_id == companyID && 
                string.Compare(skuID, s.sku_id, true) == 0).FirstOrDefault();
            if (SKU == null)
                return null;

            SKUSubcategories SubCategory = DB.SKUSubcategories.Where(s => s.company_id == companyID && 
                string.Compare(s.subcategory_id, SKU.subcategory_id, true) == 0).FirstOrDefault();
            if (SubCategory == null)
                return null;

            var Properties = (from skuProp in DB.SKUProperties
                              where skuProp.company_id == companyID && string.Compare(skuProp.sku_id, skuID, true) == 0
                              select new { property_id = skuProp.property_id, company_id = companyID, sku_id = skuID }
                             ).Union
                             (
                                from segmentProp in DB.SKUSegmentProperties
                                where segmentProp.company_id == companyID &&
                                string.Compare(segmentProp.segment_id, SKU.segment_id, true) == 0
                                select new { property_id = segmentProp.property_id, company_id = companyID, sku_id = skuID }
                             ).Union
                             (
                               from businessProp in DB.SKUBusinessProperties
                               where businessProp.company_id == companyID &&
                               string.Compare(businessProp.business_id, SKU.business_id, true) == 0
                               select new { property_id = businessProp.property_id, company_id = companyID, sku_id = skuID }
                             ).Union
                             (
                                from subProp in DB.SKUSubcategoryProperties
                                where subProp.company_id == companyID &&
                                string.Compare(subProp.subcategory_id, SubCategory.subcategory_id, true) == 0
                                select new { property_id = subProp.property_id, company_id = companyID, sku_id = skuID }
                             ).Union
                             (
                                from catProp in DB.SKUCategoryProperties
                                where catProp.company_id == companyID &&
                                string.Compare(catProp.category_id, SubCategory.category_id, true) == 0
                                select new { property_id = catProp.property_id, company_id = companyID, sku_id = skuID }
                             );
            if (Properties == null || Properties.Count() <= 0)
                return null;

            var PropertySetting = from prop in Properties
                                  join masterProp in DB.QualityProperties on
                                    new { prop.property_id, prop.company_id } equals
                                    new { masterProp.property_id, masterProp.company_id }
                                  join propSetting in DB.SKUPropertySettings on
                                    new { masterProp.company_id, masterProp.property_id, prop.sku_id } equals
                                    new { propSetting.company_id, propSetting.property_id, propSetting.sku_id }
                                    into result
                                  from r in result.DefaultIfEmpty()
                                  select new
                                  {
                                      url = r == null ? 0 : r.url,
                                      lrl = r == null ? 0 : r.lrl,
                                      sigma = r == null ? 0 : r.sigma,
                                      sigma_multiplier = r == null ? 0 : r.sigma_multiplier,
                                      target = r == null ? 0 : r.target,
                                      ucl = r == null ? 0 : r.ucl,
                                      lcl = r == null ? 0 : r.lcl,
                                      upper_map = r == null ? 0 : r.upper_map,
                                      lower_map = r == null ? 0 : r.lower_map,
                                      weight = r == null ? 1 : r.weight,
                                      cp = r == null ? 0 : r.cp,
                                      cpk = r == null ? 0 : r.cpk,
                                      property_name = masterProp.property_name,
                                      property_id = masterProp.property_id
                                  };
            return new JsonResult()
            {
                Data = PropertySetting,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        [HttpPost]
        public string SaveSingleProperty(int companyID, string skuID, string propertyID, string attrib, double newValue)
        {
            try
            {
                IEnumerable<SKUPropertiesSetting> PropSetting = DB.SKUPropertySettings.Where(s => 
                    s.company_id == companyID && string.Compare(s.sku_id, skuID, true) == 0 && 
                    string.Compare(s.property_id, propertyID, true) == 0);

                SKUPropertiesSetting Setting = null;
                bool IsNew = false;
                if (PropSetting == null || PropSetting.Count() <= 0)
                {
                    Setting = new SKUPropertiesSetting();
                    Setting.company_id = companyID;
                    Setting.sku_id = skuID;
                    Setting.property_id = propertyID;
                    Setting.revised_by = User.Identity.Name;
                    IsNew = true;
                }
                else
                    Setting = PropSetting.ElementAt(0);

                bool IsDirty = false;
                if (string.Compare(attrib, "lrl", true) == 0 && Setting.lrl != newValue)
                {
                    Setting.lrl = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "url", true) == 0 && Setting.url != newValue)
                {
                    Setting.url = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "sigma", true) == 0 && Setting.sigma != newValue)
                {
                    Setting.sigma = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "sigmax", true) == 0 && Setting.sigma_multiplier != newValue)
                {
                    Setting.sigma_multiplier = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "lcl", true) == 0 && Setting.lcl != newValue)
                {
                    Setting.lcl = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "ucl", true) == 0 && Setting.ucl != newValue)
                {
                    Setting.ucl = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "target", true) == 0 && Setting.target != newValue)
                {
                    Setting.target = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "ll", true) == 0 && Setting.lower_map != newValue)
                {
                    Setting.lower_map = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "rl", true) == 0 && Setting.upper_map != newValue)
                {
                    Setting.upper_map = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "weight", true) == 0 && Setting.weight != newValue)
                {
                    Setting.weight = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "cp", true) == 0 && Setting.cp != newValue)
                {
                    Setting.cp = newValue;
                    IsDirty = true;
                }
                else if (string.Compare(attrib, "cpk", true) == 0 && Setting.cpk != newValue)
                {
                    Setting.cpk = newValue;
                    IsDirty = true;
                }

                if(IsDirty)
                {
                    if (IsNew)
                        DB.SKUPropertySettings.Add(Setting);
                    else
                    {
                        Setting.revised_date = DateTime.Now;
                        Setting.revised_by = User.Identity.Name;
                        DB.Entry(Setting).State = System.Data.Entity.EntityState.Modified;
                    }
                    DB.SaveChanges();
                }
            }
            catch (Exception)
            { return "0x001"; }
            return "0x000";
        }

        [Authorize]
        [HttpPost]
        public string SavePropertySetting(int companyID, string skuID, string propertyID, double lrl, 
            double url, double sigma, double sigmax, double lcl, double ucl, double target, 
            double ll, double rl, double weight)
        {
            try
            {
                IEnumerable<SKUPropertiesSetting> PropSetting = DB.SKUPropertySettings.Where(s =>
                    s.company_id == companyID && string.Compare(s.sku_id, skuID, true) == 0 &&
                    string.Compare(s.property_id, propertyID, true) == 0);

                SKUPropertiesSetting Setting = null;
                bool IsNew = false;
                if (PropSetting == null || PropSetting.Count() <= 0)
                {
                    Setting = new SKUPropertiesSetting();
                    Setting.company_id = companyID;
                    Setting.sku_id = skuID;
                    Setting.property_id = propertyID;
                    Setting.revised_by = User.Identity.Name;
                    IsNew = true;
                }
                else
                    Setting = PropSetting.ElementAt(0);

                bool IsDirty = false;
                if (Setting.lrl != lrl)
                {
                    Setting.lrl = lrl;
                    IsDirty = true;
                }

                if (Setting.url != url)
                {
                    Setting.url = url;
                    IsDirty = true;
                }

                if (Setting.sigma != sigma)
                {
                    Setting.sigma = sigma;
                    IsDirty = true;
                }

                if (Setting.sigma_multiplier != sigmax)
                {
                    Setting.sigma_multiplier = sigmax;
                    IsDirty = true;
                }

                if (Setting.lcl != lcl)
                {
                    Setting.lcl = lcl;
                    IsDirty = true;
                }

                if (Setting.ucl != ucl)
                {
                    Setting.ucl = ucl;
                    IsDirty = true;
                }

                if (Setting.target != target)
                {
                    Setting.target = target;
                    IsDirty = true;
                }

                if (Setting.lower_map != ll)
                {
                    Setting.lower_map = ll;
                    IsDirty = true;
                }

                if (Setting.upper_map != rl)
                {
                    Setting.upper_map = rl;
                    IsDirty = true;
                }

                if (Setting.weight != weight)
                {
                    Setting.weight = weight;
                    IsDirty = true;
                }

                if (IsDirty)
                {
                    if (IsNew)
                        DB.SKUPropertySettings.Add(Setting);
                    else
                    {
                        Setting.revised_date = DateTime.Now;
                        Setting.revised_by = User.Identity.Name;
                        DB.Entry(Setting).State = System.Data.Entity.EntityState.Modified;
                    }
                    DB.SaveChanges();
                }
            }
            catch(Exception)
            { return "0x0001"; }
            return "0x000";
        }
        #endregion
        #endregion

        #region Defect
        [Authorize]
        public  List<MasterDefect> GetDefects(int companyID)
        {
            IEnumerable<MasterDefect> Defects = DB.Defects;
            if (companyID > 0)
                Defects = Defects.Where(d => d.company_id == companyID);
            return Defects.OrderBy(d => d.defect_name.ToLower()).ToList();
        }

        [Authorize]
        public  List<MasterDefect> GetDefects(int companyID, string parentID)
        {
            IEnumerable<MasterDefect> Defects = DB.Defects.Where(d => d.company_id == companyID &&
                string.Compare(d.parent_defect_id, parentID, true) == 0).OrderBy(d => d.defect_name.ToLower());

            return Defects.ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetDefectsJSON(int companyID, string parentID)
        {
            var Defects = from defectA in DB.Defects
                          where string.Compare(defectA.parent_defect_id, parentID, true) == 0
                          join defectB in DB.Defects on
                          new { p1 = defectA.company_id, p2 = defectA.defect_id } equals
                          new { p1 = defectB.company_id, p2 = defectB.parent_defect_id }
                          into result
                          from rs in result.DefaultIfEmpty()
                          select new { defectID = defectA.defect_id, defect = defectA, 
                              count = (rs == null ? 0 : result.Count()) };
            return Json(Defects.GroupBy(d => d.defectID).Select(g => g.FirstOrDefault()).OrderBy(d => d.defect.defect_name));
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetDefectInfoJSON(int companyID, string defectID)
        {
            return Json(DB.Defects.Where(d => d.company_id == companyID && 
                string.Compare(d.defect_id, defectID, true) == 0));
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetDefectPhotosJSON(int companyID, string defectID)
        {
            return Json(DB.DefectPhotos.Where(d => d.company_id == companyID &&
                string.Compare(d.defect_id, defectID, true) == 0));
        }

        [Authorize, HttpPost]
        public ActionResult GetLowestDefect(int companyID) {
            var Defects = from sourceDefect in DB.Defects
                          where sourceDefect.company_id == companyID
                          join destDefects in DB.Defects on
                            new { c1 = sourceDefect.company_id, c2 = sourceDefect.defect_id } equals
                            new { c1 = destDefects.company_id, c2 = destDefects.parent_defect_id }
                              into result
                          from r in result.DefaultIfEmpty()
                          select new
                          {
                              defect = sourceDefect,
                              has_child = (r == null ? false : true)
                          };

            Defects = Defects.Where(d => !d.has_child).GroupBy(
                d => d.defect.defect_id).Select(d => d.FirstOrDefault()).OrderBy(d => d.defect.defect_name.ToLower());

            return new JsonResult() {
                Data = Defects.Select(d => new { defect_id = d.defect.defect_id, defect_name = d.defect.defect_name }),
                MaxJsonLength=Int32.MaxValue
            };
        }
        #endregion

        #region Production
        #region Batch
        [Authorize]
        public  List<MasterBatch> GetBatches(int companyID)
        {
            IEnumerable<MasterBatch> Batches = DB.Batches;
            if (companyID > 0)
                Batches = Batches.Where(b => b.company_id == companyID);
            return Batches.OrderByDescending(b => b.start_date).ThenBy(b => b.batch_id.ToLower()).ToList();
        }

        [AllowCrossSiteJson]
        public ActionResult GetBatchesJSON(int companyID,string skuID)
        {
            List<MasterBatch> Batches = GetBatches(companyID);

            if (skuID != null && skuID.Length > 0)
                Batches = Batches.Where(b => string.Compare(b.sku_id, skuID, true) == 0).ToList();
            return new JsonResult()
            {
                Data = Batches.OrderBy(b => b.sku_id.ToLower()).ThenByDescending(b => b.start_date).Join
                    (
                    DB.SKU, b => new { c1 = b.company_id, c2 = b.sku_id },
                    s => new { c1 = s.company_id, c2 = s.sku_id },
                    (b, s) => new
                    {
                        batch_id = b.batch_id,
                        sku_id = b.sku_id,
                        sku_name = s.sku_name,
                        start_date = b.start_date,
                        finish_date = b.finish_date
                    }
                    ).ToList().Select
                    (
                    b => new
                    {
                        batch_id = b.batch_id,
                        sku_id = b.sku_id,
                        sku_name = b.sku_name,
                        start_date = b.start_date.ToString("MMMM dd yyyy HH:mm"),
                        finish_date = b.finish_date.ToString("MMMM dd yyyy HH:mm")
                    }
                    ),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        [HttpPost]
        public ActionResult FindBatch(int company_id, string category_id, string subcategory_id, 
            string segment_id, string business_id, string sku_id, string batch_id, 
            string division_id, string block_id, string machine_id)
        {
            var Result = from batch in DB.Batches
                         join machine in DB.Machines on new { c1 = batch.company_id, c2 = batch.machine_id }
                            equals new { c1 = machine.company_id, c2 = machine.machine_id }
                         join block in DB.Blocks on new { c1 = machine.company_id, c2 = machine.block_id }
                            equals new { c1 = block.company_id, c2 = block.block_id }
                         join division in DB.Divisions on new { c1 = block.company_id, c2 = block.division_id }
                            equals new { c1 = division.company_id, c2 = division.division_id }
                         join sku in DB.SKU
                         on new { c1 = batch.company_id, c2 = batch.sku_id } equals new { c1 = sku.company_id, c2 = sku.sku_id }
                         join business in DB.SKUBusiness
                         on new { c1 = sku.company_id, c2 = sku.business_id } equals
                            new { c1 = business.company_id, c2 = business.business_id }
                         join segment in DB.SKUSegments
                         on new { c1 = sku.company_id, c2 = sku.segment_id } equals
                            new { c1 = segment.company_id, c2 = segment.segment_id }
                         join subcat in DB.SKUSubcategories
                         on new { c1 = sku.company_id, c2 = sku.subcategory_id } equals
                            new { c1 = subcat.company_id, c2 = subcat.subcategory_id }
                         join cat in DB.SKUCategories
                         on new { c1 = subcat.company_id, c2 = subcat.category_id } equals
                            new { c1 = cat.company_id, c2 = cat.category_id }
                         join company in DB.Companies
                         on batch.company_id equals company.company_id
                         select new
                         {
                             batch,
                             division_id = division.division_id,
                             block_id = block.block_id,
                             machine = machine,
                             category_id = cat.category_id,
                             subcategory_id = subcat.subcategory_id,
                             segment_id = segment.segment_id,
                             business_id = business.business_id,
                             sku_name = sku.sku_name,
                             company_name = company.company_name,
                             start_date = batch.start_date,
                             finish_date = batch.finish_date
                         };

            if (Result.Count() > 0 && company_id > 0)
                Result = Result.Where(r => r.batch.company_id == company_id);

            if (Result.Count() > 0 && category_id != null && category_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.category_id, category_id, true) == 0);

            if (Result.Count() > 0 && subcategory_id != null && subcategory_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.subcategory_id, subcategory_id, true) == 0);

            if (Result.Count() > 0 && segment_id != null && segment_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.segment_id, segment_id, true) == 0);

            if (Result.Count() > 0 && business_id != null && business_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.business_id, business_id, true) == 0);

            if (Result.Count() > 0 && sku_id != null && sku_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.batch.sku_id, sku_id, true) == 0);

            if (Result.Count() > 0 && batch_id != null && batch_id.Trim().Length > 0)
                Result = Result.Where(r => r.batch.batch_id.ToLower().Contains(batch_id.Trim().ToLower()));

            if (Result.Count() > 0 && division_id != null && division_id.Trim().Length > 0)
                Result = Result.Where(r => string.Compare(r.division_id, division_id.Trim(), true) == 0);

            if (Result.Count() > 0 && block_id != null && block_id.Trim().Length > 0)
                Result = Result.Where(r => string.Compare(r.block_id, block_id.Trim(), true) == 0);

            if (Result.Count() > 0 && machine_id != null && machine_id.Trim().Length > 0)
                Result = Result.Where(r => string.Compare(r.machine.machine_id, machine_id.Trim(), true) == 0);

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = Result.OrderBy(r=>r.sku_name).OrderByDescending(r => r.batch.start_date).ToList().Select(r => new
                {
                    batch_id = r.batch.batch_id,
                    sku_id = r.batch.sku_id,
                    sku_name = r.sku_name,
                    start_date = r.batch.start_date.ToString("MMM dd yyyy HH:mm"),
                    finish_date = r.batch.finish_date.ToString("MMM dd yyyy HH:mm"),
                    company_id = r.batch.company_id,
                    machine_name = r.machine.machine_name
                })
            };
        }
        #endregion

        #region Product
        [Authorize]
        public List<MasterProduct> GetProduct(int company_id, string product_id)
        {
            IEnumerable<MasterProduct> Products = DB.Products.Where(p => p.product_id == product_id);

            if (product_id != null && product_id.Length > 0)
                Products = Products.Where(p => string.Compare(p.product_id, product_id, true) == 0);

            return Products.ToList();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetProductJSON(int company_id, string product_id)
        {
            return Json(GetProduct(company_id, product_id));
        }

        [AllowCrossSiteJson]
        public ActionResult FindProduct(int company_id, string division_id, string block_id,
            string machine_id, string category_id, string subcategory_id,
            string segment_id, string business_id, string sku_id, string batch_id, string product_id,
            int maxReturnValue = 100)
        {
            var Result = from product in DB.Products
                         join batch in DB.Batches
                         on new { c1 = product.company_id, c2 = product.batch_id } equals
                            new { c1 = batch.company_id, c2 = batch.batch_id }
                         join machine in DB.Machines
                         on new { c1 = batch.company_id, c2 = batch.machine_id } equals
                            new { c1 = machine.company_id, c2 = machine.machine_id }
                         join block in DB.Blocks
                         on new { c1 = machine.company_id, c2 = machine.block_id } equals
                            new { c1 = block.company_id, c2 = block.block_id }
                         join division in DB.Divisions
                         on new { c1 = block.company_id, c2 = block.division_id } equals
                            new { c1 = division.company_id, c2 = division.division_id }
                         join sku in DB.SKU
                         on new { c1 = batch.company_id, c2 = batch.sku_id } equals
                            new { c1 = sku.company_id, c2 = sku.sku_id }
                         join business in DB.SKUBusiness
                         on new { c1 = sku.company_id, c2 = sku.business_id } equals
                            new { c1 = business.company_id, c2 = business.business_id }
                         join segment in DB.SKUSegments
                         on new { c1 = sku.company_id, c2 = sku.segment_id } equals
                            new { c1 = segment.company_id, c2 = segment.segment_id }
                         join subcat in DB.SKUSubcategories
                         on new { c1 = sku.company_id, c2 = sku.subcategory_id } equals
                            new { c1 = subcat.company_id, c2 = subcat.subcategory_id }
                         join cat in DB.SKUCategories
                         on new { c1 = subcat.company_id, c2 = subcat.category_id } equals
                            new { c1 = cat.company_id, c2 = cat.category_id }
                         select new
                         {
                             company_id = product.company_id,
                             product_id = product.product_id,
                             category_id = cat.category_id,
                             category_name = cat.category_name,
                             subcategory_id = subcat.subcategory_id,
                             subcategory_name = subcat.subcategory_name,
                             segment_id = segment.segment_id,
                             segment_name = segment.segment_name,
                             business_id = business.business_id,
                             business_name = business.business_name,
                             sku_id = sku.sku_id,
                             sku_name = sku.sku_name,
                             batch_id = batch.batch_id,
                             division_id = division.division_id,
                             division_name = division.division_name,
                             block_id = block.block_id,
                             block_name = block.block_name,
                             machine_id = machine.machine_id,
                             machine_name = machine.machine_name,
                             start_date = product.start_date,
                             finish_date = product.finish_date,
                             qi = product.qi
                         };

            if (Result.Count() > 0 && company_id > 0)
                Result = Result.Where(r => r.company_id == company_id);

            if (Result.Count() > 0 && division_id != null && division_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.division_id, division_id, true) == 0);

            if (Result.Count() > 0 && block_id != null && block_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.block_id, block_id, true) == 0);

            if (Result.Count() > 0 && machine_id != null && machine_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.machine_id, machine_id, true) == 0);

            if (Result.Count() > 0 && category_id != null && category_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.category_id, category_id, true) == 0);

            if (Result.Count() > 0 && subcategory_id != null && subcategory_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.subcategory_id, subcategory_id, true) == 0);

            if (Result.Count() > 0 && segment_id != null && segment_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.segment_id, segment_id, true) == 0);

            if (Result.Count() > 0 && business_id != null && business_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.business_id, business_id, true) == 0);

            if (Result.Count() > 0 && sku_id != null && sku_id.Length > 0)
                Result = Result.Where(r => string.Compare(r.sku_id, sku_id, true) == 0);

            if (Result.Count() > 0 && batch_id != null && batch_id.Trim().Length > 0)
                Result = Result.Where(r => string.Compare(r.batch_id, batch_id.Trim(), true) == 0);

            if (Result.Count() > 0 && product_id != null && product_id.Trim().Length > 0)
                Result = Result.Where(r => r.product_id.ToLower().Contains(product_id.ToLower().Trim()));

            Result = Result.OrderByDescending(r => r.finish_date);

            if (maxReturnValue > 0)
                Result = Result.Take(maxReturnValue);

            return new JsonResult()
            {
                Data = Result.ToList().Select(r => new
                {
                    company_id = r.company_id,
                    product_id = r.product_id,
                    category_id = r.category_id,
                    category_name = r.category_name,
                    subcategory_id = r.subcategory_id,
                    subcategory_name = r.subcategory_name,
                    segment_id = r.segment_id,
                    segment_name = r.segment_name,
                    business_id = r.business_id,
                    business_name = r.business_name,
                    sku_id = r.sku_id,
                    sku_name = r.sku_name,
                    batch_id = r.batch_id,
                    division_id = r.division_id,
                    division_name = r.division_name,
                    block_id = r.block_id,
                    block_name = r.block_name,
                    machine_id = r.machine_id,
                    machine_name = r.machine_name,
                    start_date = r.start_date.ToString("MMM dd yyyy HH:mm"),
                    finish_date = r.finish_date.ToString("MMM dd yyyy HH:mm"),
                    qi = r.qi
                }),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AllowCrossSiteJson]
        public ActionResult GetQualityData(int company_id,string product_id)
        {
            MasterProduct Product = DB.Products.Where(p => p.company_id == company_id && 
                string.Compare(p.product_id, product_id, true) == 0).FirstOrDefault();

            if (Product == null)
                return null;

            MasterBatch Batch = DB.Batches.Where(b => b.company_id == company_id && 
                string.Compare(b.batch_id, Product.batch_id, true) == 0).FirstOrDefault();

            if (Batch == null)
                return null;

            MasterSKU SKU = DB.SKU.Where(s => s.company_id == company_id &&
                string.Compare(Batch.sku_id, s.sku_id, true) == 0).FirstOrDefault();
            if (SKU == null)
                return null;

            SKUSubcategories SubCategory = DB.SKUSubcategories.Where(s => s.company_id == company_id &&
                string.Compare(s.subcategory_id, SKU.subcategory_id, true) == 0).FirstOrDefault();
            if (SubCategory == null)
                return null;

            var Properties = (from skuProp in DB.SKUProperties
                              where skuProp.company_id == company_id && string.Compare(skuProp.sku_id, SKU.sku_id, true) == 0
                              select new { property_id = skuProp.property_id, company_id = company_id, sku_id = SKU.sku_id }
                             ).Union
                             (
                                from segmentProp in DB.SKUSegmentProperties
                                where segmentProp.company_id == company_id &&
                                string.Compare(segmentProp.segment_id, SKU.segment_id, true) == 0
                                select new { property_id = segmentProp.property_id, company_id = company_id, sku_id = SKU.sku_id }
                             ).Union
                             (
                               from businessProp in DB.SKUBusinessProperties
                               where businessProp.company_id == company_id &&
                               string.Compare(businessProp.business_id, SKU.business_id, true) == 0
                               select new { property_id = businessProp.property_id, company_id = company_id, sku_id = SKU.sku_id }
                             ).Union
                             (
                                from subProp in DB.SKUSubcategoryProperties
                                where subProp.company_id == company_id &&
                                string.Compare(subProp.subcategory_id, SubCategory.subcategory_id, true) == 0
                                select new { property_id = subProp.property_id, company_id = company_id, sku_id = SKU.sku_id }
                             ).Union
                             (
                                from catProp in DB.SKUCategoryProperties
                                where catProp.company_id == company_id &&
                                string.Compare(catProp.category_id, SubCategory.category_id, true) == 0
                                select new { property_id = catProp.property_id, company_id = company_id, sku_id = SKU.sku_id }
                             );
            if (Properties == null || Properties.Count() <= 0)
                return null;

            var QualityData = from prop in Properties
                              join mProp in DB.QualityProperties on
                                new { c1 = prop.company_id, c2 = prop.property_id } equals
                                new { c1 = mProp.company_id, c2 = mProp.property_id }
                              join unit in DB.EngineeringUnits on
                                new { c1 = mProp.company_id, c2 = mProp.unit_id } equals
                                new { c1 = unit.company_id, c2 = unit.unit_id }
                              join masterProp in DB.QualityProperties on
                                new { prop.property_id, prop.company_id } equals
                                new { masterProp.property_id, masterProp.company_id }
                              join propSetting in DB.SKUPropertySettings on
                                new { masterProp.company_id, masterProp.property_id, prop.sku_id } equals
                                new { propSetting.company_id, propSetting.property_id, propSetting.sku_id }
                                into result
                              from r in result.DefaultIfEmpty()
                              join qualityData in DB.ProductQualities on
                                new { c1 = prop.company_id, c2 = prop.property_id, c3 = product_id } equals
                                new
                                {
                                    c1 = qualityData.company_id,
                                    c2 = qualityData.quality_property_id,
                                    c3 = qualityData.product_id
                                }
                              into result2
                              from r2 in result2.DefaultIfEmpty()
                              select new
                              {
                                  url = r == null ? 0 : r.url,
                                  lrl = r == null ? 0 : r.lrl,
                                  sigma = r == null ? 0 : r.sigma,
                                  sigma_multiplier = r == null ? 0 : r.sigma_multiplier,
                                  target = r == null ? 0 : r.target,
                                  ucl = r == null ? 0 : r.ucl,
                                  lcl = r == null ? 0 : r.lcl,
                                  upper_map = r == null ? 0 : r.upper_map,
                                  lower_map = r == null ? 0 : r.lower_map,
                                  weight = r == null ? 1 : r.weight,
                                  cp = r == null ? 0 : r.cp,
                                  cpk = r == null ? 0 : r.cpk,
                                  property_name = masterProp.property_name,
                                  property_id = masterProp.property_id,
                                  unit_name = unit.unit_name,
                                  quality_value = r2 == null ? 0 : r2.quality_value,
                                  qi = r2 == null ? 0 : r2.qi,
                                  product_id = product_id,
                                  company_id = r.company_id,
                                  udefect = mProp.url_defect,
                                  ldefect = mProp.lrl_defect
                              };
            return new JsonResult()
            {
                Data = QualityData,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AllowCrossSiteJson]
        public ActionResult GetProductDefect(int company_id,string product_id)
        {
            var ProdDefects = from pDefects in DB.ProductDefects
                              where string.Compare(pDefects.product_id, product_id, true) == 0
                              join defect in DB.Defects on
                              new { c1 = pDefects.company_id, c2 = pDefects.defect_id } equals
                              new { c1 = defect.company_id, c2 = defect.defect_id }
                              select new
                              {
                                  defect_id = defect.defect_id,
                                  defect_name = defect.defect_name,
                                  weight = pDefects.weight,
                                  severity = pDefects.severity
                              };
            return new JsonResult()
            {
                Data = ProdDefects.OrderBy(p => p.defect_name.ToLower()),
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion
        #endregion

        #region Historian
        [Authorize]
        public ActionResult GetCompressionMethod()
        {
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
                Data = DB.TagCompressions.Select(t => t)
            };
        }
        #endregion
    }
}