﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class SKUSegmentController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /SKUSegment/
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0;

            List<SKUSegments> Segments = Helper.GetSKUSegments(CompanyID);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, List<QualityProperties>> RelatedProperties = new Dictionary<string, List<QualityProperties>>();

            if (Segments.Count > 0)
            {
                foreach (SKUSegments seg in Segments)
                {
                    if (!RelatedCompanies.ContainsKey(seg.company_id))
                    {
                        string CompanyName = "All Companies";
                        if (seg.company_id > 0)
                        {
                            MasterCompanies Company = DB.Companies.Find(seg.company_id);
                            if (Company != null)
                                CompanyName = Company.company_name;
                        }

                        RelatedCompanies.Add(seg.company_id, CompanyName);
                    }

                    if (!RelatedProperties.ContainsKey(seg.segment_id))
                    {
                        List<QualityProperties> QProp =
                            Helper.GetQualityPropertiesBySKUSegment(seg.company_id, seg.segment_id);
                        if (QProp.Count > 0)
                            RelatedProperties.Add(seg.segment_id, QProp);
                    }
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedProperties = RelatedProperties;

            return View(Segments);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(SKUSegmentRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.SKUSegments.Where(q => q.company_id == register.company_id &&
                    string.Compare(q.segment_id, register.segment_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "SKU Segment already exist");
                else
                {
                    SKUSegments NewSegment = new SKUSegments();
                    NewSegment.company_id = register.company_id;
                    NewSegment.segment_id = register.segment_id;
                    NewSegment.segment_name = register.segment_name;
                    NewSegment.revised_by = User.Identity.Name;
                    DB.SKUSegments.Add(NewSegment);

                    if (Request["properties"] != null)
                    {
                        string[] Properties = Request["properties"].Split(',');
                        if (Properties.Length > 0)
                        {
                            foreach (string prop in Properties)
                            {
                                SKUSegmentProperties SegmentProp = new SKUSegmentProperties();
                                SegmentProp.company_id = NewSegment.company_id;
                                SegmentProp.segment_id = NewSegment.segment_id;
                                SegmentProp.property_id = prop;
                                SegmentProp.created_by = User.Identity.Name;
                                DB.SKUSegmentProperties.Add(SegmentProp);
                            }
                        }
                    }

                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot register new SKU segment");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string segmentID)
        {
            IEnumerable<SKUSegments> Segments = DB.SKUSegments.Where(q =>
                q.company_id == companyID && string.Compare(q.segment_id, segmentID, true) == 0);

            if (Segments.Count() <= 0)
                return RedirectToAction("Index");

            SKUSegmentRegisterModel EditSegment = new SKUSegmentRegisterModel();
            EditSegment.company_id = Segments.ElementAt(0).company_id;
            EditSegment.segment_id = Segments.ElementAt(0).segment_id;
            EditSegment.segment_name = Segments.ElementAt(0).segment_name;

            ViewBag.Properties = Helper.GetQualityProperties(EditSegment.company_id);

            return View(EditSegment);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(SKUSegmentRegisterModel edited)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<SKUSegments> Segments = DB.SKUSegments.Where(q =>
                    q.company_id == edited.company_id && string.Compare(q.segment_id, edited.segment_id, true) == 0);
                if (Segments.Count() > 0)
                {
                    bool IsDirty = false;

                    if (string.Compare(Segments.ElementAt(0).segment_name, edited.segment_name) != 0)
                    {
                        Segments.ElementAt(0).segment_name = edited.segment_name;
                        IsDirty = true;
                    }

                    var SelectedProperties = new HashSet<string>();
                    if (Request["properties"] != null)
                        SelectedProperties = new HashSet<string>(Request["properties"].Split(','));

                    IEnumerable<SKUSegmentProperties> SegmentProp = DB.SKUSegmentProperties.Where(q => 
                        q.company_id == edited.company_id && string.Compare(q.segment_id, edited.segment_id, true) == 0);

                    var CurrentProperties = new HashSet<string>(SegmentProp.Select(c => c.property_id));

                    IEnumerable<string> NewProperties = SelectedProperties.Where(p => !CurrentProperties.Contains(p));
                    if (NewProperties != null && NewProperties.Count() > 0)
                    {
                        foreach (string prop in NewProperties)
                        {
                            SKUSegmentProperties NewProp = new SKUSegmentProperties();
                            NewProp.company_id = edited.company_id;
                            NewProp.segment_id = edited.segment_id;
                            NewProp.property_id = prop;
                            NewProp.created_by = User.Identity.Name;

                            DB.SKUSegmentProperties.Add(NewProp);
                            IsDirty = true;
                        }
                    }

                    IEnumerable<SKUSegmentProperties> DeletedProperties = SegmentProp.Where(c =>
                        !SelectedProperties.Contains(c.property_id));
                    if (DeletedProperties != null && DeletedProperties.Count() > 0)
                    {
                        foreach (SKUSegmentProperties deleted in DeletedProperties)
                        {
                            DB.Entry(deleted).State = System.Data.Entity.EntityState.Deleted;
                            IsDirty = true;
                        }
                    }

                    if (IsDirty)
                    {
                        Segments.ElementAt(0).revised_by = User.Identity.Name;
                        Segments.ElementAt(0).revised_date = DateTime.Now;

                        DB.Entry(Segments.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;

                        DB.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Cannot update segment information");
            ViewBag.Properties = Helper.GetQualityProperties(edited.company_id);

            return View(edited);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string segmentID, int companyID)
        {
            IEnumerable<SKUSegments> Segments = DB.SKUSegments.Where(c =>
                c.company_id == companyID && string.Compare(c.segment_id, segmentID, true) == 0);

            bool IsDirty = false;

            if (Segments != null && Segments.Count() > 0)
            {
                DB.Entry(Segments.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            IEnumerable<SKUSegmentProperties> Properties = DB.SKUSegmentProperties.Where(p =>
                p.company_id == companyID && string.Compare(p.segment_id, segmentID, true) == 0);
            if (Properties != null && Properties.Count() > 0)
            {
                foreach (SKUSegmentProperties prop in Properties)
                    DB.Entry(prop).State = System.Data.Entity.EntityState.Deleted;
                IsDirty = true;
            }

            if (IsDirty)
                DB.SaveChanges();

            return RedirectToAction("Index");
        }
	}
}