﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using System.Text;
namespace DaVinxi.Controllers
{
    public class ProcessControl {
        public string component_id { get; set; }
        public string component_name { get; set; }
        public string tag_id { get; set; }
        public double timestamp { get; set; }
        public double value { get; set; }
        public string sku_id { get; set; }
        public string sku_name { get; set; }
        public string bath_id { get; set; }
        public string machine_id { get; set; }
        public string machine_name { get; set; }
        public string unit_name { get; set; }
        public double target { get; set; }
        public double min { get; set; }
        public double max { get; set; }
        public double lower_alarm_limit { get; set; }
        public double upper_alarm_limit { get; set; }
        public double rl { get; set; }
        public double rr { get; set; }
    }

    public class ProcessControlController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        [Authorize]
        // GET: ProcessControl
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);

            return View();
        }

        [Authorize]
        public ActionResult GetProcessData(int company_id, string machine_id, string[] component_id, 
            DateTime start_date, DateTime finish_date) 
        {
            StringBuilder ComponentClause = new StringBuilder();
            string ComponentWhereClause = "";
            if (component_id != null && component_id.Length > 0)
            {
                foreach (string component in component_id)
                    ComponentClause.AppendFormat(",'{0}'", component);
                ComponentWhereClause = string.Format(" and mComponent.component_id in({0})", 
                    ComponentClause.ToString().Substring(1));
            }

            string SQL = string.Format("select mComponent.component_id,mComponent.component_name," + 
                            "tag.tag_id,tag.[timestamp],tag.[value]," +
                            "batch.sku_id,sku.sku_name, batch.batch_id, batch.machine_id, machine.machine_name, units.unit_name," +
                            "COALESCE(machineSetting.[target],0) as [target]," +
                            "COALESCE(machineSetting.[min],0) as [min]," +
                            "COALESCE(machineSetting.[max],0) as [max]," +
                            "COALESCE(machineSetting.[lower_alarm_limit],0) as [lower_alarm_limit]," +
                            "COALESCE(machineSetting.[upper_alarm_limit],0) as [upper_alarm_limit]," +
                            "COALESCE(machineSetting.[rl],0) as [rl]," +
                            "COALESCE(machineSetting.[rr],0) as [rr] " +
                            "from [dbo].[MachineComponents] component " +
                                "join [dbo].[MasterMachineComponents] mComponent on component.company_id=mComponent.company_id " +
                                    "and component.component_id=mComponent.component_id " +
                                "join [dbo].[MasterEngineeringUnits] units on units.company_id=mComponent.company_id " +
                                    "and units.unit_id=mComponent.unit_id " +
                                "join [dbo].[TagDatas] tag on component.company_id=tag.company_id and " +
                                    "component.tag_id=tag.tag_id " +
                                "join [dbo].[MasterBatches] batch on tag.company_id=batch.company_id " +
                                    "and batch.[start_date] <= (CAST(tag.[timestamp] as datetime) -2 ) " +
                                    "and (CAST(tag.[timestamp] as datetime) -2 ) < = batch.[finish_date] " +
                                "join [dbo].[MasterMachines] machine on batch.company_id=machine.company_id " +
                                    "and batch.machine_id=machine.machine_id " +
                                "join [dbo].[MasterSKUs] sku on batch.company_id=sku.company_id " +
	                                "and batch.sku_id=sku.sku_id " +
                                "left outer join [dbo].[SKUMachines] machineSetting on " +
                                    "machineSetting.company_id=batch.company_id " +
                                    "and machineSetting.machine_id=batch.machine_id and machineSetting.sku_id=batch.sku_id " +
                                    "and machineSetting.component_id=mComponent.component_id where " +
                                    "component.company_id={0} and batch.machine_id='{1}' and " +
                                    "tag.[timestamp]>={2} and tag.[timestamp]<={3}{4} " +
                                    "order by batch.sku_id,mComponent.component_name,tag.[timestamp]", company_id, machine_id, 
                                    start_date.ToOADate(), finish_date.ToOADate(),
                                    ComponentWhereClause);
            List<ProcessControl> LstResult = DB.Database.SqlQuery<ProcessControl>(SQL).ToList();

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = LstResult.Select(r => new
                {
                    result = r,
                    pi = (r.value >= r.lower_alarm_limit && r.value <= r.target ?
                        Math.Pow((r.value - r.lower_alarm_limit) / (r.target - r.lower_alarm_limit), r.rl) :
                        Math.Pow((r.upper_alarm_limit - r.value) / (r.upper_alarm_limit - r.target), r.rr)),
                    totalMs = DateTime.FromOADate(r.timestamp).Subtract(Constant.Epoch).TotalMilliseconds
                })
            };
        }
    }
}