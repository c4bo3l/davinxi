﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class MachineTypeController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /MachineType/
        [Authorize]
        public ActionResult Index()
        {
            List<MasterMachineType> MachTypes = Helper.GetMachineType(
                Request.Cookies["company_id"] != null && 
                int.Parse(Request.Cookies["company_id"].Value) > 0 ? 
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, IList<MasterMachineComponents>> RelatedComponents = 
                new Dictionary<string, IList<MasterMachineComponents>>();

            if (MachTypes != null && MachTypes.Count() > 0)
            {

                foreach (MasterMachineType m in MachTypes)
                {
                    string ID = string.Format("{0}{1}", m.company_id, m.machine_type_id);

                    if (!RelatedComponents.ContainsKey(ID))
                    {
                        RelatedComponents.Add(ID, 
                            Helper.GetMachineComponentByMachineType(m.machine_type_id, m.company_id));
                    }

                    if (!RelatedCompanies.ContainsKey(m.company_id))
                    {
                        String CompanyName = "All Companies";

                        if (m.company_id > 0)
                        {
                            MasterCompanies Company = DB.Companies.Find(m.company_id);
                            if (Company != null)
                                CompanyName = Company.company_name;
                        }

                        RelatedCompanies.Add(m.company_id, CompanyName);
                    }
                }
            }

            ViewBag.RelatedComponents = RelatedComponents;
            ViewBag.RelatedCompanies = RelatedCompanies;

            return View(MachTypes);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(MachineTypeRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.MachineTypes.Where(m => m.company_id == register.company_id && 
                    string.Compare(m.machine_type_id, register.machine_type_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Machine type already exist");
                else
                {
                    MasterMachineType NewMachType = new MasterMachineType();
                    NewMachType.company_id = register.company_id;
                    NewMachType.machine_type_id = register.machine_type_id;
                    NewMachType.machine_type_name = register.machine_type_name;
                    NewMachType.revised_by = User.Identity.Name;

                    DB.MachineTypes.Add(NewMachType);

                    if (Request["components"] != null && Request["components"].Count() > 0)
                    {
                        foreach (string c in Request["components"].ToString().Split(','))
                        {
                            MachineTypeComponents MComponent = new MachineTypeComponents();
                            MComponent.company_id = NewMachType.company_id;
                            MComponent.machine_type_id = NewMachType.machine_type_id;
                            MComponent.component_id = c;
                            MComponent.created_by = User.Identity.Name;

                            DB.MachineTypeComponents.Add(MComponent);
                        }
                    }
                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("","Cannot add new machine type");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string id)
        {
            IEnumerable<MasterMachineType> MachType = DB.MachineTypes.Where(m => m.company_id == companyID && string.Compare(id, m.machine_type_id, true) == 0);

            if (MachType != null && MachType.Count() > 0)
            {
                MachineTypeRegisterModel EditMachine = new MachineTypeRegisterModel();
                EditMachine.company_id = MachType.ElementAt(0).company_id;
                EditMachine.machine_type_id = MachType.ElementAt(0).machine_type_id;
                EditMachine.machine_type_name = MachType.ElementAt(0).machine_type_name;

                return View(EditMachine);
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(MachineTypeRegisterModel editModel)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<MasterMachineType> MachType = DB.MachineTypes.Where(m => 
                    m.company_id == editModel.company_id && 
                    string.Compare(editModel.machine_type_id, m.machine_type_id, true) == 0);

                if (MachType != null && MachType.Count() > 0)
                {
                    bool IsDirty = false;

                    if (string.Compare(editModel.machine_type_name, 
                        MachType.ElementAt(0).machine_type_name) != 0)
                    {
                        IsDirty = true;
                        MachType.ElementAt(0).machine_type_name = editModel.machine_type_name;
                    }

                    if (IsDirty)
                    {
                        MachType.ElementAt(0).revised_date = DateTime.Now;
                        MachType.ElementAt(0).revised_by = User.Identity.Name;
                        DB.Entry(MachType.ElementAt(0)).State = 
                            System.Data.Entity.EntityState.Modified;
                    }

                    if (Request["components"] != null && Request["components"].Length > 0)
                    {
                        List<MachineTypeComponents> RelatedComponent =
                            Helper.GetMachineTypeComponent(
                            MachType.ElementAt(0).machine_type_id, MachType.ElementAt(0).company_id);

                        string[] SplittedComponents = Request["components"].Split(',');

                        var SelectedComponents = new HashSet<string>(SplittedComponents);
                        var CurrentComponents = new HashSet<string>(RelatedComponent.Select(p => p.component_id));

                        IEnumerable<string> NewComponents = SplittedComponents.Where(
                            c => !CurrentComponents.Contains(c));
                        if (NewComponents != null && NewComponents.Count() > 0)
                        {
                            foreach (string c in NewComponents)
                            {
                                MachineTypeComponents NewMachineComponent = new MachineTypeComponents();
                                NewMachineComponent.company_id = MachType.ElementAt(0).company_id;
                                NewMachineComponent.component_id = c;
                                NewMachineComponent.machine_type_id = MachType.ElementAt(0).machine_type_id;
                                NewMachineComponent.created_by = User.Identity.Name;
                                DB.MachineTypeComponents.Add(NewMachineComponent);
                            }
                            IsDirty = true;
                        }

                        IEnumerable<MachineTypeComponents> DeletedComponents = 
                            RelatedComponent.Where(c => 
                            !SelectedComponents.Contains(c.component_id));
                        if (DeletedComponents != null && DeletedComponents.Count() > 0)
                        {
                            foreach (MachineTypeComponents c in DeletedComponents)
                                DB.Entry(c).State = System.Data.Entity.EntityState.Deleted;
                            IsDirty = true;
                        }

                    }

                    if (IsDirty)
                        DB.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Cannot edit machine type");
            return View(editModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string machTypeID, int companyID)
        {
            IEnumerable<MasterMachineType> MachType =
                DB.MachineTypes.Where(m =>
                    string.Compare(machTypeID, m.machine_type_id, true) == 0 &&
                    m.company_id == companyID).ToList();
            if (MachType != null && MachType.Count() > 0)
            {
                DB.Entry(MachType.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
                
                IEnumerable<MachineTypeComponents> Components =
                    Helper.GetMachineTypeComponent(machTypeID, companyID);
                if (Components != null && Components.Count() > 0)
                {
                    foreach (MachineTypeComponents c in Components)
                        DB.Entry(c).State = System.Data.Entity.EntityState.Deleted;
                }

                DB.SaveChanges();
            }
            
            return RedirectToAction("Index");
        }
	}
}