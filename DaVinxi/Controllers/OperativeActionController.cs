﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
namespace DaVinxi.Controllers
{
    public class OperativeActionController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: OperativeAction
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;
            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult GetOperativeActions(int companyID)
        {
            return Json(DB.OperativeActions.Where(o => o.company_id == companyID).OrderBy(o => o.action_name).ToList());
        }

        [Authorize]
        public ActionResult Register(int companyID)
        {
            MasterOperativeAction NewAction = new MasterOperativeAction();
            NewAction.company_id = companyID;
            NewAction.revised_by = User.Identity.Name;
            return View(NewAction);
        }

        [Authorize, HttpPost]
        public ActionResult Register(MasterOperativeAction newAction)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (DB.OperativeActions.Where(o => o.company_id == newAction.company_id && string.Compare(o.action_id, newAction.action_id, true) == 0).Count() <= 0)
                    {
                        DB.OperativeActions.Add(newAction);
                        DB.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                        ModelState.AddModelError("", "Operative action already exist");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ex", ex.Message);
            }
            return View(newAction);
        }

        [Authorize]
        public ActionResult Update(int companyID, string actionID)
        {
            MasterOperativeAction OAction = DB.OperativeActions.Where(o => o.company_id == companyID && 
                string.Compare(o.action_id, actionID, true) == 0).FirstOrDefault();
            if (OAction == null)
                return RedirectToAction("Index");
            return View(OAction);
        }

        [Authorize, HttpPost]
        public ActionResult Update(MasterOperativeAction editedAction)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    MasterOperativeAction ExistedAction = DB.OperativeActions.Where(o => 
                        o.company_id == editedAction.company_id && 
                        string.Compare(o.action_id, editedAction.action_id, true) == 0).FirstOrDefault();
                    if (ExistedAction != null)
                    {
                        bool IsDirty = false;

                        if (string.Compare(editedAction.action_name, ExistedAction.action_name) != 0)
                        {
                            IsDirty = true;
                            ExistedAction.action_name = editedAction.action_name;
                        }

                        if (string.Compare(editedAction.note, ExistedAction.note) != 0)
                        {
                            IsDirty = true;
                            ExistedAction.note = editedAction.note;
                        }

                        if (IsDirty)
                        {
                            DB.Entry(ExistedAction).State = System.Data.Entity.EntityState.Modified;
                            DB.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ex", ex.Message);
            }
            return View(editedAction);
        }

        [Authorize, HttpPost]
        public ActionResult Delete(int companyID, string actionID)
        {
            try
            {
                MasterOperativeAction OAction = DB.OperativeActions.Where(o => o.company_id == companyID &&
                string.Compare(o.action_id, actionID, true) == 0).FirstOrDefault();
                if (OAction == null)
                    return RedirectToAction("Index");
                else
                {
                    DB.Entry(OAction).State = System.Data.Entity.EntityState.Deleted;
                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message });
            }
            return Json(new { message = "success" });
        }
    }
}