﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using System.Web.UI.DataVisualization.Charting;

namespace DaVinxi.Controllers
{
    public class SKUPropertySettingController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /SKUPropertySetting/
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [Authorize]
        [HttpPost]
        public string Simulate(int simCompany, string simSKU,int simIteration, string simMachine, DateTime simDate, 
            double simDuration, double simWeight, IEnumerable<PostedSKUPropSimulation> simProp)
        {
            try
            {
                double ProgressStep = 100.0 / (1.0 + (double)(simProp.Count() * simIteration));

                MasterBatch SimBatch = new MasterBatch();
                SimBatch.batch_id = string.Format("sim_{0}", DateTime.Now.ToString("MMddyyyyHHmmssfff"));
                SimBatch.company_id = simCompany;
                SimBatch.start_date = simDate;
                SimBatch.finish_date = simDate.AddMinutes((double)(simIteration + 1) * simDuration);
                SimBatch.sku_id = simSKU;
                SimBatch.machine_id = simMachine;
                SimBatch.revised_by = User.Identity.Name;
                SimBatch.revised_date = DateTime.Now;

                DB.Batches.Add(SimBatch);
                
                Chart HelpChart = new Chart();
                Random Rand = new Random();

                Dictionary<string, SKUPropertiesSetting> DictSetting = new Dictionary<string, SKUPropertiesSetting>();
                Dictionary<string, QualityProperties> DictProp = new Dictionary<string, QualityProperties>();

                for (int i = 0; i < simIteration; i++)
                {
                    MasterProduct SimProduct = new MasterProduct();
                    SimProduct.batch_id = SimBatch.batch_id;
                    SimProduct.company_id = simCompany;
                    SimProduct.created_date = DateTime.Now;
                    SimProduct.finish_date = simDate.AddMinutes((i + 1) * simDuration);
                    SimProduct.start_date = simDate.AddMinutes(i * simDuration);
                    SimProduct.product_id = string.Format("prod_sim_{0}", DateTime.Now.ToString("MMddyyyyHHmmssfff"));
                    SimProduct.qi = 0;
                    SimProduct.revised_by = User.Identity.Name;
                    SimProduct.revised_date = DateTime.Now;
                    
                    double SumPropertyWeight = 0, TQI = 1;

                    foreach (PostedSKUPropSimulation prop in simProp)
                    {
                        if (!DictSetting.ContainsKey(prop.property_id))
                        {
                            DictSetting.Add(prop.property_id,
                                DB.SKUPropertySettings.Where(s =>
                                    string.Compare(s.property_id, prop.property_id, true) == 0 &&
                                    string.Compare(s.sku_id, simSKU, true) == 0 && s.company_id == simCompany).FirstOrDefault());
                        }

                        if (!DictProp.ContainsKey(prop.property_id))
                        {
                            DictProp.Add(prop.property_id, DB.QualityProperties.Where(p => p.company_id == simCompany &&
                                string.Compare(p.property_id, prop.property_id, true) == 0).FirstOrDefault());
                        }

                        
                        ProductQuality Quality = new ProductQuality();
                        Quality.company_id = simCompany;
                        Quality.product_id = SimProduct.product_id;
                        Quality.quality_property_id = prop.property_id;
                        Quality.quality_value = Math.Round((HelpChart.DataManipulator.Statistics.InverseNormalDistribution(Rand.NextDouble()) *
                            (prop.sigma * prop.sigmax)) + prop.target, 3, MidpointRounding.AwayFromZero);
                        Quality.revised_by = User.Identity.Name;
                        Quality.revised_date = DateTime.Now;
                        Quality.start_date = simDate.AddMinutes(i * simDuration);
                        Quality.qi = 0;

                        if (Quality.quality_value >= DictSetting[prop.property_id].lrl &&
                            Quality.quality_value <= DictSetting[prop.property_id].target)
                        {
                            Quality.qi = Math.Pow((Quality.quality_value - DictSetting[prop.property_id].lrl) /
                                (DictSetting[prop.property_id].target - DictSetting[prop.property_id].lrl),
                                DictSetting[prop.property_id].lower_map);
                        }
                        else if (Quality.quality_value > DictSetting[prop.property_id].target &&
                            Quality.quality_value <= DictSetting[prop.property_id].url)
                        {
                            Quality.qi = Math.Pow((DictSetting[prop.property_id].url - Quality.quality_value) /
                                (DictSetting[prop.property_id].url - DictSetting[prop.property_id].target),
                                DictSetting[prop.property_id].upper_map);
                        }

                        Quality.qi = Math.Round(Quality.qi, 3, MidpointRounding.AwayFromZero);

                        TQI *= Math.Pow(Quality.qi, DictSetting[prop.property_id].weight);
                        SumPropertyWeight += DictSetting[prop.property_id].weight;

                        if (Quality.quality_value < DictSetting[prop.property_id].lrl || Quality.quality_value > DictSetting[prop.property_id].url)
                        {
                            ProductDefect Defect = new ProductDefect();
                            Defect.company_id = simCompany;
                            Defect.defect_id = Quality.quality_value < DictSetting[prop.property_id].lrl ?
                                DictProp[prop.property_id].lrl_defect : DictProp[prop.property_id].url_defect;
                            Defect.product_id = SimProduct.product_id;
                            Defect.revised_by = User.Identity.Name;
                            Defect.revised_date = DateTime.Now;
                            Defect.severity = 4;
                            Defect.start_date = Quality.start_date;
                            Defect.weight = simWeight;

                            DB.ProductDefects.Add(Defect);
                        }

                        DB.ProductQualities.Add(Quality);
                        
                    }

                    SimProduct.qi = Math.Round(Math.Pow(TQI, 1.0 / SumPropertyWeight), 3, MidpointRounding.AwayFromZero);

                    DB.Products.Add(SimProduct);
                    System.Threading.Thread.Sleep(100);
                }

                DB.SaveChanges();
                return "0x000";
            }
            catch (Exception ex)
            {
                DB.DetachAll();
                return ex.Message;
            }
        }
	}
}