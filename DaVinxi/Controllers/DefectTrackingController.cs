﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using System.Text;

namespace DaVinxi.Controllers
{
    public class DefectTrackingController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: DefectTracking
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [AllowCrossSiteJson]
        public ActionResult GetDefectTracking(DefectTrackingFilterModel filter)
        {
            try
            {
                StringBuilder WhereClause = new StringBuilder(), 
                    PChartWhereClause = new StringBuilder(), ConCatPChart = new StringBuilder();

                if (filter.company_id > 0)
                {
                    WhereClause.AppendFormat(" and product.company_id={0}", filter.company_id);
                    PChartWhereClause.AppendFormat(" and product.company_id={0}", filter.company_id);
                }

                if (filter.division_id != null && filter.division_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and division.division_id='{0}'", filter.division_id);
                    PChartWhereClause.AppendFormat(" and division.division_id='{0}'", filter.division_id);
                }

                if (filter.block_id != null && filter.block_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and block.block_id='{0}'", filter.block_id);
                    PChartWhereClause.AppendFormat(" and block.block_id='{0}'", filter.block_id);
                }

                if (filter.machine_id != null && filter.machine_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and machine.machine_id='{0}'", filter.machine_id);
                    PChartWhereClause.AppendFormat(" and machine.machine_id='{0}'", filter.machine_id);
                }

                if (filter.category_id != null && filter.category_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and category.category_id='{0}'", filter.category_id);
                    PChartWhereClause.AppendFormat(" and category.category_id='{0}'", filter.category_id);
                }

                if (filter.subcategory_id != null && filter.subcategory_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and subcategory.subcategory_id='{0}'", filter.subcategory_id);
                    PChartWhereClause.AppendFormat(" and subcategory.subcategory_id='{0}'", filter.subcategory_id);
                }

                if (filter.business_id != null && filter.business_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and business.business_id='{0}'", filter.business_id);
                    PChartWhereClause.AppendFormat(" and business.business_id='{0}'", filter.business_id);
                }

                if (filter.segment_id != null && filter.segment_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and segment.segment_id='{0}'", filter.segment_id);
                    PChartWhereClause.AppendFormat(" and segment.segment_id='{0}'", filter.segment_id);
                }

                if (filter.sku_id != null && filter.sku_id.Length > 0)
                {
                    WhereClause.AppendFormat(" and sku.sku_id='{0}'", filter.sku_id);
                    PChartWhereClause.AppendFormat(" and sku.sku_id='{0}'", filter.sku_id);
                }

                /*if (filter.batch_id != null && filter.batch_id.Length > 0)
                    WhereClause.AppendFormat(" and batch.batch_id='{0}'", filter.batch_id);*/

                if (filter.startDate != null && filter.finishDate != null)
                {
                    if (filter.startDate.Subtract(filter.finishDate).TotalSeconds > 0)
                    {
                        DateTime TempDT = filter.startDate;
                        filter.startDate = filter.finishDate;
                        filter.finishDate = TempDT;
                    }
                    WhereClause.AppendFormat(" and product.start_date>='{0}'", filter.startDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    WhereClause.AppendFormat(" and product.start_date<='{0}'", filter.finishDate.ToString("yyyy-MM-dd HH:mm:ss"));

                    PChartWhereClause.AppendFormat(" and product.start_date>='{0}'", filter.startDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    PChartWhereClause.AppendFormat(" and product.start_date<='{0}'", filter.finishDate.ToString("yyyy-MM-dd HH:mm:ss"));
                }

                if (filter.severity >= 0)
                    WhereClause.AppendFormat(" and defectData.severity={0}", filter.severity);

                string ParentDefect = (filter.targetParentDefect != null && filter.targetParentDefect.Length > 0 &&
                    string.Compare(filter.targetParentDefect, "null", true) != 0 ?
                    string.Format("'{0}'", filter.targetParentDefect) : "null"),
                    WhereStr = WhereClause.ToString().Length > 0 ? string.Format("WHERE {0}", WhereClause.ToString().Substring(5)) : "",
                    PChartWhereStr = PChartWhereClause.ToString().Length > 0 ? string.Format("WHERE {0}", PChartWhereClause.ToString().Substring(5)) : "";

                string SQL = string.Format("select defect.company_id, defect.defect_id, defect.defect_name, " +
                    "COALESCE(count(defect.defect_id),0) as sum_count, " +
                    "COALESCE(sum(defectData.weight),0) as sum_weight from [dbo].[ProductDefects] defectData join " +
                        "[dbo].[MasterDefects] defect on defectData.company_id=defect.company_id and " +
                        "defect.defect_id=[dbo].[GetParentDefect](defectData.company_id,defectData.defect_id,{0}) join " +
                    "[dbo].[MasterProducts] product on defectData.company_id=product.company_id and " +
                        "defectData.product_id=product.product_id join " +
                    "[dbo].[MasterBatches] batch on batch.company_id=product.company_id and " +
                        "batch.batch_id=product.batch_id join " +
                    "[dbo].[MasterMachines] machine on machine.company_id=product.company_id and " +
                        "machine.machine_id=batch.machine_id join " +
                    "[dbo].[MasterBlocks] block on machine.company_id=block.company_id and " +
                        "machine.block_id=block.block_id join " +
                    "[dbo].[MasterDivisions] division on division.company_id=block.company_id and " +
                        "division.division_id=block.division_id join " +
                    "[dbo].[MasterSKUs] sku on batch.company_id=sku.company_id and " +
                        "batch.sku_id=sku.sku_id join " +
                    "[dbo].[SKUBusinesses] business on business.company_id=sku.company_id and " +
                        "business.business_id=sku.business_id join " +
                    "[dbo].[SKUSegments] segment on segment.company_id=sku.company_id and " +
                        "segment.segment_id=sku.segment_id join " +
                    "[dbo].[SKUSubcategories] subcategory on subcategory.company_id=sku.company_id and " +
                        "subcategory.subcategory_id=sku.subcategory_id join " +
                    "[dbo].[SKUCategories] category on subcategory.company_id=category.company_id and " +
                    "subcategory.category_id=category.category_id {1} " +
                    "group by defect.company_id, defect.defect_id, defect.defect_name",
                    ParentDefect, WhereStr);
                Console.WriteLine(SQL);

                List<DefectTrackingModel> DModel = DB.Database.SqlQuery<DefectTrackingModel>(SQL).ToList();

                StringBuilder PChartGroupedBy = new StringBuilder();

                if (filter.chkBatch)
                {
                    PChartGroupedBy.Append(",pData.batch_id");
                    ConCatPChart.Append(",'-','Batch: ',pData.batch_id");

                }
                if (filter.chkBlock)
                {
                    PChartGroupedBy.Append(",pData.block_id,pData.block_name");
                    ConCatPChart.Append(",'-','Block: ',pData.block_name");
                }
                if (filter.chkBusiness)
                {
                    PChartGroupedBy.Append(",pData.business_id,pData.business_name");
                    ConCatPChart.Append(",'-','Business: ',pData.business_name");
                }
                if (filter.chkCategory)
                {
                    PChartGroupedBy.Append(",pData.category_id,pData.category_name");
                    ConCatPChart.Append(",'-','Category: ',pData.category_name");
                }
                if (filter.chkDivision)
                {
                    PChartGroupedBy.Append(",pData.division_id,pData.division_name");
                    ConCatPChart.Append(",'-','Division: ',pData.division_name");
                }
                if (filter.chkMachine)
                {
                    PChartGroupedBy.Append(",pData.machine_id,pData.machine_name");
                    ConCatPChart.Append(",'-','Machine: ',pData.machine_name");
                }
                if (filter.chkSegment)
                {
                    PChartGroupedBy.Append(",pData.segment_id,pData.segment_name");
                    ConCatPChart.Append(",'-','Segment: ',pData.segment_name");
                }
                if (filter.chkSKU)
                {
                    PChartGroupedBy.Append(",pData.sku_id,pData.sku_name");
                    ConCatPChart.Append(",'-','SKU: ',pData.sku_name");
                }
                if (filter.chkSubcategory)
                {
                    PChartGroupedBy.Append(",pData.subcategory_id,pData.subcategory_name");
                    ConCatPChart.Append(",'-','SubCategory: ',pData.subcategory_name");
                }

                SQL = string.Format("SELECT pData.company_id,pData.timediff * {1} as timediff,{6} as xName," +
                                    "COUNT(pData.product_id) as total_product," +
                                    "SUM(pData.is_defected_product) as count_defect_product," +
                                    "ROUND(CAST(SUM(pData.is_defected_product) as float) / " +
                                    "CAST(COUNT(pData.product_id) as float), 3) as fraction_defective FROM (" +
                                        "SELECT data.company_id,data.product_id,data.batch_id,data.sku_id,data.sku_name," +
                                            "data.business_id,data.business_name,data.segment_id,data.segment_name," +
                                            "data.subcategory_id,data.subcategory_name,data.category_id," +
                                            "data.category_name,data.machine_id,data.machine_name," +
                                            "data.block_id,data.block_name,data.division_id,division_name," +
                                            "data.start_date,data.timediff," +
                                            "IIF(SUM(data.is_defected_product) > 0, 1, 0) as is_defected_product FROM (" +
                                                "SELECT product.company_id,product.product_id,batch.batch_id," +
                                                    "sku.sku_id,sku.sku_name,business.business_id,business.business_name," +
                                                    "segment.segment_id,segment.segment_name,subcategory.subcategory_id," +
                                                    "subcategory.subcategory_name,category.category_id,category.category_name," +
                                                    "machine.machine_id,machine.machine_name,block.block_id,block.block_name," +
                                                    "division.division_id,division_name,product.start_date," +
                                                    "FLOOR(DATEDIFF(second,'{0}',product.start_date)/({1})) as timediff," +
                                                    "IIF([dbo].[GetParentDefect](defectData.company_id," +
                                                        "defectData.defect_id,{2}) IS NOT NULL OR " +
                                                        "defectData.defect_id={2},1,0) as is_defected_product " +
                                                    "FROM [dbo].[MasterProducts] product " +
                                                    "LEFT OUTER JOIN [dbo].[ProductDefects] defectData " +
                                                        "ON product.company_id=defectData.company_id AND " +
                                                        "defectData.product_id=product.product_id AND defectData.severity={5} " +
                                                    "LEFT OUTER JOIN [dbo].[MasterDefects] defect " +
                                                        "ON defectData.company_id=defect.company_id AND " +
                                                        "defectData.defect_id=defect.defect_id AND defect.data_type='p' " +
                                                    "JOIN [dbo].[MasterBatches] batch " +
                                                        "ON product.company_id=batch.company_id AND " +
                                                        "product.batch_id=batch.batch_id " +
                                                    "JOIN [dbo].[MasterSKUs] sku " +
                                                        "ON batch.company_id=sku.company_id AND batch.sku_id=sku.sku_id " +
                                                    "JOIN [dbo].[SKUBusinesses] business " +
                                                        "ON sku.company_id=business.company_id AND " +
                                                        "sku.business_id=business.business_id " +
                                                    "JOIN [dbo].[SKUSegments] segment " +
                                                        "ON sku.company_id=segment.company_id AND " +
                                                        "sku.segment_id=segment.segment_id " +
                                                    "JOIN [dbo].[SKUSubcategories] subcategory " +
                                                        "ON sku.company_id=subcategory.company_id AND " +
                                                        "sku.subcategory_id=subcategory.subcategory_id " +
                                                    "JOIN [dbo].[SKUCategories] category " +
                                                        "ON category.company_id=subcategory.company_id AND " +
                                                        "category.category_id=subcategory.category_id " +
                                                    "JOIN [dbo].[MasterMachines] machine " +
                                                        "ON machine.company_id=product.company_id AND " +
                                                        "machine.machine_id=batch.machine_id " +
                                                    "JOIN [dbo].[MasterBlocks] block " +
                                                        "ON block.company_id=machine.company_id AND " +
                                                        "block.block_id=machine.block_id " +
                                                    "JOIN [dbo].[MasterDivisions] division " +
                                                        "ON division.division_id=block.division_id AND " +
                                                        "division.company_id=block.company_id {3}) data " +
                                                "GROUP BY data.company_id,data.product_id,data.batch_id,data.sku_id," +
                                                "data.sku_name,data.business_id,data.business_name,data.segment_id," +
                                                "data.segment_name,data.subcategory_id,data.subcategory_name," +
                                                "data.category_id,data.category_name,data.machine_id,data.machine_name," +
                                                "data.block_id,data.block_name,data.division_id,division_name," +
                                                "data.start_date,data.timediff) pData GROUP BY pData.company_id,pData.timediff{4}",
                                                filter.startDate.ToString("yyyy-MM-dd HH:mm:ss"), filter.GetSeconds(),
                                                ParentDefect, PChartWhereStr, PChartGroupedBy.ToString(), filter.severity,
                                                ConCatPChart.ToString().Length > 0 ? string.Format("CONCAT('',{0})", ConCatPChart.ToString().Substring(5)) : "''");

                List<PChartDataModel> PModel = DB.Database.SqlQuery<PChartDataModel>(SQL).ToList();

                if (PModel != null && PModel.Count > 0)
                {
                    foreach (PChartDataModel p in PModel)
                    {
                        p.formated_start_date = filter.startDate.AddSeconds(p.timediff).ToString("yyyy/MM/dd HH:mm");
                    }
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        rawData = DModel,
                        pData = PModel
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        
    }
}