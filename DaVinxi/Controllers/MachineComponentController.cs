﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class MachineComponentController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /MachineComponent/

        [Authorize]
        public ActionResult Index()
        {
            List<MasterMachineComponents> Components = DB.Components.ToList();

            if (Request.Cookies["company_id"] != null && int.Parse(Request.Cookies["company_id"].Value) > 0)
                Components = Components.Where(u =>
                    u.company_id == int.Parse(Request.Cookies["company_id"].Value)).ToList();

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, string> RelatedUnits = new Dictionary<string, string>();

            foreach (MasterMachineComponents component in Components)
            {
                if (!RelatedCompanies.ContainsKey(component.company_id))
                {
                    String CompanyName = "All Companies";

                    if (component.company_id > 0)
                    {
                        MasterCompanies Company = DB.Companies.Find(component.company_id);
                        if (Company != null)
                            CompanyName = Company.company_name;
                    }

                    RelatedCompanies.Add(component.company_id, CompanyName);
                }

                if (!RelatedUnits.ContainsKey(component.unit_id))
                {
                    String UnitName = "";

                    IEnumerable<MasterEngineeringUnits> Units = DB.EngineeringUnits.Where(
                        u => u.company_id == component.company_id && 
                            string.Compare(u.unit_id, component.unit_id, true) == 0);

                    if (Units != null && Units.Count() > 0)
                        UnitName = Units.ElementAt(0).unit_name;

                    RelatedUnits.Add(component.unit_id, UnitName);
                }
            }

            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedUnits = RelatedUnits;

            return View(Components);
        }

        [Authorize]
        public ActionResult Register()
        {

            ViewBag.Companies = Helper.GetCompanies(
                Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Units = Helper.GetEngineeringUnits(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(MachineComponentRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.Components.Where(m =>
                    m.company_id == register.company_id &&
                    string.Compare(m.component_id, register.component_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Component already exist");
                else
                {
                    MasterMachineComponents NewComponent = new MasterMachineComponents();
                    NewComponent.company_id = register.company_id;
                    NewComponent.component_id = register.component_id;
                    NewComponent.component_name = register.component_name;
                    NewComponent.unit_id = register.unit_id;
                    NewComponent.is_numeric = register.is_numeric;
                    NewComponent.revised_by = User.Identity.Name;

                    DB.Components.Add(NewComponent);
                    DB.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot register new machine component");

            ViewBag.Companies = Helper.GetCompanies(
                Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Units = Helper.GetEngineeringUnits(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string id)
        {
            IEnumerable<MasterMachineComponents> Component = DB.Components.Where(m =>
                    m.company_id == companyID &&
                    string.Compare(m.component_id, id, true) == 0);
            
            if (Component.Count() <= 0)
                return RedirectToAction("Index");

            MachineComponentRegisterModel EditComponent = new MachineComponentRegisterModel();
            EditComponent.company_id = Component.ElementAt(0).company_id;
            EditComponent.component_id = Component.ElementAt(0).component_id;
            EditComponent.component_name = Component.ElementAt(0).component_name;
            EditComponent.unit_id = Component.ElementAt(0).unit_id;
            EditComponent.is_numeric = Component.ElementAt(0).is_numeric;

            ViewBag.Units = Helper.GetEngineeringUnits(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            return View(EditComponent);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(MachineComponentRegisterModel editModel)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<MasterMachineComponents> Component = DB.Components.Where(m =>
                        m.company_id == editModel.company_id &&
                        string.Compare(m.component_id, editModel.component_id, true) == 0);

                if (Component.Count() <= 0)
                    return RedirectToAction("Index");

                bool IsDirty = false;

                if (string.Compare(editModel.component_name, 
                    Component.ElementAt(0).component_name) != 0)
                {
                    Component.ElementAt(0).component_name = editModel.component_name;
                    IsDirty = true;
                }

                if (string.Compare(editModel.unit_id,
                    Component.ElementAt(0).unit_id) != 0)
                {
                    Component.ElementAt(0).unit_id = editModel.unit_id;
                    IsDirty = true;
                }

                if (editModel.is_numeric != Component.ElementAt(0).is_numeric)
                {
                    Component.ElementAt(0).is_numeric = editModel.is_numeric;
                    IsDirty = true;
                }

                if (IsDirty)
                {
                    Component.ElementAt(0).revised_date = DateTime.Now;
                    Component.ElementAt(0).revised_by = User.Identity.Name;

                    DB.Entry(Component.ElementAt(0)).State = 
                        System.Data.Entity.EntityState.Modified;
                    DB.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.Units = Helper.GetEngineeringUnits(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            ModelState.AddModelError("", "Cannot edit component");

            return View(editModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string componentID, int companyID)
        { 
            IEnumerable<MasterMachineComponents> Component = DB.Components.Where(m =>
                    m.company_id == companyID &&
                    string.Compare(m.component_id, componentID, true) == 0);

            if (Component.Count() > 0)
            {
                DB.Entry(Component.ElementAt(0)).State =
                        System.Data.Entity.EntityState.Deleted;

                IEnumerable<MachineTypeComponents> MachTypeComponents = 
                    DB.MachineTypeComponents.Where(m => m.company_id == companyID && 
                        string.Compare(m.component_id, componentID, true) == 0);

                if (MachTypeComponents != null && MachTypeComponents.Count() > 0)
                {
                    foreach (MachineTypeComponents m in MachTypeComponents)
                        DB.Entry(m).State = System.Data.Entity.EntityState.Deleted;
                }

                IEnumerable<MachineComponents> MachComponents =
                    DB.MachineComponents.Where(m => m.company_id == companyID &&
                        string.Compare(m.component_id, componentID, true) == 0);

                if (MachComponents != null && MachComponents.Count() > 0)
                {
                    foreach (MachineComponents m in MachComponents)
                        DB.Entry(m).State = System.Data.Entity.EntityState.Deleted;
                }

                DB.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}
