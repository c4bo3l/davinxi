﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class DivisionController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /Division/

        [Authorize]
        public ActionResult Index()
        {
            List<MasterDivisions> Divisions = Helper.GetDivisions(
                Request.Cookies["company_id"] != null && 
                int.Parse(Request.Cookies["company_id"].Value) > 0 ? 
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            Dictionary<int, string> RelatedCompanies = new Dictionary<int, string>();
            Dictionary<string, string> RelatedUser = new Dictionary<string, string>();

            foreach (MasterDivisions division in Divisions)
            {
                if (!RelatedCompanies.ContainsKey(division.company_id))
                {
                    String CompanyName = "All Companies";

                    if (division.company_id > 0)
                    {
                        MasterCompanies Company = DB.Companies.Find(division.company_id);
                        if (Company != null)
                            CompanyName = Company.company_name;
                    }

                    RelatedCompanies.Add(division.company_id, CompanyName);
                }

                if (!RelatedUser.ContainsKey(division.pic_username))
                {
                    String PICName = "";
                    if (division.pic_username.Length > 0)
                    {
                        MasterUser MUser = DB.Users.Find(division.pic_username);
                        if (MUser != null)
                            PICName = MUser.full_name;
                    }

                    RelatedUser.Add(division.pic_username, PICName);
                }
            }
            
            ViewBag.RelatedCompanies = RelatedCompanies;
            ViewBag.RelatedUser = RelatedUser;

            return View(Divisions);
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null ? int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ? int.Parse(Request.Cookies["company_id"].Value) : 0);

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(DivisionRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.Divisions.Where(d => d.company_id == register.company_id && 
                    string.Compare(register.division_id, d.division_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Division already existed");
                else
                {
                    MasterDivisions Division = new MasterDivisions();

                    Division.company_id = register.company_id;
                    Division.division_id = register.division_id;
                    Division.division_name = register.division_name;
                    Division.pic_username = register.pic_username;
                    Division.description = register.description;
                    Division.revised_by = User.Identity.Name;

                    DB.Divisions.Add(Division);
                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null ? int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ? int.Parse(Request.Cookies["company_id"].Value) : 0);

            ModelState.AddModelError("", "Cannot register new division");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID,string divisionID)
        {
            IEnumerable<MasterDivisions> Divisions = DB.Divisions.Where(d => 
                d.company_id == companyID && string.Compare(d.division_id, divisionID, true) == 0);
            if (Divisions != null && Divisions.Count() > 0)
            {
                DivisionRegisterModel EditDivision = new DivisionRegisterModel();
                EditDivision.company_id = Divisions.ElementAt(0).company_id;
                EditDivision.division_id = Divisions.ElementAt(0).division_id;
                EditDivision.division_name = Divisions.ElementAt(0).division_name;
                EditDivision.pic_username = Divisions.ElementAt(0).pic_username;
                EditDivision.description = Divisions.ElementAt(0).description;

                ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ? int.Parse(Request.Cookies["company_id"].Value) : 0);

                return View(EditDivision);
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(DivisionRegisterModel editedDivision)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<MasterDivisions> Divisions = DB.Divisions.Where(d =>
                    d.company_id == editedDivision.company_id && 
                    string.Compare(d.division_id, editedDivision.division_id, true) == 0);
                if (Divisions != null && Divisions.Count() > 0)
                {
                    bool IsDirty = false;
                    if (string.Compare(editedDivision.division_name, Divisions.ElementAt(0).division_name) != 0)
                    {
                        Divisions.ElementAt(0).division_name = editedDivision.division_name;
                        IsDirty = true;
                    }

                    if (string.Compare(editedDivision.pic_username, Divisions.ElementAt(0).pic_username) != 0)
                    {
                        Divisions.ElementAt(0).pic_username = editedDivision.pic_username;
                        IsDirty = true;
                    }

                    if (string.Compare(editedDivision.description, Divisions.ElementAt(0).description) != 0)
                    {
                        Divisions.ElementAt(0).description = editedDivision.description;
                        IsDirty = true;
                    }

                    if (IsDirty)
                    {
                        DB.Entry(Divisions.ElementAt(0)).State = 
                            System.Data.Entity.EntityState.Modified;
                        DB.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }

                return HttpNotFound();
            }

            ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ? int.Parse(Request.Cookies["company_id"].Value) : 0);

            ModelState.AddModelError("", "Cannot edit division");
            return View(editedDivision);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string divisionID, int companyID)
        {
            IEnumerable<MasterDivisions> Divisions = DB.Divisions.Where(d =>
                    d.company_id == companyID &&
                    string.Compare(d.division_id, divisionID, true) == 0);
            if (Divisions != null && Divisions.Count() > 0)
            {
                DB.Entry(Divisions.ElementAt(0)).State =
                            System.Data.Entity.EntityState.Deleted;
                DB.SaveChanges();

                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }
    }
}
