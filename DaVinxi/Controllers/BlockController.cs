﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class BlockController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /Block/

        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
               int.Parse(Request.Cookies["company_id"].Value) > 0 ?
               int.Parse(Request.Cookies["company_id"].Value) : 0;

            List<MasterBlocks> Blocks = Helper.GetBlocks(CompanyID);

            IEnumerable<int> Companies = Blocks.Select(b => b.company_id);
            IEnumerable<string> Users = Blocks.Select(b => b.pic_username);

            ViewBag.RelatedCompanies = DB.Companies.Where(
                c => Companies.Contains(c.company_id)).ToDictionary(
                c => c.company_id, c => c.company_name);
            ViewBag.RelatedUser = DB.Users.Where(
                u => Users.Contains(u.username)).ToDictionary(
                u => u.username, u => u.full_name);
            ViewBag.RelatedDivision = Blocks.GroupBy(b => b.division_id).Select(m => m.First()).Join(DB.Divisions,
                b => new { c1 = b.company_id, c2 = b.division_id },
                d => new { c1 = d.company_id, c2 = d.division_id },
                (b, d) => new
                {
                    divisionID = d.division_id,
                    divisionName = d.division_name
                }).ToDictionary(
                    d => d.divisionID, d => d.divisionName);

            return View(Blocks);
        }



        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null ? 
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ? 
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Divisions = Helper.GetDivisions(Request.Cookies["company_id"] != null ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(BlockRegisterModel register)
        {
            if (ModelState.IsValid)
            {
                if (DB.Blocks.Where(b => b.company_id == register.company_id && 
                    string.Compare(b.block_id, register.block_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Block has been registered");
                else
                {
                    MasterBlocks NewBlock = new MasterBlocks();
                    NewBlock.company_id = register.company_id;
                    NewBlock.division_id = register.division_id;
                    NewBlock.block_id = register.block_id;
                    NewBlock.block_name = register.block_name;
                    NewBlock.pic_username = register.pic_username;
                    NewBlock.description = register.description;
                    NewBlock.revised_by = User.Identity.Name;

                    DB.Blocks.Add(NewBlock);
                    DB.SaveChanges();

                    return RedirectToAction("Index");
                }
            }

            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Divisions = Helper.GetDivisions(Request.Cookies["company_id"] != null ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            ModelState.AddModelError("","Cannot register new block");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string blockID)
        {
            IEnumerable<MasterBlocks> Block = DB.Blocks.Where(b => 
                b.company_id == companyID && string.Compare(b.block_id, blockID, true) == 0);
            if (Block == null || Block.Count() <= 0)
                return HttpNotFound();

            BlockRegisterModel EditBlock = new BlockRegisterModel();
            EditBlock.company_id = Block.ElementAt(0).company_id;
            EditBlock.division_id = Block.ElementAt(0).division_id;
            EditBlock.block_id = Block.ElementAt(0).block_id;
            EditBlock.block_name = Block.ElementAt(0).block_name;
            EditBlock.pic_username = Block.ElementAt(0).pic_username;
            EditBlock.description = Block.ElementAt(0).description;

            ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            ViewBag.Divisions = Helper.GetDivisions(Request.Cookies["company_id"] != null ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);

            return View(EditBlock);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(BlockRegisterModel editBlock)
        {   
            if (!ModelState.IsValid)
            {
                ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null ?
                    int.Parse(Request.Cookies["company_id"].Value) : 0);
                ViewBag.Users = Helper.GetUsers(Request.Cookies["company_id"] != null ?
                    int.Parse(Request.Cookies["company_id"].Value) : 0);
                ViewBag.Divisions = Helper.GetDivisions(Request.Cookies["company_id"] != null ?
                    int.Parse(Request.Cookies["company_id"].Value) : 0);

                ModelState.AddModelError("", "Cannot update block");

                return View(editBlock);
            }

            IEnumerable<MasterBlocks> Block = DB.Blocks.Where(b =>
                b.company_id == editBlock.company_id &&
                string.Compare(b.block_id, editBlock.block_id, true) == 0);

            if (Block == null || Block.Count() <= 0)
                return HttpNotFound();

            bool IsDirty = false;

            if (string.Compare(editBlock.division_id, Block.ElementAt(0).division_id, true) != 0)
            {
                Block.ElementAt(0).division_id = editBlock.division_id;
                IsDirty = true;
            }

            if (string.Compare(editBlock.block_name, Block.ElementAt(0).block_name, true) != 0)
            {
                Block.ElementAt(0).block_name = editBlock.block_name;
                IsDirty = true;
            }

            if (string.Compare(editBlock.pic_username, Block.ElementAt(0).pic_username, true) != 0)
            {
                Block.ElementAt(0).pic_username = editBlock.pic_username;
                IsDirty = true;
            }

            if (string.Compare(editBlock.description, Block.ElementAt(0).description) != 0)
            {
                Block.ElementAt(0).description = editBlock.description;
                IsDirty = true;
            }

            if (IsDirty)
            {
                Block.ElementAt(0).revised_by = User.Identity.Name;
                DB.Entry(Block.ElementAt(0)).State = System.Data.Entity.EntityState.Modified;
                DB.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string blockID, int companyID)
        {
            IEnumerable<MasterBlocks> Block = DB.Blocks.Where(b =>
                b.company_id == companyID &&
                string.Compare(b.block_id, blockID, true) == 0);

            if (Block == null || Block.Count() <= 0)
                return HttpNotFound();

            DB.Entry(Block.ElementAt(0)).State = System.Data.Entity.EntityState.Deleted;
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
