﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using System.IO;

namespace DaVinxi.Controllers
{
    public class CompanyController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        //
        // GET: /Company/

        [Authorize]
        public ActionResult Index()
        {
            return View(DB.Companies.ToList());
        }

        [Authorize]
        public ActionResult Register()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(CompanyRegisterModel register)
        {
            string Filename = "";

            if (Request.Files != null && Request.Files.Count > 0 && Request.Files["logo"] != null
                    && Request.Files["logo"].ContentLength > 0)
            {
                string Extension = Path.GetExtension(Request.Files["logo"].FileName);
                Filename = string.Format("{0}{1}",
                    DateTime.Now.ToString("yyyyMMddHHmmss"), Extension);
            }
            
            if (ModelState.IsValid)
            {
                MasterCompanies Company = new MasterCompanies();

                Company.company_name = register.company_name;
                Company.address = register.address;
                Company.phone_number = register.phone_number;
                Company.fax_number = register.fax_number;
                Company.email = register.email;
                Company.notes = register.notes;
                Company.revised_by = User.Identity.Name;
                Company.logo = Filename;

                if (Filename.Trim().Length > 0)
                {
                    string FolderPath = Server.MapPath(Constant.CompanyImagePath);
                    Filename = Path.Combine(FolderPath, Filename);

                    if (!Directory.Exists(FolderPath))
                        Directory.CreateDirectory(FolderPath);

                    Request.Files["logo"].SaveAs(Filename);
                }

                DB.Companies.Add(Company);
                DB.SaveChanges();
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Cannot add new company");

            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int id)
        {
            MasterCompanies Company = DB.Companies.Find(id);
            if (Company == null)
                return HttpNotFound();

            CompanyRegisterModel EditCompany = new CompanyRegisterModel();
            EditCompany.company_name = Company.company_name;
            EditCompany.address = Company.address;
            EditCompany.phone_number = Company.phone_number;
            EditCompany.fax_number = Company.fax_number;
            EditCompany.email = Company.email;
            EditCompany.notes = Company.notes;
            EditCompany.logo = Company.logo;

            ViewBag.companyID = id;

            return View(EditCompany);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(CompanyRegisterModel editCompany)
        {
            if (ModelState.IsValid)
            {
                MasterCompanies Company = DB.Companies.Find(int.Parse(Request["id"].ToString()));
                if (Company == null)
                    return HttpNotFound();

                bool isDirty = false;

                if (string.Compare(editCompany.company_name, Company.company_name, true) != 0)
                {
                    Company.company_name = editCompany.company_name;
                    isDirty = true;
                }

                if (string.Compare(editCompany.address, Company.address, true) != 0)
                {
                    Company.address = editCompany.address;
                    isDirty = true;
                }

                if (string.Compare(editCompany.phone_number, Company.phone_number, true) != 0)
                {
                    Company.phone_number = editCompany.phone_number;
                    isDirty = true;
                }

                if (string.Compare(editCompany.fax_number, Company.fax_number, true) != 0)
                {
                    Company.fax_number = editCompany.fax_number;
                    isDirty = true;
                }

                if (string.Compare(editCompany.email, Company.email, true) != 0)
                {
                    Company.email = editCompany.email;
                    isDirty = true;
                }

                if (string.Compare(editCompany.notes, Company.notes, true) != 0)
                {
                    Company.notes = editCompany.notes;
                    isDirty = true;
                }

                if (Request.Files != null && Request.Files.Count > 0 && Request.Files["logo"] != null 
                    && Request.Files["logo"].ContentLength > 0)
                {
                    if (!Directory.Exists(Server.MapPath(Constant.CompanyImagePath)))
                        Directory.CreateDirectory(Server.MapPath(Constant.CompanyImagePath));

                    Request.Files["logo"].SaveAs(Path.Combine(
                        Server.MapPath(Constant.CompanyImagePath), Company.logo));
                }

                if (isDirty)
                {
                    Company.revised_by = User.Identity.Name;
                    Company.revised_date = DateTime.Now;

                    DB.Entry(Company).State = System.Data.Entity.EntityState.Modified;
                    DB.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            return View(editCompany);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            MasterCompanies Company = DB.Companies.Find(id);
            if (Company == null)
                return HttpNotFound();

            string LogoPath = Company.logo;

            if (LogoPath.Length > 0)
            {
                FileInfo Logo = new FileInfo(Path.Combine(Server.MapPath(Constant.CompanyImagePath), LogoPath));
                if (Logo.Exists)
                    Logo.Delete();
            }

            DB.Entry(Company).State = System.Data.Entity.EntityState.Deleted;
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
