﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using System.Text;

namespace DaVinxi.Controllers
{
    public class DowntimeTrackingController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: DowntimeTracking
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View();
        }

        [AllowCrossSiteJson]
        public ActionResult GetDowntimeTracking(PostedDowntimeLogFilter filter, string targetParent)
        {
            try
            {
                targetParent = targetParent != null && targetParent.Length > 0 ? string.Format("'{0}'", targetParent) : "null";

                StringBuilder WhereClause = new StringBuilder();

                if (filter != null)
                {
                    if (filter.company_id > 0)
                        WhereClause.AppendFormat(" AND log.company_id={0}", filter.company_id);

                    if (filter.block_id != null && filter.block_id.Length > 0)
                        WhereClause.AppendFormat(" AND block.block_id='{0}'", filter.block_id);

                    if (filter.block_id != null && filter.block_id.Length > 0)
                        WhereClause.AppendFormat(" AND block.block_id='{0}'", filter.block_id);

                    if (filter.division_id != null && filter.division_id.Length > 0)
                        WhereClause.AppendFormat(" AND division.division_id='{0}'", filter.division_id);

                    if (filter.machine_id != null && filter.machine_id.Length > 0)
                        WhereClause.AppendFormat(" AND machine.machine_id='{0}'", filter.machine_id);

                    WhereClause.AppendFormat(" AND log.start_time>='{0}'", filter.start_date.ToString("yyyy-MM-dd HH:mm:ss"));
                    WhereClause.AppendFormat(" AND log.end_time<='{0}'", filter.finish_date.ToString("yyyy-MM-dd HH:mm:ss"));
                }

                string WhereStr = WhereClause.ToString().Length > 0 ? string.Format("WHERE {0}", WhereClause.ToString().Substring(5)) : "";

                string SQL = string.Format("SELECT log.downtime_log_id, log.start_time, " +
                        "log.end_time, DATEDIFF(minute,log.start_time,log.end_time) as duration, " +
                        "downtime.downtime_id,downtime.downtime_name  FROM [dbo].[DowntimeLogs] log " +
                    "JOIN [dbo].[MasterDowntimes] downtime ON " + 
                        "log.company_id=downtime.company_id AND " + 
                        "downtime.downtime_id=[dbo].[GetParentDowntime](log.company_id,log.downtime_id,{0}) " + 
                    "JOIN [dbo].[MasterMachines] machine on log.company_id=machine.company_id AND " + 
                        "log.machine_id=machine.machine_id " + 
                    "JOIN [dbo].[MasterBlocks] block on machine.company_id=block.company_id AND " + 
                        "machine.block_id=block.block_id " + 
                    "JOIN [dbo].[MasterDivisions] division on block.company_id=division.company_id AND " + 
                        "block.division_id=division.division_id {1}", targetParent, WhereStr);

                IEnumerable<DowntimeTrackingModel> Result = DB.Database.SqlQuery<DowntimeTrackingModel>(SQL).ToList();

                if (Result != null && Result.Count() > 0)
                {
                    var Tracking = from r in Result
                                   group r by r.downtime_id into g
                                   select new
                                   {
                                       downtime_id = g.Key,
                                       downtime_name = g.First().downtime_name,
                                       count = g.Count(),
                                       sum_duration = g.Sum(x => x.duration)
                                   };

                    HashSet<string> LogIDs=new HashSet<string>(Result.Select(r=>r.downtime_log_id));

                    var Actions = from dAction in DB.DowntimeActions
                                  where LogIDs.Contains(dAction.downtime_log_id) && dAction.is_fixed_the_problem
                                  join mAction in DB.OperativeActions on
                                  new { c1 = dAction.company_id, c2 = dAction.action_id } equals
                                  new { c1 = mAction.company_id, c2 = mAction.action_id }
                                  into rs
                                  from r in rs
                                  group r by r.action_id into g
                                  select new
                                  {
                                      action_id = g.Key,
                                      action_name = g.FirstOrDefault().action_name,
                                      count = g.Count()
                                  };

                    return new JsonResult()
                    {
                        Data = new
                        {
                            tracking = Tracking,
                            dAction = Actions
                        },
                        MaxJsonLength = Int32.MaxValue,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch(Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}