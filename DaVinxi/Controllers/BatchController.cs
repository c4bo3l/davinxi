﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class BatchController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }


        [Authorize]
        // GET: Batch
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);

            return View();
        }

        [Authorize]
        public ActionResult Register()
        {
            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            MasterBatch NewBatch = new MasterBatch();
            NewBatch.revised_by = User.Identity.Name;
            return View(NewBatch);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(MasterBatch registerBatch)
        {
            if(ModelState.IsValid)
            {
                if (DB.Batches.Where(b => b.company_id == registerBatch.company_id && 
                    string.Compare(b.batch_id, registerBatch.batch_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Batch already exist");
                else
                {
                    registerBatch.revised_by = User.Identity.Name;
                    DB.Batches.Add(registerBatch);
                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0);
            return View(registerBatch);
        }

        [Authorize]
        public ActionResult Update(int companyID,string batchID)
        {
            MasterBatch Batch = DB.Batches.Where(b => b.company_id == companyID && 
                string.Compare(b.batch_id, batchID, true) == 0).FirstOrDefault();

            if (Batch == null)
                return RedirectToAction("Index");

            return View(Batch);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(MasterBatch updatedBatch)
        {
            if (ModelState.IsValid)
            {
                MasterBatch Batch = DB.Batches.Where(b => b.company_id == updatedBatch.company_id &&
                    string.Compare(b.batch_id, updatedBatch.batch_id, true) == 0).FirstOrDefault();

                if (Batch != null)
                {
                    bool IsDirty = false;

                    if(string.Compare(Batch.sku_id,updatedBatch.sku_id,true)!=0)
                    {
                        Batch.sku_id = updatedBatch.sku_id;
                        IsDirty = true;
                    }

                    if (string.Compare(Batch.machine_id, updatedBatch.machine_id, true) != 0)
                    {
                        Batch.machine_id = updatedBatch.machine_id;
                        IsDirty = true;
                    }

                    if (updatedBatch.start_date.Subtract(Batch.start_date).TotalMinutes != 0)
                    {
                        Batch.start_date = updatedBatch.start_date;
                        IsDirty = true;
                    }

                    if (updatedBatch.finish_date.Subtract(Batch.finish_date).TotalMinutes != 0)
                    {
                        Batch.finish_date = updatedBatch.finish_date;
                        IsDirty = true;
                    }

                    if (IsDirty)
                    {
                        DB.Entry(Batch).State = System.Data.Entity.EntityState.Modified;
                        DB.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }

            return View(updatedBatch);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(int companyID,string batchID)
        {
            string SQL = string.Format("exec [dbo].[DeleteBatch] @companyID={0},@batchID='{1}'", companyID, batchID);
            DB.Database.ExecuteSqlCommand(SQL);

            return RedirectToAction("Index");
        }
    }
}