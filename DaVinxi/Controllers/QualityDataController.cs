﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class QualityDataController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: QualityData
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View();
        }

        [Authorize]
        [HttpPost]
        public string SaveQualityData(int company_id, string product_id, double qi, 
            IEnumerable<PostedQualityData> quality_data, IEnumerable<PostedDefectData> defect_data)
        {
            MasterProduct Product = DB.Products.Where(p => p.company_id == company_id && string.Compare(p.product_id, product_id, true) == 0).FirstOrDefault();
            if (Product == null)
                return "0xE";

            if (Product.qi != qi)
            {
                Product.qi = qi;
                DB.Entry(Product).State = System.Data.Entity.EntityState.Modified;
                DB.SaveChanges();
            }

            bool Success = true;
            string Status = "";
            if (quality_data != null)
            {
                Success = Success && SaveQualityData(Product, quality_data);
                if (!Success)
                    Status = "0x01";
            }

            if (defect_data != null)
            {
                Success = Success && SaveDefectData(Product, defect_data);
                if (!Success)
                    Status += "0x02";
            }

            return Success ? "0x00" : Status;
        }

        [Authorize]
        private bool SaveQualityData(MasterProduct product, IEnumerable<PostedQualityData> quality_data)
        {
            try
            {
                bool HasChanges = false;

                IEnumerable<ProductQuality> QDatas = DB.ProductQualities.Where(p => 
                            p.company_id == product.company_id &&
                            string.Compare(p.product_id, product.product_id, true) == 0);

                foreach(PostedQualityData data in quality_data)
                {
                    bool IsNew = false, IsDirty = false;

                    ProductQuality QData =
                       QDatas.Where(p => p.company_id == product.company_id &&
                            string.Compare(p.product_id, product.product_id, true) == 0 &&
                            string.Compare(p.quality_property_id, data.property_id, true) == 0).FirstOrDefault();

                    if (QData == null)
                    {
                        IsNew = true;
                        QData = new ProductQuality();
                        QData.company_id = product.company_id;
                        QData.product_id = product.product_id;
                        QData.quality_property_id = data.property_id;
                        IsDirty = true;
                    }

                    if (QData.quality_value != data.quality_value)
                    {
                        QData.quality_value = data.quality_value;
                        IsDirty = true;
                    }

                    if (QData.qi != data.qi)
                    {
                        QData.qi = data.qi;
                        IsDirty = true;
                    }

                    if (IsDirty)
                    {
                        QData.revised_date = DateTime.Now;
                        QData.revised_by = User.Identity.Name;

                        if (IsNew)
                            DB.ProductQualities.Add(QData);
                        else
                            DB.Entry(QData).State = System.Data.Entity.EntityState.Modified;

                        HasChanges = true;
                    }
                }

                if (HasChanges)
                    DB.SaveChanges();
            }
            catch(Exception)
            {
                return false;
            }

            return true;
        }

        [Authorize]
        private bool SaveDefectData(MasterProduct product, IEnumerable<PostedDefectData> defect_data)
        {
            try
            {
                bool HasChanges = false;

                IEnumerable<ProductDefect> PDefects = DB.ProductDefects.Where(p => 
                    p.company_id == product.company_id && string.Compare(p.product_id, product.product_id, true) == 0);

                if (PDefects != null && PDefects.Count() > 0)
                {
                    IEnumerable<string> SelectedDefect = defect_data.Select(d => d.defect_id);
                    IEnumerable<ProductDefect> DeletedPDefects = PDefects.Where(p => !SelectedDefect.Contains(p.defect_id));

                    if (DeletedPDefects != null && DeletedPDefects.Count() > 0)
                    {
                        foreach (ProductDefect deleted in DeletedPDefects)
                            DB.Entry(deleted).State = System.Data.Entity.EntityState.Deleted;
                    }

                    HasChanges = true;
                }

                foreach (PostedDefectData postedDefect in defect_data)
                {
                    bool IsNew = false, IsDirty = false;
                    ProductDefect PDefect = PDefects.Where(p => 
                        string.Compare(p.defect_id, postedDefect.defect_id, true) == 0).FirstOrDefault();

                    if (PDefect == null)
                    {
                        IsNew = true;

                        PDefect = new ProductDefect();
                        PDefect.company_id = product.company_id;
                        PDefect.product_id = product.product_id;
                        PDefect.defect_id = postedDefect.defect_id;

                        IsDirty = true;
                    }

                    if (PDefect.weight != postedDefect.weight)
                    {
                        PDefect.weight = postedDefect.weight;
                        IsDirty = true;
                    }

                    if (PDefect.severity != postedDefect.severity)
                    {
                        PDefect.severity = postedDefect.severity;
                        IsDirty = true;
                    }

                    if (IsDirty)
                    {
                        PDefect.revised_date = DateTime.Now;
                        PDefect.revised_by = User.Identity.Name;

                        if (IsNew)
                            DB.ProductDefects.Add(PDefect);
                        else
                            DB.Entry(PDefect).State = System.Data.Entity.EntityState.Modified;

                        HasChanges = true;
                    }
                }

                if (HasChanges)
                    DB.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}