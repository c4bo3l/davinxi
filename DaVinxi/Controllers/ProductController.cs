﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class ProductController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: Product
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View();
        }

        [Authorize]
        public ActionResult Register()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            MasterProduct NewProduct = new MasterProduct();
            NewProduct.revised_by = User.Identity.Name;
            return View(NewProduct);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(MasterProduct register)
        {
            if(ModelState.IsValid)
            {
                if (DB.Products.Where(p => p.company_id == register.company_id && 
                    string.Compare(p.product_id, register.product_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Product already exist");
                else
                {
                    register.revised_date = DateTime.Now;
                    register.revised_by = User.Identity.Name;
                    DB.Products.Add(register);
                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError("", "Cannot add new product");

            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;
            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View(register);
        }

        [Authorize]
        public ActionResult Delete(int companyID,string productID)
        {
            string SQL = string.Format("exec [dbo].[DeleteProduct] @companyID={0},@productID='{1}'", companyID, productID);
            DB.Database.ExecuteSqlCommand(SQL);

            return RedirectToAction("Index");
        }
    }
}