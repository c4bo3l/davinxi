﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class MachineController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        //
        // GET: /Machine/
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            
            return View();
        }

        [Authorize]
        public ActionResult Register()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Register(MachineRegisterModel register, ICollection<PostedMachineComponents> components)
        {
            if (ModelState.IsValid)
            {
                bool IsValid = true;

                MasterMachine NewMachine = new MasterMachine();
                NewMachine.company_id = register.company_id;
                NewMachine.machine_type_id = register.machine_type_id;
                NewMachine.machine_id = register.machine_id;
                NewMachine.machine_name = register.machine_name;
                NewMachine.installed_date = register.installed_date;
                NewMachine.revised_by = User.Identity.Name;
                NewMachine.block_id = register.block_id;
                DB.Machines.Add(NewMachine);

                if (components != null && components.Count > 0)
                {
                    foreach (PostedMachineComponents c in components)
                    {
                        double TestValue = 0;

                        if (!c.is_numeric || 
                            (c.is_numeric && double.TryParse(c.data_value, out TestValue)))
                        {
                            MachineComponents NewMachComponent = new MachineComponents();
                            NewMachComponent.company_id = NewMachine.company_id;
                            NewMachComponent.machine_id = NewMachine.machine_id;
                            NewMachComponent.component_id = c.id;
                            NewMachComponent.value = c.data_value;
                            NewMachComponent.tag_id = c.tag_id;
                            NewMachComponent.revised_by = User.Identity.Name;
                            DB.MachineComponents.Add(NewMachComponent);

                            if (c.tag_id != null && c.tag_id.Length > 0) {
                                HistorianTag Tag = DB.HistorianTags.Where(t => t.company_id == NewMachine.company_id && 
                                    string.Compare(t.tag_id, c.tag_id, true) == 0).FirstOrDefault();
                                if (Tag != null && string.Compare(Tag.machine_id, NewMachine.machine_id, true) != 0)
                                {
                                    Tag.machine_id = NewMachine.machine_id;
                                    Tag.revised_date = DateTime.Now;
                                    Tag.revised_by = User.Identity.Name;
                                    DB.Entry(Tag).State = System.Data.Entity.EntityState.Modified;
                                }
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("nonNumeric", 
                                "Please check again your component input");
                            IsValid = false;
                            break;
                        }
                    }
                }

                if (IsValid)
                {
                    try
                    {
                        DB.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch (Exception) {
                        DB.DetachAll();
                    }
                }
            }

            ViewBag.Companies = Helper.GetCompanies(Request.Cookies["company_id"] != null &&
                            int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                            int.Parse(Request.Cookies["company_id"].Value) : 0);

            ModelState.AddModelError("", "Cannot register new machine");
            return View(register);
        }

        [Authorize]
        public ActionResult Edit(int companyID, string machID)
        {
            MasterMachine Machine = DB.Machines.Where(
                m => m.company_id == companyID && string.Compare(machID, m.machine_id, true) == 0).FirstOrDefault();

            if (Machine != null)
            {
                MachineRegisterModel EditMachine = new MachineRegisterModel();
                EditMachine.company_id = Machine.company_id;
                EditMachine.machine_id = Machine.machine_id;
                EditMachine.machine_type_id = Machine.machine_type_id;
                EditMachine.machine_name = Machine.machine_name;
                EditMachine.installed_date = Machine.installed_date;
                EditMachine.block_id = Machine.block_id;
                
                return View(EditMachine);
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(MachineRegisterModel editMachine, 
            ICollection<PostedMachineComponents> components)
        {
            if (ModelState.IsValid)
            {
                bool IsValid = true;
                bool IsDirty = false;

                MasterMachine Machine = DB.Machines.Where
                    (m => m.company_id == editMachine.company_id && 
                        string.Compare(m.machine_id, editMachine.machine_id, true) == 0).FirstOrDefault();
                if (Machine != null)
                {

                    if (string.Compare(Machine.machine_name, editMachine.machine_name) != 0)
                    {
                        IsDirty = true;
                        Machine.machine_name = editMachine.machine_name;
                    }

                    if (Machine.installed_date.Subtract(editMachine.installed_date).TotalSeconds != 0)
                    {
                        IsDirty = true;
                        Machine.installed_date = editMachine.installed_date;
                    }

                    if (string.Compare(Machine.block_id, editMachine.block_id, true) != 0)
                    {
                        IsDirty = true;
                        Machine.block_id = editMachine.block_id;
                    }

                    if (IsDirty)
                        DB.Entry(Machine).State = 
                            System.Data.Entity.EntityState.Modified;

                    if (components != null && components.Count > 0)
                    {
                        bool IsNewComponent = false;

                        var DeletedComponentID = new HashSet<string>(components.Select(c => c.id));

                        IEnumerable<MachineComponents> MachComponents =
                                DB.MachineComponents.Where(m =>
                                    m.company_id == editMachine.company_id &&
                                    string.Compare(m.machine_id, editMachine.machine_id, true) == 0);

                        IEnumerable<MachineComponents> DeletedMachComponents = 
                            MachComponents.Where(m => !DeletedComponentID.Contains(m.component_id));
                        if (DeletedMachComponents != null && DeletedMachComponents.Count() > 0)
                        {
                            foreach (MachineComponents m in DeletedMachComponents)
                                DB.Entry(m).State = System.Data.Entity.EntityState.Deleted;
                        }

                        foreach (PostedMachineComponents c in components)
                        {
                            IsNewComponent = false;

                            IEnumerable<MachineComponents> MComponents = 
                                MachComponents.Where(m => 
                                    string.Compare(m.component_id, c.id, true) == 0);

                            MachineComponents MachComponent = null;

                            if (MComponents != null && MComponents.Count() > 0)
                                MachComponent = MComponents.ElementAt(0);
                            else
                            {
                                MachComponent = new MachineComponents();
                                MachComponent.company_id = editMachine.company_id;
                                MachComponent.machine_id = editMachine.machine_id;
                                MachComponent.component_id = c.id;
                                IsNewComponent = true;
                            }

                            if (string.Compare(MachComponent.tag_id, c.tag_id, true) != 0)
                            {
                                MachComponent.tag_id = c.tag_id;

                                HistorianTag Tag = DB.HistorianTags.Where(t => t.company_id == MachComponent.company_id && 
                                    string.Compare(t.tag_id, c.tag_id, true) == 0).FirstOrDefault();
                                if (Tag != null && string.Compare(Tag.machine_id, MachComponent.machine_id, true) != 0)
                                {
                                    Tag.machine_id = MachComponent.machine_id;
                                    Tag.revised_date = DateTime.Now;
                                    Tag.revised_by = User.Identity.Name;
                                    DB.Entry(Tag).State = System.Data.Entity.EntityState.Modified;
                                }

                                IsDirty = true;
                            }

                            double TestInput = 0;
                            if (c.is_numeric && !double.TryParse(c.data_value, out TestInput))
                            {
                                ModelState.AddModelError("noNumeric", "Invalid input");
                                IsValid = false;
                                break;
                            }
                            else if (MachComponent.value == null ||
                                string.Compare(MachComponent.value, c.data_value) != 0)
                            {
                                MachComponent.value = c.data_value;
                                MachComponent.revised_date = DateTime.Now;
                                MachComponent.revised_by = User.Identity.Name;

                                if (IsNewComponent)
                                    DB.MachineComponents.Add(MachComponent);
                                else
                                    DB.Entry(MachComponent).State =
                                        System.Data.Entity.EntityState.Modified;

                                IsDirty = true;
                            }
                        }
                    }
                }
                else
                    return RedirectToAction("Index");
                
                if (IsValid)
                {
                    try
                    {
                        if (IsDirty) DB.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        DB.DetachAll();
                    }
                }
            }

            ModelState.AddModelError("", "Cannot edit machine");
            return View(editMachine);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string machID, int companyID)
        {
            MasterMachine Machine = 
                DB.Machines.Where(m => m.company_id == companyID && 
                    string.Compare(m.machine_id, machID, true) == 0).FirstOrDefault();
            if (Machine != null)
            {
                DB.Entry(Machine).State = System.Data.Entity.EntityState.Deleted;

                IEnumerable<MachineComponents> Components = 
                    DB.MachineComponents.Where(m => m.company_id == companyID && 
                        string.Compare(m.machine_id, machID, true) == 0);

                if (Components != null && Components.Count() > 0)
                {
                    foreach (MachineComponents c in Components)
                        DB.Entry(c).State = System.Data.Entity.EntityState.Deleted;
                }

                DB.SaveChanges();
            }

            return RedirectToAction("Index");
        }
	}
}
