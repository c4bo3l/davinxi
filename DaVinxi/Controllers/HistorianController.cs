﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;
using System.Text.RegularExpressions;
using System.Data.Linq.SqlClient;

namespace DaVinxi.Controllers
{
    public class HistorianController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        // GET: Historian
        [Authorize]
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);

            return View();
        }

        [Authorize]
        public ActionResult Register()
        {
            HistorianTag NewTag = new HistorianTag();
            NewTag.revised_by = User.Identity.Name;

            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);

            return View(NewTag);
        }

        [Authorize, HttpPost]
        public ActionResult Register(HistorianTag newTag)
        {
            if (ModelState.IsValid)
            {
                if (DB.HistorianTags.Where(t => t.company_id == newTag.company_id && string.Compare(t.tag_id, newTag.tag_id, true) == 0).Count() > 0)
                    ModelState.AddModelError("exist", "Tag already exist");
                else
                {
                    DB.HistorianTags.Add(newTag);
                    DB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View(newTag);
        }

        [AllowCrossSiteJson]
        public ActionResult FindTag(SearchTagFilter filter)
        {
            try
            {
                var Result = from company in DB.Companies
                             join division in DB.Divisions on company.company_id equals division.company_id
                             join block in DB.Blocks on
                                new { c1 = division.company_id, c2 = division.division_id }
                                equals new { c1 = block.company_id, c2 = block.division_id }
                             join machine in DB.Machines on new { c1 = block.company_id, c2 = block.block_id }
                                equals new { c1 = machine.company_id, c2 = machine.block_id }
                             join tag in DB.HistorianTags on new { c1 = machine.company_id, c2 = machine.machine_id }
                                equals new { c1 = tag.company_id, c2 = tag.machine_id }
                             join unit in DB.EngineeringUnits on new { c1 = tag.company_id, c2 = tag.uom }
                                equals new { c1 = unit.company_id, c2 = unit.unit_id }
                             join compression in DB.TagCompressions on tag.compression_method equals compression.compression_id
                             select new
                             {
                                 company.company_id,
                                 division.division_id,
                                 block.block_id,
                                 machine.machine_id,
                                 machine.machine_name,
                                 unit.unit_name,
                                 compression.compression_name,
                                 tag = tag
                             };

                if (filter.company_id > 0)
                    Result = Result.Where(r => r.company_id == filter.company_id);

                if (filter.division_id != null && filter.division_id.Length > 0)
                    Result = Result.Where(r => string.Compare(r.division_id, filter.division_id, true) == 0);

                if (filter.block_id != null && filter.block_id.Length > 0)
                    Result = Result.Where(r => string.Compare(r.block_id, filter.block_id, true) == 0);

                if (filter.machine_id != null && filter.machine_id.Length > 0)
                    Result = Result.Where(r => string.Compare(r.machine_id, filter.machine_id, true) == 0);

                if (filter.tag_name != null && filter.tag_name.Length > 0)
                {
                    Result = Result.Where(r => r.tag.tag_name.ToLower().Contains(filter.tag_name.ToLower()));
                }

                if (filter.active_state >= 0) {
                    bool ActiveState = filter.active_state == 1;
                    Result = Result.Where(r => r.tag.active == ActiveState);
                }

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    MaxJsonLength = Int32.MaxValue,
                    Data = new
                    {
                        status = 1,
                        result = Result
                    }
                };
            }
            catch (Exception ex)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 0,
                        result = ex.Message
                    }
                };
            }
        }

        [Authorize]
        public ActionResult ChangeActiveState(int company_id, string tag_id, bool active)
        {
            string message = ""; int code = 1;

            try
            {
                HistorianTag Tag = DB.HistorianTags.Where(t => t.company_id == company_id &&
                    string.Compare(t.tag_id, tag_id, true) == 0).FirstOrDefault();
                if (Tag != null && Tag.active != active)
                {
                    Tag.active = active;
                    DB.Entry(Tag).State = System.Data.Entity.EntityState.Modified;
                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                code = -1;
                message = ex.Message;
            }

            return new JsonResult()
            {
                Data = new { code = code, message = message }
            };
        }

        [Authorize]
        public ActionResult Update(int companyID, string tagID)
        {
            HistorianTag Tag = DB.HistorianTags.Where(t => t.company_id == companyID && 
                string.Compare(t.tag_id, tagID, true) == 0).FirstOrDefault();
            if (Tag == null)
                return RedirectToAction("Index");
            return View(Tag);
        }

        [Authorize, HttpPost]
        public ActionResult Update(HistorianTag editedTag)
        {
            if (ModelState.IsValid)
            {
                HistorianTag ExistedTag = DB.HistorianTags.Where(t => t.company_id == editedTag.company_id 
                    && string.Compare(t.tag_id, editedTag.tag_id, true) == 0).FirstOrDefault();
                if (ExistedTag != null)
                {
                    bool IsDirty = false;

                    if (string.Compare(ExistedTag.tag_name, editedTag.tag_name, true) != 0)
                    {
                        ExistedTag.tag_name = editedTag.tag_name;
                        IsDirty = true;
                    }

                    if (string.Compare(ExistedTag.machine_id, editedTag.machine_id, true) != 0)
                    {
                        ExistedTag.machine_id = editedTag.machine_id;
                        IsDirty = true;
                    }

                    if (string.Compare(ExistedTag.uom, editedTag.uom, true) != 0)
                    {
                        ExistedTag.uom = editedTag.uom;
                        IsDirty = true;
                    }

                    if (string.Compare(ExistedTag.compression_method, editedTag.compression_method, true) != 0)
                    {
                        ExistedTag.compression_method = editedTag.compression_method;
                        IsDirty = true;
                    }

                    if (ExistedTag.compression_bandwidth != editedTag.compression_bandwidth)
                    {
                        ExistedTag.compression_bandwidth = editedTag.compression_bandwidth;
                        IsDirty = true;
                    }

                    if (ExistedTag.expired_time != editedTag.expired_time)
                    {
                        ExistedTag.expired_time = editedTag.expired_time;
                        IsDirty = true;
                    }

                    if (ExistedTag.active != editedTag.active)
                    {
                        ExistedTag.active = editedTag.active;
                        IsDirty = true;
                    }

                    if (ExistedTag.tag_description != editedTag.tag_description)
                    {
                        ExistedTag.tag_description = editedTag.tag_description;
                        IsDirty = true;
                    }

                    if (string.Compare(ExistedTag.source_tag_id, editedTag.source_tag_id) != 0)
                    {
                        ExistedTag.source_tag_id = editedTag.source_tag_id;
                        IsDirty = true;
                    }

                    if (IsDirty)
                    {
                        ExistedTag.revised_date = DateTime.Now;
                        ExistedTag.revised_by = User.Identity.Name;
                        DB.Entry(ExistedTag).State = System.Data.Entity.EntityState.Modified;
                        DB.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            return View(editedTag);
        }

        [Authorize, HttpPost]
        public ActionResult Delete(int companyID, string tagID)
        {
            HistorianTag ExistedTag = DB.HistorianTags.Where(t => t.company_id == companyID
                    && string.Compare(t.tag_id, tagID, true) == 0).FirstOrDefault();
            if (ExistedTag != null)
            {
                DB.Entry(ExistedTag).State = System.Data.Entity.EntityState.Deleted;
                DB.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}