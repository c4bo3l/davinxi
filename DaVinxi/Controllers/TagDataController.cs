﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaVinxi.Models;

namespace DaVinxi.Controllers
{
    public class TagDataController : Controller
    {
        private MyDBContext _DB;
        public MyDBContext DB
        {
            get
            {
                if (_DB == null)
                    _DB = new MyDBContext();
                return _DB;
            }
        }

        private HelperClassController _Helper;
        public HelperClassController Helper
        {
            get
            {
                if (_Helper == null)
                    _Helper = new HelperClassController(DB);
                return _Helper;
            }
        }

        [Authorize]
        // GET: TagData
        public ActionResult Index()
        {
            int CompanyID = Request.Cookies["company_id"] != null &&
                int.Parse(Request.Cookies["company_id"].Value) > 0 ?
                int.Parse(Request.Cookies["company_id"].Value) : 0;

            ViewBag.Companies = Helper.GetCompanies(CompanyID);
            return View();
        }

        [AllowCrossSiteJson]
        public ActionResult UploadData(IEnumerable<TagData> data)
        {
            string message = "No data";
            int status = 0;
            try
            {
                if (data != null && data.Count() > 0)
                {
                    HistorianTag Tag = null;
                    string PrevTag = "";
                    foreach (TagData d in data)
                    {
                        if (d.company_id > 0 && d.tag_id != null && d.tag_id.Length > 0)
                        {
                            bool IsNew = false, IsTagUpdated = false;
                            TagData ExistedData = DB.TagValues.Where(t => t.company_id == d.company_id &&
                                string.Compare(t.tag_id, d.tag_id, true) == 0 &&
                                t.timestamp == d.timestamp).FirstOrDefault();
                            if (string.Compare(PrevTag, d.tag_id, true) != 0)
                            {
                                Tag = DB.HistorianTags.Where(t => t.company_id == d.company_id &&
                                    string.Compare(t.tag_id, d.tag_id, true) == 0).FirstOrDefault();
                                PrevTag = d.tag_id;
                            }
                            if (Tag == null)
                                continue;

                            if (ExistedData == null)
                            {
                                ExistedData = new TagData();
                                ExistedData.company_id = d.company_id;
                                ExistedData.tag_id = d.tag_id;
                                ExistedData.timestamp = d.timestamp;
                                IsNew = true;
                            }

                            ExistedData.value = d.value;
                            if (Tag.last_date_data.ToOADate() < d.timestamp)
                            {
                                IsTagUpdated = true;
                                Tag.last_date_data = DateTime.FromOADate(d.timestamp);
                                Tag.revised_date = DateTime.Now;
                                Tag.revised_by = "auto_uploader";
                            }

                            if (Request.IsAuthenticated)
                                ExistedData.revised_by = User.Identity.Name;
                            else
                                ExistedData.revised_by = "auto_uploader";

                            if (IsNew)
                                DB.TagValues.Add(ExistedData);
                            else
                            {
                                ExistedData.revised_date = DateTime.Now;
                                DB.Entry(ExistedData).State = System.Data.Entity.EntityState.Modified;
                            }

                            if (IsTagUpdated)
                                DB.Entry(Tag).State = System.Data.Entity.EntityState.Modified;
                        }
                    }

                    DB.SaveChanges();

                    status = 1;
                    message = "Success";
                }
            }
            catch (Exception ex)
            {
                DB.DetachAll();
                status = -1;
                message = ex.Message;
            }

            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
                Data = new
                {
                    status = status,
                    message = message
                }
            };
        }

        [AllowCrossSiteJson]
        public ActionResult GetTags(int companyID, string machine_id = null)
        {
            IEnumerable<HistorianTag> Tags = DB.HistorianTags.Where(t => t.company_id == companyID);

            if (machine_id != null && machine_id.Length > 0 && Tags != null && Tags.Count() > 0)
                Tags = Tags.Where(t => string.Compare(t.machine_id, machine_id, true) == 0);

            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
                Data = Tags
            };
        }

        [Authorize, HttpPost]
        public ActionResult GetData(FilterTagData filter)
        {
            var Tags = from tag in DB.HistorianTags
                       join unit in DB.EngineeringUnits
                       on new { c1 = tag.company_id, c2 = tag.uom } equals new { c1 = unit.company_id, c2 = unit.unit_id }
                       join data in DB.TagValues
                       on new { c1 = tag.company_id, c2 = tag.tag_id } equals new { c1 = data.company_id, c2 = data.tag_id }
                       into result
                       from subdata in result.DefaultIfEmpty()
                       select new
                       {
                           tag,
                           unit,
                           data = subdata
                       };
            if (filter != null)
            {
                if (filter.company_id > 0)
                    Tags = Tags.Where(t => t.tag.company_id == filter.company_id);

                if (filter.tag_id != null && filter.tag_id.Count() > 0)
                    Tags = Tags.Where(t => filter.tag_id.Contains(t.tag.tag_id));

                double StartOADate = filter.start_date.ToOADate(), FinishOADate = filter.finish_date.ToOADate();

                Tags = Tags.Where(t => t.data.timestamp >= StartOADate && t.data.timestamp <= FinishOADate);
            }

            var TagIDs = Tags.ToList().GroupBy(t => t.tag.tag_id).Select(t => t.First().tag.tag_id);

            List<TagDataResult> Result = new List<TagDataResult>();

            if (TagIDs != null && TagIDs.Count() > 0)
            {
                DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                foreach (var id in TagIDs)
                {
                    TagDataResult DataResult = new TagDataResult();
                    DataResult.Tag = Tags.Where(t => string.Compare(t.tag.tag_id, id, true) == 0).Select(t => t.tag).First();
                    DataResult.Unit = Tags.Where(t => string.Compare(t.tag.tag_id, id, true) == 0).Select(t => t.unit).First();
                    var SubData = Tags.Where(t => string.Compare(t.tag.tag_id, id, true) == 0).ToList();
                    DataResult.Data = SubData.Select(t => 
                        new TagDataStructure() 
                        { 
                            timestamp = DateTime.FromOADate(t.data.timestamp).Subtract(Epoch).TotalMilliseconds, 
                            value = t.data.value 
                        }).ToList();
                    Result.Add(DataResult);
                }
            }

            return new JsonResult()
            {
                MaxJsonLength = Int32.MaxValue,
                Data = Result
            };
        }
    }
}