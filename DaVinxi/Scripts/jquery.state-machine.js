﻿function StateMachine(companyID, stateMachineID) {
    this.company_id = companyID;
    this.state_machine_id = stateMachineID;
    this.variables = new HashTable();
    this.transitions = new HashTable();

    this.getTransitionsJSON = function (url,sourceStateMachineID, sourceStateID) {
        var Result = null;

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                company_id: this.company_id, state_machine_id: this.state_machine_id,
                source_state_machine_id: sourceStateMachineID, state_id: sourceStateID
            },
            async: false,
            dataType: 'json'
        }).done(function (data) {
            Result = data;
        });

        return Result;
    }

    this.getNextState = function (url,currentState) {
        var TransitionID = "'" + currentState.state_machine_id + "_" + currentState.state_id + "'";
        if (!this.transitions.hasItem(TransitionID)) {
            this.transitions.setItem(TransitionID, this.getTransitionsJSON(url, currentState.state_machine_id,
                currentState.state_id));
        }

        var Transitions = this.transitions.getItem(TransitionID);

        for (var i = 0; i < Transitions.length; i++) {
            var VariablePattern = /@\w+/ig, ContainedVariable = Transitions[i].Trans.condition.match(VariablePattern);
            if (ContainedVariable != null) {
                for (var j = 0; j < ContainedVariable.length; j++) {
                    Transitions[i].Trans.condition = Transitions[i].Trans.condition.replace(new RegExp(ContainedVariable[j], "ig"),
                        "this.variables.getItem('" + ContainedVariable[j].replace("@", "") + "')");
                }
            }

            if (eval(Transitions[i].Trans.condition)) {
                return Transitions[i].target_state;
                break;
            }
        }

        return currentState;
    }
}