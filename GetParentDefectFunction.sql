use [DaVinxi]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
IF OBJECT_ID (N'dbo.GetParentDefect', N'FN') IS NOT NULL
    DROP FUNCTION GetParentDefect;
GO
-- =============================================
-- Author:		CabZ
-- Create date: Dec 26, 2014
-- Description:	Get parent defect
-- =============================================
CREATE FUNCTION GetParentDefect 
(
	-- Add the parameters for the function here
	@companyID int,
	@defectID varchar(128),
	@targetDefect varchar(128)
)
RETURNS varchar(128)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(128)
	declare @TempID varchar(128)
	declare @TempCurrentDefectID varchar(128)
	Set @TempID=@defectID
	set @Result=null
	-- Add the T-SQL statements to compute the return value here

	if(@TempID=@targetDefect)
		return @TempID

	while(@TempID<>'0x0000')
	begin
		select @TempID=parent_defect_id, @TempCurrentDefectID=defect_id from [dbo].[MasterDefects] where defect_id=@TempID and company_id=@companyID
		
		if(@targetDefect is not null and @targetDefect=@TempID)
			break
			
		if(@TempID<>'0x0000')
			Set @Result=@TempID	
	end
	if(@targetDefect is not null and @targetDefect<>@TempID)
		set @Result=null
	else if(@targetDefect is not null and @targetDefect=@TempID and @Result is null)
		set @Result=@TempCurrentDefectID
	-- Return the result of the function
	RETURN @Result

END
GO

