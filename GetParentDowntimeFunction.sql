USE [DaVinxi]
GO
/****** Object:  UserDefinedFunction [dbo].[GetParentDowntime]    Script Date: 1/21/2015 12:49:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.GetParentDowntime', N'FN') IS NOT NULL
    DROP FUNCTION GetParentDowntime;
GO
-- =============================================
-- Author:		CabZ
-- Create date: Dec 26, 2014
-- Description:	Get parent defect
-- =============================================
CREATE FUNCTION [dbo].[GetParentDowntime] 
(
	-- Add the parameters for the function here
	@companyID int,
	@downtimeID varchar(128),
	@targetDowntime varchar(128)
)
RETURNS varchar(128)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(128)
	declare @TempID varchar(128)
	declare @TempCurrentDowntimeID varchar(128)
	Set @TempID=@downtimeID
	set @Result=null
	-- Add the T-SQL statements to compute the return value here
	if(@TempID=@targetDowntime)
		return @TempID
	while(@TempID<>'0x0000')
	begin
		select @TempID=parent_downtime_id, @TempCurrentDowntimeID=downtime_id from [dbo].[MasterDowntimes] where downtime_id=@TempID and company_id=@companyID
		
		if(@targetDowntime is not null and @targetDowntime=@TempID)
			break
			
		if(@TempID<>'0x0000')
			Set @Result=@TempID	
	end
	if(@targetDowntime is not null and @targetDowntime<>@TempID)
		set @Result=null
	else if(@targetDowntime is not null and @targetDowntime=@TempID and @Result is null)
		set @Result=@TempCurrentDowntimeID
	-- Return the result of the function
	RETURN @Result

END
go